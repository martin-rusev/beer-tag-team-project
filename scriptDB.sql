create or replace table db_beers.countries
(
    country_id int auto_increment
        primary key,
    country    varchar(100) not null,
    constraint countries_name_uindex
        unique (country)
);

create or replace table db_beers.breweries
(
    brewery_id int auto_increment
        primary key,
    brewery    varchar(100) null,
    country_id int          not null,
    name       varchar(255) null,
    constraint breweries_countries_country_id_fk
        foreign key (country_id) references db_beers.countries (country_id)
);

create or replace table db_beers.rating
(
    id           int                                                                                   not null
        primary key,
    rating       enum ('POOR', 'NOT_SATISFIED', 'NICE', 'SUPERB', 'EXCELLENT', 'EXCEEDS_EXPECTATIONS') null,
    rating_value int                                                                                   null,
    constraint rating_rating_uindex
        unique (rating)
);

create or replace table db_beers.roles
(
    role_id int auto_increment
        primary key,
    role    varchar(50) not null
);

create or replace table db_beers.styles
(
    style_id int auto_increment
        primary key,
    style    varchar(100) not null,
    constraint styles_name_uindex
        unique (style)
);

create or replace table db_beers.tags
(
    tag_id int auto_increment
        primary key,
    tag    varchar(100) not null,
    constraint tags_tag_uindex
        unique (tag)
);

create or replace table db_beers.users
(
    user_id    int auto_increment
        primary key,
    username   varchar(50) not null,
    password   varchar(68) null,
    first_name varchar(50) null,
    last_name  varchar(50) null,
    email      varchar(50) null,
    enabled    tinyint     null,
    picture    longblob    null,
    constraint users_username_uindex
        unique (username)
);

create or replace table db_beers.authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint authorities_fk
        foreign key (username) references db_beers.users (username)
);

create or replace table db_beers.beers
(
    beer_id     int auto_increment
        primary key,
    name        varchar(100) not null,
    description text         null,
    brewery_id  int          null,
    abv         double       null,
    picture     longblob     null,
    style_id    int          null,
    created_by  int          null,
    country_id  int          null,
    rating      double       null,
    constraint beer_breweries_brewery_id_fk
        foreign key (brewery_id) references db_beers.breweries (brewery_id),
    constraint beer_styles_style_id_fk
        foreign key (style_id) references db_beers.styles (style_id),
    constraint beer_users_user_id_fk
        foreign key (created_by) references db_beers.users (user_id),
    constraint beers_countries_country_id_fk
        foreign key (country_id) references db_beers.countries (country_id)
);

create or replace table db_beers.beers_tags
(
    beer_tag_id int auto_increment
        primary key,
    beer_id     int not null,
    tag_id      int not null,
    constraint beer_id
        unique (beer_id, tag_id),
    constraint beers_tags_beers_beer_id_fk
        foreign key (beer_id) references db_beers.beers (beer_id),
    constraint beers_tags_tags_tag_id_fk
        foreign key (tag_id) references db_beers.tags (tag_id)
);

create or replace table db_beers.lists
(
    list_id   int auto_increment
        primary key,
    list_type varchar(50) not null,
    user_id   int         null,
    constraint FKe59kv852m4k3g8kmefph4i3kx
        foreign key (user_id) references db_beers.users (user_id)
);

create or replace table db_beers.lists_users_beers
(
    list_user_beer_id int auto_increment
        primary key,
    list_id           int not null,
    beer_id           int not null,
    constraint lists_users_beers_beers_beer_id_fk
        foreign key (beer_id) references db_beers.beers (beer_id),
    constraint lists_users_beers_lists_list_id_fk
        foreign key (list_id) references db_beers.lists (list_id)
);

create or replace table db_beers.ratings
(
    rating_id int auto_increment
        primary key,
    user_id   int null,
    beer_id   int not null,
    rating    int null,
    constraint ratings_beers_beer_id_fk
        foreign key (beer_id) references db_beers.beers (beer_id),
    constraint ratings_users_user_id_fk
        foreign key (user_id) references db_beers.users (user_id)
);

create or replace table db_beers.users_roles
(
    user_role_id int auto_increment
        primary key,
    user_id      int not null,
    role_id      int not null,
    constraint users_roles_roles_role_id_fk
        foreign key (role_id) references db_beers.roles (role_id),
    constraint users_roles_users_user_id_fk
        foreign key (user_id) references db_beers.users (user_id)
);


