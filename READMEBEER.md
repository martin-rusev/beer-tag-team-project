Beer Tag Team Project

    I. Details: 

	Team work on Beer Tag App between Martin and Teodora.
Link to our public Trello board: https://trello.com/b/tgu7EsUT/beer-tag-team-assignment

Link to Swagger: http://localhost:8081/swagger-ui.html#

    II. Project Description:

 The current software product represents a Beer Tag Web Application. The application allows the users:

    • to obtain detailed information about a certain beer
    • to filter and sort the beers in the database by a specified attribute/future
    • to add new beers in the storage by providing detailed information about them
    • to rate the beers and to see the average rating of a certain beer
    • to add and remove beers from their wish list or drunk list and to modify their personal information
    • admin can register new user with role admin



In this application there are three types of users: Anonymous users, Registered users and Administrators. Тhey have different rights based on their type:

-  Тhe anonymous users can get information about all the beers in the database as well as filter and sort them by a future/attribute
-  The registered users can create new beers, add a beer to their wish/drunk list and rate a beer. They can also edit and delete their own beers as well to modify their personal information
- The administrators have full control over all beers and users

    III. Rest API

In refence to the above functionalities, in the application was developed REST API Web service. The communication protocol used is HTTP and the date format used is JSON. It is provided and an API documentation that is required to successfully consume and integrate with the application programming interface.



    IV. Front – end
The public part of this application is accessible from any user without authentication. This includes the start page of the software product, the user login and the user registration forms, as well as list of all beers that have been added in the database. People that are not authenticated cannot see any user specific details, they can only browse the beers and see details of them.
The private part of this application is accessible from the registered users after a successful login. The UI provides them with the functionality to add, delete, edit and rate beers. It is provided and user profile page that shows user’s photo, details and their top three most ranked beers.
The administration part of this application provides administrative access to the system and permissions to administer all entities – e.g. users, beers. This is responsibility of the system administrators.

    V. Database
The data of the application is stored in a relational database – MySQL/MariaDB.

    VI. Used Tools:
    • JDK 1.8
    • Spring MVC and SpringBoot framework
    • Spring Security
    • Hibernate/JPA
    • AJAX and others

