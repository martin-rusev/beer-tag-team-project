package com.example.beertag.services;

import com.example.beertag.models.Role;
import com.example.beertag.repostitory.RoleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class RoleServiceImplTests {
    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Test
    public void createShould_CallRepository() {
        Role role = new Role("sos");
        Mockito.when(roleRepository.createRole(role))
                .thenReturn(role);

        roleService.createRole("sos");

        Mockito.verify(roleRepository,
                times(1)).createRole(role);
    }

}
