package com.example.beertag.services;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.repostitory.RatingRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static com.example.beertag.Factory.getStyles;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTests {

    @Mock
    RatingRepository ratingRepository;

    @InjectMocks
    RatingServiceImpl ratingServiceMock;

        @Test
    public void addRatingShould_CallReptository() {
        BeerRating beerRating = new BeerRating();

        Mockito.when((ratingRepository.addRating(beerRating)))
                .thenReturn(beerRating);

        //Act
        ratingServiceMock.addRating(new Beer(), Rating.EXCEEDS_EXPECTATIONS, new User());

        Mockito.verify(ratingRepository,
                times(1));

    }

    @Test
    public void getRatingShould_ThrowWhenNotFound() {
        Beer beer = new Beer(1,"bla", "blab", 4.5);

        Mockito.when(ratingRepository.checkIfBeerIsRated(beer))
                .thenReturn(false);

        Assertions.assertThrows(NotFoundException.class, () -> {
            ratingServiceMock.getRating(beer);
        });
    }

    @Test
    public void getRatingMVCShould_CallRepository() {
        Beer beer = new Beer(1, "dasa", "sass", 4.5);

        ratingServiceMock.getRatingMVC(beer);

        Mockito.verify(ratingRepository,
                times(1)).getRating(beer);

    }

    @Test
    public void addRatingShould_CallRepository() {
        BeerRating beerRating = new BeerRating();

        ratingServiceMock.addRating(beerRating);

        Mockito.verify(ratingRepository,
                times(1)).addRating(beerRating);

    }

    @Test
    public void getRatingShould_CallRepository() {
        Beer beer = new Beer(1, "dasa", "sass", 4.5);

        Mockito.when(ratingRepository.checkIfBeerIsRated(beer))
                .thenReturn(true);

        ratingServiceMock.getRating(beer);

        Mockito.verify(ratingRepository,
                times(1)).getRating(beer);

    }
}
