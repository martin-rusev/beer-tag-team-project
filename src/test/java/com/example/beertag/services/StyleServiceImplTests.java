package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.repostitory.StyleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.beertag.Factory.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class StyleServiceImplTests {

    @Mock
    StyleRepository styleRepository;

    @InjectMocks
    StyleServiceImpl mockService;

    @Test
    public void getStyleByNameShould_CallRepository() {
        Style style = new Style("TelerikStyle");

        Mockito.when(styleRepository.getStyleByName("TelerikStyle"))
                .thenReturn(style);

        //Act
        mockService.getStyleByName("TelerikStyle");

        Mockito.verify(styleRepository,
                times(1)).getStyleByName("TelerikStyle");
    }

    @Test
    public void getAllBeerStylesShould_CallRepository() {
        List<Style> styles = getStyles();

        Mockito.when((styleRepository.getAllStyles()))
                .thenReturn(styles);

        //Act
        mockService.allStyles();

        Mockito.verify(styleRepository,
                times(1)).getAllStyles();

    }

    @Test
    public void getStyleByIdShould_CallRepository() {
        Style style = new Style("TelerikStyle");

        Mockito.when(styleRepository.getStyleById(1))
                .thenReturn(style);

        //Act
        mockService.getStyleById(1);

        Mockito.verify(styleRepository,
                times(1)).getStyleById(1);
    }

    @Test
    public void deleteShould_CallRepository() {
        Style style = new Style("TelerikStyle");

        Mockito.when(styleRepository.delete(style))
                .thenReturn(style);

        //Act
        mockService.delete(style);

        Mockito.verify(styleRepository,
                times(1)).delete(style);
    }

    @Test
    public void updateShould_CallRepository() {
        Style style = new Style("TelerikStyle");

        Mockito.when(styleRepository.update(style))
                .thenReturn(style);

        //Act
        mockService.update(style);

        Mockito.verify(styleRepository,
                times(1)).update(style);
    }

    @Test
    public void createShould_CallRepository() {
        Style style = new Style("TelerikStyle");

        Mockito.when(styleRepository.create(style))
                .thenReturn(style);

        //Act
        mockService.create(style);

        Mockito.verify(styleRepository,
                times(1)).create(style);
    }

    @Test
    public void createShould_ThrowWhenStyleAlreadyExist() {
        Style style = new Style("sasa");


        Mockito.when(styleRepository.checkIfStyleExist("sasa"))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.create(style);
        });
    }

    @Test
    public void saveImgShould_ThrowWIOException() {
        List<Style>style;

        Mockito.when(styleRepository.getAllStyles())
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.allStyles();
        });
    }

    @Test
    public void getStyleByNamehould_ThrowWhenStyleNotFound() {

        Mockito.when(styleRepository.getStyleByName("sas"))
                .thenThrow(RuntimeException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getStyleByName("sas");
        });
    }

    @Test
    public void checkIfStyleHasBeers_CallRepository() {
        Style style = new Style("TelerikStyle");

        Mockito.when(styleRepository.checkIfStyleHasBeers("TelerikStyle"))
                .thenReturn(true);

        //Act
        mockService.checkIfStyleHasBeers("TelerikStyle");

        Mockito.verify(styleRepository,
                times(1)).checkIfStyleHasBeers("TelerikStyle");
    }

    @Test
    public void getBeersPopularStyles_NoBeerStyles_ShouldReturnEmptyStyles() {
        Mockito.when(mockService.allStyles())
                .thenReturn(new ArrayList<Style>());

        Assert.assertEquals(mockService.getBeersPopularStyles().size(), 0);
    }

    @Test
    public void getBeersPopularStyles_OneBeerStyleWithNoBeers_ShouldReturnSingleStyleWithNoBeers() {
        Style lightStyle = new Style("light");
        Mockito.when(mockService.allStyles())
                .thenReturn(new ArrayList<Style>(){
                    {
                        add(lightStyle);
                    }
                });
        Mockito.when(mockService.checkIfStyleHasBeers(Mockito.anyString()))
                .thenReturn(false);

        Map<Style, List<Beer>> styles = mockService.getBeersPopularStyles();
        Assert.assertEquals(1, styles.size());
        Assert.assertEquals(0, styles.get(lightStyle).size());
    }

    @Test
    public void getStyleBeersCOunt_NoBeerStyles_ShouldReturnEmptyStyles() {
        Mockito.when(mockService.allStyles())
                .thenReturn(new ArrayList<Style>());

        Assert.assertEquals(mockService.getStylesBeersCount().size(), 0);
    }
}
