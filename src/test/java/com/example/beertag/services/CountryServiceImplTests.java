package com.example.beertag.services;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Country;
import com.example.beertag.models.Style;
import com.example.beertag.repostitory.CountryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.example.beertag.Factory.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.Silent.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    CountryServiceImpl mockService;

    @Test
    public void getCountryByIdShould_ReturnCountry_WhenCountryExists() {
        Country country = createCountry();

        Mockito.when(mockService.getCountryById(1))
                .thenReturn(country);

        //Act
        Country returnedCountry = countryRepository.getCountryById(1);

        Assert.assertEquals(returnedCountry.getId(), country.getId());
    }

    @Test
    public void getCountryByNameShould_CallRepository() {
        Country country = new Country(1, "TelerikCountry");

        //Act
        mockService.getCountryByName("TelerikCountry");

        Mockito.verify(countryRepository,
                times(1)).getCountryByName("TelerikCountry");
    }

    @Test
    public void getAllCountryShould_CallRepository() {
        List<Country> countries = new ArrayList<>();
        countries.add(new Country(1, "TelerikCountry"));
        countries.add(new Country(2, "TelerikCountry2"));

        Mockito.when(countryRepository.getAllCountries())
                .thenReturn(countries);

        //Act
        mockService.getAllCountries();

        Mockito.verify(countryRepository,
                times(1)).getAllCountries();
    }

    @Test
    public void getAllCountriesShould_ThrowWhenCountriesNotFound() {
        List<Country>countries;

        Mockito.when(countryRepository.getAllCountries())
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getAllCountries();
        });
    }

    @Test
    public void checkIfCountryHasBeers_CallRepository() {
        Country country = new Country(1, "TelerikCountry");

        mockService.checkIfSCountryHasBeers("TelerikCountry");

        Mockito.verify(countryRepository,
                times(1)).checkIfSCountryHasBeers("TelerikCountry");

    }

    @Test
    public void getBeersByCountryShould_NotFoundException() {
        Beer beer = new Beer(1, "dsds", "dsds", 4.5);
        Country country = new Country(1,"ko");

        Mockito.when(countryRepository.getCountryByName("ko"))
                .thenThrow(RuntimeException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getBeersByCountry("ko", "name");
        });
    }

    @Test
    public void getBeersByCountryShould_CallgetAllBeersByCounty() {
        Country country = new Country(1, "TelerikCountry");

        Mockito.when(countryRepository.checkIfSCountryHasBeers("TelerikCountry")).thenReturn(true);

        mockService.getBeersByCountry("TelerikCountry", "name");

        Mockito.verify(countryRepository,
                times(1)).getAllBeersByCounty("TelerikCountry", "name");
    }

    @Test
    public void getBeersByCountryShould_NOTCallgetAllBeersByCounty() {
        Country country = new Country(1, "TelerikCountry");

        Mockito.when(countryRepository.checkIfSCountryHasBeers("TelerikCountry")).thenReturn(false);

        mockService.getBeersByCountry("TelerikCountry", "name");

//        Mockito.verify(countryRepository,
//                times(0)).getAllBeersByCounty("TelerikCountry", "name");
        Assert.assertEquals(mockService.getBeersByCountry("TelerikCountry", "name"), new ArrayList<>());
    }
}
