package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.models.User;
import com.example.beertag.repostitory.BeerRepository;
import com.example.beertag.repostitory.UserRepository;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static com.example.beertag.Factory.createBeer;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class ImageServiceImplTests {
    @Mock
    BeerRepository beerRepository;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    ImageServiceImpl mockImageService;
    //TODO IN BEFORE BLOCK
    @Spy
    public MultipartFile file = new MultipartFile() {
        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getOriginalFilename() {
            return null;
        }

        @Override
        public String getContentType() {
            return null;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public long getSize() {
            return 0;
        }

        @Override
        public byte[] getBytes() throws IOException {
            return new byte[0];
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return null;
        }

        @Override
        public void transferTo(File dest) throws IOException, IllegalStateException {

        }
    };

    @Test
    public void saveImagFileShould_CallRepository() {
        Beer beer = new Beer(1, "ba", "ba", 23);

        Mockito.when(beerRepository.getBeerById(1))
                .thenReturn(beer);

        //Act
        mockImageService.saveImgFile(1, file);

        Mockito.verify(beerRepository,
                times(1)).getBeerById(1);
    }

    @Test
    public void saveImagUserFileShould_CallRepository() throws IOException {
        User user = new User(1, "dssd", "sdsd","dssdsd", "dsdsds", "dsdsd"  );

        Mockito.when(userRepository.getUserById(1))
                .thenReturn(user);

        //Act
        mockImageService.saveUserImgFile(1, file);

        Mockito.verify(userRepository,
                times(1)).getUserById(1);
    }

    @Test
    public void saveImagUserFileShould_ThrowWhenStyleNotFound() throws IOException {

        Mockito.when(file.getBytes())
                .thenThrow(IOException.class);

        Assertions.assertThrows(IOException.class, () -> {
            mockImageService.saveUserImgFile(1, file);
        });
    }

}
