package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Country;
import com.example.beertag.repostitory.BeerRepository;
import com.example.beertag.repostitory.BreweryRepository;
import com.example.beertag.repostitory.CountryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.example.beertag.Factory.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.Silent.class)
public class BreweryServiceImplTests {

    @Mock
    BreweryRepository breweryRepository;
    @Mock
    BeerRepository beerRepository;
    @Mock
    CountryRepository countryRepository;

    @InjectMocks
    BreweryServiceImpl mockService;


    @Test
    public void getBreweryByIdShould_CallRepository() {
        Brewery expectedBrewery = createBrewery();

        Mockito.when(breweryRepository.getBreweryById(1))
                .thenReturn(expectedBrewery);

        //Act
        mockService.getBreweryById(1);

        Mockito.verify(breweryRepository,
                times(1)).getBreweryById(1);

    }

    @Test
    public void getBreweryById_ShouldReturnBrewery_WhenBreweryExists() {
        Brewery originalBrewery = createBrewery();

        Mockito.when(mockService.getBreweryById(1))
                .thenReturn(originalBrewery);

        //Act
        Brewery returnedBrewery = breweryRepository.getBreweryById(1);

        Assert.assertEquals(returnedBrewery.getId(), originalBrewery.getId());
    }

    @Test
    public void getBreweryNameShould_CallRepository() {
        Brewery brewery = createBrewery();

        Mockito.when(breweryRepository.getBreweryName("TekerikBrewery"))
                .thenReturn(brewery);

        //Act
        mockService.getBreweryName("TekerikBrewery");

        Mockito.verify(breweryRepository,
                times(1)).getBreweryName("TekerikBrewery");
    }

    @Test
    public void getAllBreweriesShould_CallRepository() {
        List<Brewery> breweries = getBreweries();

        Mockito.when((breweryRepository.getAllBreweries()))
                .thenReturn(breweries);

        //Act
        mockService.getAllBreweries();

        Mockito.verify(breweryRepository,
                times(1)).getAllBreweries();
    }

    @Test
    public void getCollectionBreweryByNameShould_CallRepository() {
        List<Brewery> breweries = getBreweries();

        Mockito.when((breweryRepository.getCollectionBreweryByName("SomeName")))
                .thenReturn(breweries);

        //Act
        mockService.getCollectionBreweryByName("SomeName");

        Mockito.verify(breweryRepository,
                times(1)).getCollectionBreweryByName("SomeName");
    }


    @Test
    public void getBreweryBeersShould_CallBeerRepository() {

        List<Beer> beers = getBeers();

        Mockito.when((beerRepository.getBreweryBeers(1)))
                .thenReturn(beers);

        //Act
        mockService.getBreweryBeers(1);

        Mockito.verify(beerRepository,
                times(1)).getBreweryBeers(1);

    }

    @Test
    public void updateBreweryShould_CallRepository() {
        Brewery brewery = createBrewery();
        //Act
        mockService.updateBrewery(brewery);

        Mockito.verify(breweryRepository,
                times(1)).updateBrewery(brewery);
    }

    @Test
    public void deleteBreweryShould_CallRepository() {
        Brewery brewery = createBrewery();
        //Act
        mockService.deleteBrewery(1);

        Mockito.verify(breweryRepository,
                times(1)).deleteBrewery
                (breweryRepository.getBreweryById(1));
    }


    @Test
    public void createBreweryShould_CallRepository() {
        Brewery brewery = createBrewery();
        //Act
        mockService.createBrewery(brewery);

        Mockito.verify(breweryRepository,
                times(1)).createBrewery(brewery);
    }

//    @Test
//    public void getBreweryNameShouldThrownWhen_notSuchEntity() {
//        //Act
//        Mockito.when(breweryRepository.getBreweryName("name"))
//                .thenThrow(EntityNotFoundException.class);
//
//        Assertions.assertThrows(NotFoundException.class, () -> mockService.getBreweryName("name"));
//    }

    @Test
    public void createShould_ThrowWhenBreweryAlreadyExist() {
        Brewery brewery = new Brewery(1, "dad");

        Mockito.when(breweryRepository.checkIfBreweryExist(brewery))
                .thenReturn(true);


        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.createBrewery(brewery);
        });

    }

    @Test
    public void getBreweriesByCountryShould_ThrowException() {
        Country country = countryRepository.getCountryByName("Bulgaria");

       Mockito.when(breweryRepository.getBreweriesByCountry(country))
               .thenThrow(RuntimeException.class);

        Assertions.assertThrows(NotFoundException.class, () -> mockService.getBreweriesByCountry("Bulgaria"));
    }

    @Test
    public void getBreweriesByCountryShould_CallRepository() {
        Country country = countryRepository.getCountryByName("Bulgaria");
        //Act
        mockService.getBreweriesByCountry("Bulgaria");

        Mockito.verify(breweryRepository,
                times(1)).getBreweriesByCountry(country);
    }


}
