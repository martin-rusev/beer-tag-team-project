package com.example.beertag.services;

import com.example.beertag.exeptions.DeleteForbiddenException;
import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;
import com.example.beertag.models.Style;
import com.example.beertag.models.User;
import com.example.beertag.repostitory.BeerRepository;
import com.example.beertag.repostitory.StyleRepository;
import org.graalvm.compiler.core.common.type.ArithmeticOpTable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.example.beertag.Factory.createBeer;
import static com.example.beertag.Factory.getBeers;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;


@RunWith(MockitoJUnitRunner.class)
public class BeerServiceImplTests {

    @Mock
    BeerRepository beerRepository;
    @Mock
    StyleRepository styleRepository;
    @Mock
    Beer beer;

    @Spy
    @InjectMocks
    BeerServiceImpl mockService;

    @Test
    public void getBeerById_ReturnBeer_WhenBeerExists() {
        Beer originalBeer = createBeer();

        Mockito.when(mockService.getBeerById(1))
                .thenReturn(originalBeer);

        //Act
        Beer returnedBeer = beerRepository.getBeerById(1);

        Assert.assertEquals(returnedBeer.getId(), originalBeer.getId());
    }

    @Test
    public void getBeerByIdShould_CallRepository() {
        Beer expectedBeer = createBeer();

        Mockito.when(beerRepository.getBeerById(1))
                .thenReturn(expectedBeer);

        //Act
        mockService.getBeerById(1);

        Mockito.verify(beerRepository,
                times(1)).getBeerById(1);

    }

    @Test
    public void createShould_ThrowWhenBeerAlreadyExist() {
        Beer beer = new Beer(1, "TestBeer",
                "SomeDescription", 4.5);

        Mockito.when(beerRepository.checkIfBeerExist(beer))
                .thenReturn(true);

        // Assertions.assertThrows(DuplicateException.class, () -> mockService.createBeer(createBeer()));
        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.createBeer(beer, new User());
        });

    }


    @Test
    public void getListBeersShould_CallRepository() {
        List<Beer> beers = getBeers();

        Mockito.when((beerRepository.getAllListBeers()))
                .thenReturn(beers);

        //Act
        mockService.getListBeers();

        Mockito.verify(beerRepository,
                times(1)).getAllListBeers();
    }

    @Test
    public void getBreweryBeersShould_CallBeerRepository() {

        List<Beer> beers = getBeers();

        Mockito.when((beerRepository.getBreweryBeers(1)))
                .thenReturn(beers);

        //Act
        mockService.getBreweryBeers(1);

        Mockito.verify(beerRepository,
                times(1)).getBreweryBeers(1);

    }

    @Test
    public void getBeerByNameShould_CallRepository() {
        List<Beer> beers = getBeers();

        //Act
        mockService.getBeerByName("TelerikBeer");

        Mockito.verify(beerRepository,
                times(1)).getBeerByName("TelerikBeer");
    }


    @Test
    public void createBeerShould_CallRepository() {
        Beer beer = createBeer();
        //Act
        mockService.createBeer(beer, new User());

        Mockito.verify(beerRepository,
                times(1)).createBeer(beer);
    }


    @Test
    public void getBeerByIdShouldThrownWhen_notSuchEntity() {
        //Act
        Mockito.when(beerRepository.getBeerById(1))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> mockService.getBeerById(1));
    }


    @Test
    public void getBeerByNameShould_ThrowNotFoundException() {
        Beer beer = new Beer(1, "TestBeer",
                "SomeDescription", 4.5);

        Mockito.when(beerRepository.getBeerByName("TestBeer"))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getBeerByName("TestBeer");
        });

    }


    @Test
    public void getSortedBeersByAbvShould_ThrowEntityFoundException() {

        Mockito.when(beerRepository.getSortedBeersByAbv())
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getSortedBeersByAbv();
        });
    }

    @Test
    public void getSortedBeersByAbvShould_CallRepository() {
        List<Beer> beers = getBeers();

        Mockito.when(beerRepository.getSortedBeersByAbv())
                .thenReturn(beers);

        //Act
        mockService.getSortedBeersByAbv();

        Mockito.verify(beerRepository,
                times(1)).getSortedBeersByAbv();
    }

    @Test
    public void getBeersByTagShould_CallRepository() {
        List<Beer> beers = getBeers();

        Mockito.when(beerRepository.getBeersByTag("tag"))
                .thenReturn(beers);

        //Act
        mockService.getBeersByTag("tag");

        Mockito.verify(beerRepository,
                times(1)).getBeersByTag("tag");
    }

    @Test
    public void getBeersByNameShould_ThrowNotFoundException() {
        List<Beer> beers = getBeers();

        Mockito.when(beerRepository.getBeersByName("dsd", "dsds"))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getBeersByName("dsd", "dsds");
        });

    }

    @Test
    public void getBeersByNameShould_CallRepository() {
        List<Beer> beers = getBeers();

        Mockito.when(beerRepository.getBeersByName("ds", "dsa"))
                .thenReturn(beers);

        //Act
        mockService.getBeersByName("ds", "dsa");

        Mockito.verify(beerRepository,
                times(1)).getBeersByName("ds", "dsa");
    }

    @Test
    public void deleteBeerShould_ThrowNotFoundException() {
        Beer beer = new Beer(1, "sdds", "dsdsd", 4.5);
        User user = new User();
        Mockito.when(beerRepository.deleteBeer(beer))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.deleteBeer(beer, user);
        });

    }

    @Test
    public void styleHasBeersShould_CallRepository() {
        Style style = new Style("dsds");

        //Act
        mockService.styleHasBeers(style);

        Mockito.verify(beerRepository,
                times(1)).getBeersByStyle("dsds", "name");
    }

    @Test
    public void getBeersByStyleShould_NotFoundException() {
        Beer beer = new Beer(1, "dsds", "dsds", 4.5);
        Style style = new Style("ko");

        Mockito.when(styleRepository.getStyleByName("ko"))
                .thenThrow(RuntimeException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getBeersByStyle("ko", "name");
        });
    }

    @Test
    public void getBeersByStyleShould_Return() {
        String styleName = "ko";
        String orderParam = "";
        Style style = new Style(styleName);

        Mockito.when(styleRepository.getStyleByName(Mockito.anyString()))
                .thenReturn(style);
        Mockito.doReturn(true).when(mockService).styleHasBeers(style);
        Mockito.when(beerRepository.getBeersByStyle(styleName, orderParam))
                .thenReturn(new ArrayList<>());

        Assert.assertEquals(0, mockService.getBeersByStyle(styleName, orderParam).size());
    }
    @Test
    public void getBeersByStyleShould_ReturnArray() {
        String styleName = "ko";
        String orderParam = "";
        Style style = new Style(styleName);

        Mockito.when(styleRepository.getStyleByName(Mockito.anyString()))
                .thenReturn(style);
        Mockito.doReturn(false).when(mockService).styleHasBeers(style);

        Assert.assertEquals(new ArrayList<>(), mockService.getBeersByStyle(styleName, orderParam));
    }

    @Test
    public void safeDeleteShould_CallRepository() {

        Beer beer = createBeer();
        //Act
        mockService.safeDelete(beer);

        Mockito.verify(beerRepository,
                times(1)).deleteBeer(beer);
    }

    @Test
    public void safeDeleteShould_ThrowExc() {

        Beer beer = createBeer();
        //Act
        Mockito.when(mockService.safeDelete(beer))
                .thenThrow(DataIntegrityViolationException.class);

        Assertions.assertThrows(DeleteForbiddenException.class, () -> {
            mockService.safeDelete(beer);
        });
    }


    @Test
    public void getSortedBeersByRatingShould_CallRepository() {
        List<Beer> beers = getBeers();

        Mockito.when(beerRepository.getSortedBeersByRating())
                .thenReturn(beers);

        mockService.getSortedBeersByRating();

        Mockito.verify(beerRepository,
                times(1)).getSortedBeersByRating();
    }

    @Test
    public void updateBeerRatingShould_CallRepository() {

        Beer beer = createBeer();
        //Act
        mockService.updateBeerTagingRating(beer);

        Mockito.verify(beerRepository,
                times(1)).updateBeer(beer);
    }

    @Test
    public void getAllBeersOrderedShould_Return() {
        String styleName = "ko";
        String style = "style";

        mockService.getAllBeersOrdered(styleName,style);

        Assert.assertEquals(beerRepository.getAllBeers(), mockService.getAllBeersOrdered(styleName, style));
    }

    @Test
    public void deleteBeerShould_Return() {
       Beer beer = createBeer();
       User user = new User();

        mockService.deleteBeer(beer,user);

        Assert.assertEquals(beer, mockService.deleteBeer(beer,user));
    }

    @Test
    public void deleteBeerShould_ThrowDataIntegrityException() {
        Beer beer = new Beer(1, "sdds", "dsdsd", 4.5);
        User user = new User();
        Mockito.when(beerRepository.deleteBeer(beer))
                .thenThrow(DataIntegrityViolationException.class);

        Assertions.assertThrows(DeleteForbiddenException.class, () -> {
            mockService.deleteBeer(beer, user);
        });

    }
}
