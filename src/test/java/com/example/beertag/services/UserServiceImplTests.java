package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.repostitory.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;

import static com.example.beertag.Factory.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {
    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl mockService;

    @Test
    public void getUserByIdShould_ReturnUser_WhenUserExists() {
        User originalUser = createUser();

        Mockito.when(userRepository.getUserById(originalUser.getId()))
                .thenReturn(originalUser);

        Assert.assertSame(originalUser, mockService.getUserById(originalUser.getId()));
    }

    @Test
    public void getAllUsersShould_ReturnUsers_WhenUsersExists() {

        Mockito.when(userRepository.getAllUsers())
                .thenReturn(Collections.singletonList(createUser()));

        Assert.assertEquals(1, mockService.getAllUsers().size());
    }

    @Test
    public void getUserByNameShould_CallRepository() {
        List<User> users = getUsers();

        Mockito.when(userRepository.getUsersByName("TelerikUser"))
                .thenReturn(users);

        //Act
        mockService.getUsersByName("TelerikUser");

        Mockito.verify(userRepository,
                times(1)).getUsersByName("TelerikUser");
    }

    @Test
    public void getUserByUserNameShould_CallRepository() {
        User user = new User();

        Mockito.when(userRepository.getUserByUsername("TelerikUser"))
                .thenReturn(user);

        //Act
        mockService.getUserByUsername("TelerikUser");

        Mockito.verify(userRepository,
                times(1)).getUserByUsername("TelerikUser");
    }

    @Test
    public void createUserShould_CallRepository() {
        User user = new User();
        //Act
        mockService.createUser(user);

        Mockito.verify(userRepository,
                times(1)).createUser(user);

    }

    @Test
    public void updateUserShould_CallRepository() {
        User user = new User();

        //Act
        mockService.updateUser(user);

        Mockito.verify(userRepository,
                times(1)).updateUser(user);
    }

    @Test
    public void deleteUserShould_CallRepository() {
        User user = new User();

        //Act
        mockService.deleteUser(1);

        Mockito.verify(userRepository,
                times(1)).deleteUser(userRepository.getUserById(1));
    }

    @Test
    public void getAllUsersShould_ThrowNotFoundException() {

        Mockito.when(userRepository.getAllUsers())
                .thenThrow(new EntityNotFoundException());

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getAllUsers();
        });
    }

    @Test
    public void createUserShould_ThrowDuplicateException() {
        User user = new User(1,"dsd","dsd","dsd","dsd","dsd");
        Mockito.when(userRepository.checkIfUserExist(user))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.createUser(user);
        });
    }

    @Test
    public void createListShould_CallRepository() {
        User user = new User(1,"dsd","dsd","dsd","dsd","dsd");
        ListPersonal listPersonal = new ListPersonal();
        //Act
        mockService.createList(listPersonal,user);

        Mockito.verify(userRepository,
                times(1)).createList(listPersonal, user);
    }

    @Test
    public void getUserByUsernameShould_ThrowEntityNotFound() {
        User user = new User();

        Mockito.when(userRepository.getUserByUsername("some"))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getUserByUsername("some");
        });
    }

    @Test
    public void getUserByIdShould_ThrowEntityNotFound() {
        User user = new User();

        Mockito.when(userRepository.getUserById(1))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getUserById(1);
        });
    }
}
