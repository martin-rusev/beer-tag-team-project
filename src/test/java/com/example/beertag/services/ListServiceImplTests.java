package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.ListType;
import com.example.beertag.models.Style;
import com.example.beertag.models.User;
import com.example.beertag.repostitory.ListRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DuplicateKeyException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class ListServiceImplTests {

    @Mock
    ListRepository listRepository;

    @Mock
    ListService listService;

    @Spy
    @InjectMocks
    ListServiceImpl listServiceMock;

    @Test
    public void updateListShould_CallRepository() {
        ListPersonal listPersonal = new ListPersonal();

        Mockito.when(listRepository.updateList(listPersonal))
                .thenReturn(listPersonal);

        //Act
        listServiceMock.updateList(listPersonal);

        Mockito.verify(listRepository,
                times(1)).updateList(listPersonal);
    }

    @Test
    public void getListByIdShould_CallRepository() {
        ListPersonal listPersonal = new ListPersonal();

        Mockito.when(listRepository.getListById(1))
                .thenReturn(listPersonal);

        //Act
        listServiceMock.getListById(1);

        Mockito.verify(listRepository,
                times(1)).getListById(1);
    }

    @Test
    public void getListByIdShould_ThrowNotFoundException() {
        List<Style> style;

        Mockito.when(listRepository.getListById(1))
                .thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            listServiceMock.getListById(1);
        });
    }

    @Test
    public void createListShould_CallRepository() {
        ListPersonal listPersonal = new ListPersonal();

        Mockito.when(listRepository.createList(listPersonal))
                .thenReturn(listPersonal);

        //Act
        listServiceMock.createList(listPersonal);

        Mockito.verify(listRepository,
                times(1)).createList(listPersonal);
    }

    @Test
    public void deleteListShould_CallRepository() {
        ListPersonal listPersonal = new ListPersonal();

        Mockito.when(listRepository.deleteList(listPersonal))
                .thenReturn(listPersonal);

        //Act
        listServiceMock.deleteList(listPersonal);

        Mockito.verify(listRepository,
                times(1)).deleteList(listPersonal);
    }

    @Test
    public void getListByTypeUserShould_CallRepository() {
        ListPersonal listPersonal = new ListPersonal();
        User user = new User();

        Mockito.when(listRepository.getListByTypeUser(user, ListType.DRUNKLIST))
                .thenReturn(listPersonal);

        //Act
        listServiceMock.getListByTypeUser(user, ListType.DRUNKLIST);

        Mockito.verify(listRepository,
                times(1)).getListByTypeUser(user, ListType.DRUNKLIST);
    }

    @Test
    public void updateListShould_ThrowDuplException() {
        ListPersonal listPersonal = new ListPersonal();

        Mockito.when(listRepository.updateList(listPersonal))
                .thenThrow(DuplicateKeyException.class);

        Assertions.assertThrows(DuplicateException.class, () -> {
            listServiceMock.updateList(listPersonal);
        });
    }

    @Test
    public void deleteListShould_ThrowInvalidlException() {
        ListPersonal listPersonal = new ListPersonal();

        Mockito.when(listServiceMock.isEmptyList(listPersonal))
                .thenReturn(false);

        Assertions.assertThrows(InvalidOperationException.class, () -> {
            listServiceMock.deleteList(listPersonal);
        });
    }



//    @Test
//    public void createListShould_CallRepositoryy() {
//        User user = new User();
////        ListPersonal listPersonal = new ListPersonal();
////        listPersonal.setUser(user);
////        listPersonal.setType(ListType.DRUNKLIST);
//
////        Mockito.when(listRepository.createList(listPersonal))
////                .thenReturn(listPersonal);
//
//        //Act
//        listServiceMock.createList(ListType.DRUNKLIST, user);
//
//        Mockito.verify(listRepository,
//                times(1)).createList(new ListPersonal());
//    }
}
