package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Tag;
import com.example.beertag.models.User;
import com.example.beertag.repostitory.TagRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.example.beertag.Factory.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTests {

    @Mock
    TagRepository tagRepository;

    @InjectMocks
    TagServiceImpl mockService;

    @Test
    public void getAllTagsShould_CallRepository() {
        List<Tag> tags = getTags();

        Mockito.when((tagRepository.getAllTags()))
                .thenReturn(tags);

        //Act
        mockService.getAllTags();

        Mockito.verify(tagRepository,
                times(1)).getAllTags();

    }

    @Test
    public void getTagById_ShouldReturnTag_WhenTagExists() {
        Tag tag = new Tag();

        Mockito.when(mockService.getTagById(1))
                .thenReturn(tag);

        //Act
        Tag returnedTag = tagRepository.getTagById(1);

        Assert.assertEquals(returnedTag.getId(), tag.getId());
    }

//    @Test
//    public void getTagByNameShould_CallRepository() {
//        List<Tag> tags = new ArrayList<>();
//        tags.add(new Tag(1,"super"));
//        tags.add(new Tag(2,"huhu"));
//
//        Mockito.when(tagRepository.getTagsByName("super"))
//                .thenReturn(tags);
//
//        //Act
//        mockService.getTagByName("super");
//
//        Mockito.verify(tagRepository,
//                times(1)).getTagsByName("super");
//    }
    @Test
    public void createTagShould_CallRepository() {
        Tag tag = new Tag();

        //Act
        mockService.createTag(tag);

        Mockito.verify(tagRepository,
                times(1)).createTag(tag);
    }

    @Test
    public void updateTagShould_CallRepository() {
        Tag tag = new Tag();

        //Act
        mockService.updateTag(1,tag);

        Mockito.verify(tagRepository,
                times(1)).updateTag(1,tag);
    }

    @Test
    public void deleteTagShould_CallRepository() {
        Tag tag = createTag();

        //Act
        mockService.deleteTag(1);

        Mockito.verify(tagRepository,
                times(1)).deleteTag(tagRepository.getTagById(1));
    }

    @Test
    public void getAllTagsShould_ThrowNotFoundException() {

        Mockito.when(tagRepository.getAllTags())
                .thenThrow(new NotFoundException("дсд"));

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getAllTags();
        });
    }

    @Test
    public void createTagShould_ThrowDuplicateException() {
        Tag tag = new Tag(1, "sdsds");
        Mockito.when(tagRepository.checkIfTagExist(tag))
                .thenReturn(true);

        Assertions.assertThrows(DuplicateException.class, () -> {
            mockService.createTag(tag);
        });
    }

    @Test
    public void getTagByNameShould_ThrowNotFoundException() {

        Tag tag = tagRepository.getTagByName("super");

        Mockito.when(tagRepository.checkIfTagExist(tag))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getTagByName("super");
        });
    }

    @Test
    public void getTagByNameShouldd_ThrowNotFoundException() {

        Tag tag = tagRepository.getTagByName("super");

        Mockito.when(tagRepository.checkIfTagExist(tag))
                .thenReturn(false);

        Assertions.assertThrows(NotFoundException.class, () -> {
            mockService.getTagByName("super");
        });
    }

}

