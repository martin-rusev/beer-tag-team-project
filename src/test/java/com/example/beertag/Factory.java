package com.example.beertag;

import com.example.beertag.models.*;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class Factory {
    public static Beer createBeer() {
        return new Beer(1, "TestBeer",
                "SomeDescription", 4.5);
    }

    public static User createUser() {
        return new User(1, "Petar",
                "SomePass", "petyr", "petrov", "petar@abv.bg");
    }

    public static Brewery createBrewery() {
        return new Brewery(1, "TekerikBrewery");
    }

    public static List<Brewery> getBreweries() {
        List<Brewery> breweries = new ArrayList<>();
        breweries.add(new Brewery(1, "SomeName"));
        breweries.add(new Brewery(2, "SomeName2"));
        return breweries;
    }

    public static List<Beer> getBeers() {
        List<Beer> beers = new ArrayList<>();
        beers.add(new Beer(1, "SameName", "Some discription,", 4.5));
        beers.add(new Beer(2, "SameName2", "Some discription2,", 5.5));
        return beers;
    }

    public static Country createCountry() {
        return new Country(1, "TelerikCountry");
    }

    public static Style createStyle() {
        return new Style("TelerikStyle");
    }

    public static List<Style> getStyles() {
        List<Style> styles = new ArrayList<>();
        styles.add(new Style("TelerikStyle"));
        styles.add(new Style("TelerikStyle2"));
        return styles;
    }

    public static Tag createTag() {
        return new Tag(1, "TekerikTag");
    }
    public static List<Tag> getTags() {
        List<Tag> tags = new ArrayList<>();
        tags.add(new Tag());
        tags.add(new Tag());
        return tags;
    }

    public static List<User> getUsers() {
        List<User> users = new ArrayList<>();
        users.add(new User());
        users.add(new User());
        return users;
    }

    public static List<Country> getCountries() {
        List<Country> countries = new ArrayList<>();
        countries.add(new Country(1,"TelerikCountry"));
        countries.add(new Country(2, "Another"));
        return countries;
    }
}
