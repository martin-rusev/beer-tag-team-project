package com.example.beertag.repostitory;

import com.example.beertag.models.Beer;
import com.example.beertag.models.BeerRating;
import com.example.beertag.models.Rating;
import com.example.beertag.services.RatingService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class RatingRepositoryImpl implements RatingRepository {
    SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

   @Transactional
    @Override
    public BeerRating addRating(BeerRating beerRating) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beerRating);

            return beerRating;
        }
    }

    @Override
    public double getRating(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> ratingQuery = session.
                    createQuery("select avg(rating)FROM BeerRating WHERE beer.id=:beerId");
            int beerId = beer.getId();
            ratingQuery.setParameter("beerId", beerId);

            double avgRating = 0;
               if (ratingQuery.getResultList().get(0)==null){
               avgRating= 0; }else {
                   avgRating=ratingQuery.getResultList().get(0);
               }

                return avgRating;
        }
    }

    @Override
    public boolean checkIfBeerIsRated(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> ratingQuery = session.
                    createQuery("FROM BeerRating WHERE beer.id=:beerId");
            int beerId = beer.getId();
            ratingQuery.setParameter("beerId", beerId);
            return ratingQuery.list().size() != 0;
        }
    }
}
