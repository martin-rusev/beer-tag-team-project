package com.example.beertag.repostitory;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Collection<Beer> getAllBeers() {
        try (Session session = sessionFactory.openSession()) {
            return session.
                    createQuery("from Beer order by name", Beer.class)
                    .list();
        }
    }

    @Override
    public List<Beer> getAllListBeers() {
        try (Session session = sessionFactory.openSession()) {
            return session.
                    createQuery("from Beer order by name", Beer.class)
                    .list();
        }
    }

    @Override
    public Beer getBeerById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new NotFoundException("Beer", "ID", String.valueOf(id));
            }
            return beer;
        }
    }

    @Override
    public List<Beer> getBreweryBeers(int breweryId) {

        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.
                    createQuery("from Beer where brewery.id = :breweryId", Beer.class);
            query.setParameter("breweryId", breweryId);
            return query.list();
        }
    }

    @Override
    public boolean checkIfBreweryHasBeers( Brewery brewery) {
        return getBreweryBeers(brewery.getId()).size()>=1;
    }

    @Override
    public List<Beer> getBeersByName(String name, String orderParam) {

        try (Session session = sessionFactory.openSession()) {
            Query<Beer> beerQuery = session.createQuery("from Beer where name = :name order by :orderParam");
            beerQuery.setParameter("name", name);
            beerQuery.setParameter("orderParam", orderParam);
            if (beerQuery.list().isEmpty() || beerQuery.list().size() == 0) {
                throw new NotFoundException(name);
            }
            return beerQuery.list();
        }
    }

    @Override
    public Beer getBeerByName(String name) {

        try (Session session = sessionFactory.openSession()) {
            //TODO check how to get caseInsesitive String
            Query<Beer> beerQuery = session.createQuery("from Beer where name = :name order by name");
            beerQuery.setParameter("name", name);

            if (beerQuery.list().isEmpty() || beerQuery.list().size() == 0) {
                throw new NotFoundException(name);
            }
            return beerQuery.list().get(0);
        }
    }

    @Override
    public boolean checkIfBeerExist(Beer beer) {
        String name = beer.getName();
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> beerQuery = session.createQuery("from Beer where name = :name order by name");
            beerQuery.setParameter("name", name);
            if (beerQuery.list().isEmpty() || beerQuery.list().size() == 0) {
                return false;
            }
            List<Beer> beersSameName = beerQuery.list().stream().filter(e -> e.getBrewery() != null && e.getBrewery().getName().equals(beer.getBrewery().getName()))
                    .collect(Collectors.toList());

            return beersSameName.size() >= 1;
        }
    }


    @Override
    public List<Beer> getBeersByStyle(String styleName, String orderParam) {

        try (Session session = sessionFactory.openSession()) {
            Query<Beer> styleQuery =
                    session.createQuery("from Beer where style.name = :styleName order by :orderParam");
            styleQuery.setParameter("styleName", styleName);
            styleQuery.setParameter("orderParam", orderParam);
            List<Beer> beerByStyle = styleQuery.list();
//            if (beerByStyle == null || beerByStyle.size() == 0) {
//                throw new EntityNotFoundException(String.format("Style %s doesn't have beers", styleName));
//            }
            return beerByStyle;
        }
    }


    @Override
    public Beer createBeer(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.saveOrUpdate(beer);
            return beer;
        }
    }

    @Override
    public Beer updateBeer(Beer beer) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(String.valueOf(beer.getId()), beer);
            session.getTransaction().commit();
            return beer;
        }
    }


    @Override
    public Beer deleteBeer(Beer beer) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(beer);
            session.getTransaction().commit();
            return beer;
        }
    }

    @Override
    public Collection<Beer> getSortedBeersByAbv() {

        try (Session session = sessionFactory.openSession()) {
            return session.
                    createQuery("from Beer order by abv DESC ", Beer.class)
                    .list();
        }
    }

    @Override
    public List<Beer> getBeersByTag(String tagName) {

        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session
                    .createQuery("select b from Beer b join b.tags a " +
                            "where a.name = :tagName ", Beer.class);
            query.setParameter("tagName", tagName);
            return query.list();
        }
    }

    @Override
    public Collection<Beer> getSortedBeersByRating() {
        try (Session session = sessionFactory.openSession()) {
            return session.
                    createQuery("from Beer order by tempRating DESC ", Beer.class)
                    .list();
        }
    }
}
