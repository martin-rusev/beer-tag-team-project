package com.example.beertag.repostitory;

import com.example.beertag.models.*;

import java.util.List;
import java.util.Map;

public interface StyleRepository {

    Style getStyleByName(String name);

    boolean checkIfStyleExist(String style);

    List<Style> getAllStyles();

    boolean checkIfStyleHasBeers(String styleName);

    Style create(Style style);

    Style update(Style style);

    Style delete(Style style);

    Style getStyleById(int id);
}
