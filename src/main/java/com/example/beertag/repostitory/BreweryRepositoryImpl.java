package com.example.beertag.repostitory;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;

import com.example.beertag.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Repository
public class BreweryRepositoryImpl implements BreweryRepository {
    private List<Brewery> database;
    private SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Collection<Brewery> getAllBreweries() {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery", Brewery.class);
            if (query.list().isEmpty()) {
                throw new NotFoundException("Breweries");
            }
            return query.list();
        }
    }

    @Override
    public Brewery getBreweryById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null) {
                throw new NotFoundException("Brewery", "ID", String.valueOf(id));
            }
            session.evict(brewery);
            return brewery;
        }
    }

    @Override
    public void createBrewery(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(brewery);
            session.getTransaction().commit();
        }
    }

    @Override
    public Collection<Brewery> getCollectionBreweryByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name LIKE :name", Brewery.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public Brewery getBreweryName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name = :name", Brewery.class);
            query.setParameter("name", name);

            if (query.list().isEmpty() || query.list().size() == 0) {
                throw new NotFoundException(name);
            }
            return query.list().get(0);
        }
    }

    @Override
    public void updateBrewery(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
        }
    }

    @Override
    public Brewery deleteBrewery(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(brewery);
            session.getTransaction().commit();
            return brewery;
        }
    }

    @Override
    public List<Brewery> getBreweriesByCountry(Country country) {
        try (Session session = sessionFactory.openSession()) {
            int countryId = country.getId();
            Query<Brewery> breweryQuery = session.createQuery("from Brewery where country.id = :countryId");
            breweryQuery.setParameter("countryId", countryId);
            return breweryQuery.list();
        }
    }

    @Override
    public boolean checkIfBreweryExist(Brewery brewery) {
        if (getAllBreweries().contains(brewery)) {
            return true;
        }
        return false;
    }

    private Brewery getById(int id, Session session) {
        Brewery brewery = session.get(Brewery.class, id);
        if (brewery == null) {
            throw new NotFoundException("Brewery", id);
        }
        return brewery;
    }
}
