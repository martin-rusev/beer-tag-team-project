package com.example.beertag.repostitory;

import com.example.beertag.models.Beer;
import com.example.beertag.models.BeerRating;
import com.example.beertag.models.Rating;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository {

    BeerRating addRating(BeerRating beerRating);

    double getRating(Beer beer);

    boolean checkIfBeerIsRated(Beer beer);

}
