package com.example.beertag.repostitory;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Country;

import java.util.Collection;
import java.util.List;

public interface BreweryRepository {

    Collection<Brewery> getAllBreweries();

    Brewery getBreweryById(int id);

    void createBrewery(Brewery brewery);

    Collection<Brewery> getCollectionBreweryByName(String name);

    Brewery getBreweryName(String name);

    void updateBrewery(Brewery brewery);

    Brewery deleteBrewery(Brewery brewery);

    List<Brewery> getBreweriesByCountry(Country country);

    boolean checkIfBreweryExist(Brewery brewery);
}
