package com.example.beertag.repostitory;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;

import java.util.List;

public interface CountryRepository {

    Country getCountryById(int id);

    Country getCountryByName(String name);

    List<Country> getAllCountries();

    List<Beer> getAllBeersByCounty(String countryName, String orderParam);

    boolean checkIfSCountryHasBeers(String countryName);
}
