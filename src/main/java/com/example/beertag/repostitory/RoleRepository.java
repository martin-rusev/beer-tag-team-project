package com.example.beertag.repostitory;

import com.example.beertag.models.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository {

    Role createRole(Role role);

}
