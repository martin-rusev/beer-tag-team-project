package com.example.beertag.repostitory;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.ListType;
import com.example.beertag.models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ListRepositoryImpl implements ListRepository {
    SessionFactory sessionFactory;

    @Autowired
    public ListRepositoryImpl(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    @Override
    public ListPersonal createList(ListPersonal personalList) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.evict(personalList);
            session.saveOrUpdate(personalList);
            session.getTransaction().commit();
            return personalList;
        }
    }


    @Override
    public ListPersonal getListById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ListPersonal listPersonal = session.get(ListPersonal.class, id);
            if (listPersonal == null) {
                throw new NotFoundException("Personal list", listPersonal.getId());
            }
            session.evict(listPersonal);
            return listPersonal;
        }
    }

    @Override
    public ListPersonal getListByTypeUser(User user, ListType listType) {

        try (Session session = sessionFactory.openSession()) {
            Query <ListPersonal> listPersonal= session.createQuery("from ListPersonal " +
                    "where user =:user and type=:listType ");
            listPersonal.setParameter("user",user);
            listPersonal.setParameter("listType",listType);

            return listPersonal.list().get(0);
        }

    }

    @Override
    public ListPersonal updateList(ListPersonal listPersonal) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.evict(listPersonal);
            session.saveOrUpdate(listPersonal);
            session.getTransaction().commit();
            return listPersonal;
        }

    }

    @Override
    public ListPersonal deleteList(ListPersonal personalList) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(personalList);
            session.getTransaction().commit();
            return personalList;
        }
    }
}
