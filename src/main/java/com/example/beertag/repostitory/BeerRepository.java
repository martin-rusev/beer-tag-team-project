package com.example.beertag.repostitory;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;

import java.util.Collection;
import java.util.List;

public interface BeerRepository {

    Collection<Beer> getAllBeers();

    List<Beer> getAllListBeers();

    Beer getBeerById(int id);

    List<Beer> getBeersByName(String name, String orderParam);

    Beer getBeerByName(String name);

    List<Beer> getBeersByStyle(String styleName, String orderParam);

    boolean checkIfBeerExist(Beer beer);

    boolean checkIfBreweryHasBeers( Brewery brewery);

    Beer createBeer(Beer beer);

    List<Beer> getBreweryBeers(int breweryId);

    Beer updateBeer(Beer beer);

    Beer deleteBeer(Beer beer);

    Collection<Beer> getSortedBeersByAbv();

    List<Beer> getBeersByTag(String tagName);

    Collection<Beer> getSortedBeersByRating();
}
