package com.example.beertag.repostitory;

import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.ListType;
import com.example.beertag.models.User;

public interface ListRepository {

    ListPersonal updateList(ListPersonal listUpdate);
    ListPersonal getListById(int id);
    ListPersonal createList(ListPersonal personalList);
    ListPersonal deleteList(ListPersonal personalList);


    ListPersonal getListByTypeUser(User user, ListType listType);
}
