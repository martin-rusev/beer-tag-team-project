package com.example.beertag.repostitory;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Country;
import com.example.beertag.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountryRepositoryImpl implements CountryRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CountryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Country getCountryById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (country == null) {
                throw new NotFoundException("Country", id);
            }
            session.evict(country);
            return country;
        }
    }

    @Override
    public Country getCountryByName(String countryName) {
        try (Session session = sessionFactory.openSession()) {

            Query<Country> countryQuery = session.createQuery("FROM Country where name Like :countryName", Country.class);

            countryQuery.setParameter("countryName", "%" + countryName + "%");

            Country country = countryQuery.getSingleResult();
            if (country == null) {
                throw new NotFoundException("Country", "name", countryName);
            }
            return country;
        }
    }

    @Override
    public List<Country> getAllCountries() {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country ", Country.class);
            return query.list();
        }
    }

    @Override
    public List<Beer> getAllBeersByCounty(String countryName, String orderParam) {

        try (Session session = sessionFactory.openSession()) {
            Query<Beer> countyQuery =
                    session.createQuery("from Beer where country.name = :countryName order by :orderParam");
            countyQuery.setParameter("countryName", countryName);
            countyQuery.setParameter("orderParam", orderParam);
            List<Beer> beerByCountry = countyQuery.list();
//            if (beerByStyle == null || beerByStyle.size() == 0) {
//                throw new EntityNotFoundException(String.format("Style %s doesn't have beers", styleName));
//            }
            return beerByCountry;
        }
    }

    @Override
    public boolean checkIfSCountryHasBeers(String countryName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> beerQuery =
                    session.createQuery("from Beer where country.name = :countryName");
            beerQuery.setParameter("countryName", countryName);
            List<Beer> beerByCountry = beerQuery.list();
            return beerByCountry!=null&&beerByCountry.size()>0;
        }
    }
}
