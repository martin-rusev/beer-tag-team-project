package com.example.beertag.repostitory;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.User;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Collection<User> getAllUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User ", User.class);
            return query.list();
        }
    }

    @Override
    public User getUserById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if (user == null) {
                throw new NotFoundException("User", "ID", String.valueOf(id));
            }
            session.evict(user);
            return user;
        }
    }

    @Override
    public Collection<User> getUsersByName(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username LIKE :username", User.class);
            query.setParameter("username", "%" + username + "%");
            if (query.list().size() != 1) {
                throw new NotFoundException("User", "ID", username);
            }

            return query.list();
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username");
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.size() != 1) {
                throw new NotFoundException("User");
            }

            return users.get(0);
        }
    }

    @Override
    public User createUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public ListPersonal createList(ListPersonal personalList, User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.evict(user);
            personalList.setUser(user);
            session.save(personalList);
            session.getTransaction().commit();
            return personalList;
        }

    }

    @Override
    public boolean checkIfUserExist(User user) {
        if (getAllUsers().contains(user)) {
            return true;
        }
        return false;
    }

    @Override
    public User updateUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.evict(user);
            session.saveOrUpdate(user);
            session.getTransaction().commit();
            return user;
        }
    }

    @Override
    public User deleteUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
            return user;
        }
    }
}

