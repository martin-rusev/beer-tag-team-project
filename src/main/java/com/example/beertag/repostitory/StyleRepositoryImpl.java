package com.example.beertag.repostitory;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class StyleRepositoryImpl implements StyleRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public StyleRepositoryImpl(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }


    public boolean checkIfStyleHasBeers(String styleName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> styleQuery =
                    session.createQuery("from Beer where style.name = :styleName");
            styleQuery.setParameter("styleName", styleName);
            List<Beer> beerByStyle = styleQuery.list();
            return beerByStyle != null && beerByStyle.size() > 0;
        }
    }

    @Override
    public Style getStyleByName(String name) {

        try (Session session = sessionFactory.openSession()) {
            Query<Style> styleByName =
                    session.createQuery("from Style where name =:name");
            styleByName.setParameter("name", name);

//            if (styleByName.list().isEmpty() || styleByName.list().size() == 0) {
//                throw new NotFoundException(name);
//            }
            return styleByName.getSingleResult();
        }
    }

    @Override
    public boolean checkIfStyleExist(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> styleByName =
                    session.createQuery("from Style where name =:name");
            styleByName.setParameter("name", name);


            return !styleByName.list().isEmpty();
        }

    }

    @Override
    public List<Style> getAllStyles() {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> styles =
                    session.createQuery("from Style order by name");
            List<Style> styleList = styles.list();

            if (styleList == null || styleList.isEmpty()) {
                throw new EntityNotFoundException("No Styles in repository");
            }
            return styleList;
        }

    }

    @Override
    public Style create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.saveOrUpdate(style);
            session.getTransaction().commit();
            return style;
        }
    }

    @Override
    public Style update(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(String.valueOf(style.getId()), style);
            session.getTransaction().commit();
            return style;
        }
    }

    @Override
    public Style delete(Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(style);
            session.getTransaction().commit();
            return style;
        }

    }

    @Override
    public Style getStyleById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null) {
                throw new NotFoundException("Style", "ID", String.valueOf(id));
            }
            return style;
        }
    }
}
