package com.example.beertag.repostitory;

import com.example.beertag.models.Brewery;
import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.User;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserRepository {

    Collection<User> getAllUsers();

    User getUserById(int id);

    User createUser(User user);

    User updateUser(User user);

    User deleteUser(User user);

    User getUserByUsername(String username);

    Collection<User> getUsersByName(String username);

    ListPersonal createList(ListPersonal personalList, User user);

    boolean checkIfUserExist(User user);
}
