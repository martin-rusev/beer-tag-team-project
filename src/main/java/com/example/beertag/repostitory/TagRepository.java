package com.example.beertag.repostitory;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Tag;

import java.util.Collection;
import java.util.List;

public interface TagRepository {

    Collection<Tag> getAllTags();

    Tag getTagById(int id);

    Tag createTag(Tag tag);

    void updateTag(int id, Tag tagToUpdate);

    Tag deleteTag(Tag tag);

    Collection<Tag> getTagsByName(String name);

    Tag getTagByName(String name);

    boolean checkIfTagExist(Tag tag);

}
