package com.example.beertag.exeptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


public class DuplicateException extends RuntimeException {

    public DuplicateException(String type, String attribute, String name) {
        super(String.format("%s with %s %s already exists!", type, attribute,name));
    }

    public DuplicateException(String attribute) {
        super(String.format("Beer %s already exists!", attribute));
    }
    public DuplicateException() {
        super(String.format("This beer already exists in your list"));
    }
}
