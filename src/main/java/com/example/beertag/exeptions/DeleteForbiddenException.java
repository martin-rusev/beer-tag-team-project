package com.example.beertag.exeptions;

public class DeleteForbiddenException extends RuntimeException {

    public DeleteForbiddenException(String nameBeer) {
        String.format("You can't delete %s, because someone love it!",nameBeer);
    }


}

