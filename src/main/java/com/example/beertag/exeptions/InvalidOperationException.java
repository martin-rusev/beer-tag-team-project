package com.example.beertag.exeptions;

public class InvalidOperationException extends RuntimeException {
    public InvalidOperationException(String message, int id) {
        super(String.format(message,id));
    }
    public InvalidOperationException(String message, String name) {
        super(String.format(message,name));
    }
}
