package com.example.beertag.exeptions;

public class NotFoundException extends RuntimeException {

    public NotFoundException(String s) {
        super(String.format("%s was not found!",s));
    }

    public NotFoundException(String s, String b) {
        super(String.format("%s %s was not found!",s, b));
    }

    public NotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public NotFoundException(String type, String id, String attribute) {
        super(String.format("%s with %s %s was not found in the Repository!", type, id, attribute));
    }
}
