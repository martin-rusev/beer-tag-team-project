package com.example.beertag.controllers.rest;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.models.modelsUtils.StringToEnum;
import com.example.beertag.services.BeerService;
import com.example.beertag.services.ListService;
import com.example.beertag.services.UserService;
import com.example.beertag.models.dto.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private UserService userService;
    private ListService listService;
    private BeerService beerService;
    private Mapper mapper;

    @Autowired
    public UserRestController(UserService userService, Mapper mapper,
                              ListService listService, BeerService beerService) {
        this.userService = userService;
        this.listService = listService;
        this.mapper = mapper;
        this.beerService = beerService;
    }

    @GetMapping("/show")
    public Collection<User> getAllUsers(@RequestParam(required = false) String username) {
        try {
            if (username != null && !username.isEmpty()) {
                return userService.getUsersByName(username);
            }
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        try {
            return userService.getAllUsers();
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/{id}")
    public User getUserById(@PathVariable(name = "id") int id) {
        try {
            return userService.getUserById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This user was not found in the repository!");
        }
    }

    @PostMapping("/create")
    public User create(@RequestBody @Valid User user) {
        try {
            return userService.createUser(user);
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public void update(@PathVariable(name = "id") int userId,
                       @RequestBody @Valid User newUser,
                       @RequestParam(required = false) String username,
                       @RequestParam(required = false) String firstName,
                       @RequestParam(required = false) String lastName,
                       @RequestParam(required = false) String password,
                       @RequestParam(required = false) String email) {
        try {
            User oldUser = userService.getUserById(userId);
            User userToUpdate = mapper.mergeUsers(oldUser, newUser);
            userService.updateUser(userToUpdate);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/profile/list")
    public ListPersonal createList(
            @RequestHeader(name = "Authorization") String authorization,
            @RequestParam(name = "type") String type) {

        StringToEnum stringToListPersonal = new StringToEnum();
        ListType listType = stringToListPersonal.convert(type);
        ListPersonal personalList = new ListPersonal();
        personalList.setType(listType);

        try {
            User oldUser = userService.getUserByUsername(authorization);
            return userService.createList(personalList, oldUser);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @PutMapping("/{userId}/list/remove")
    public ListPersonal deleteBeerFromList(@PathVariable(name = "userId") int userId,
                                           @RequestParam(name = "beerId") int beerId,
                                           @RequestParam(name = "listId") int listId,
                                           @RequestHeader(name = "Authorization") String authorization) {
        try {

            ListPersonal listBeers = listService.getListById(listId);
            User ownerList=listBeers.getUser();
            User user = userService.getUserByUsername(authorization);

            userService.isOwnerOrAdmin(user.getUsername(),user);
            Beer beer = beerService.getBeerById(beerId);
            listBeers.removeBeer(beer);
            listService.updateList(listBeers);
            return listBeers;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/list/{id}/delete")
    public ListPersonal deleteList( @RequestHeader(name = "Authorization") String authorization,
                                   @PathVariable(name = "id") int id) {
        try {
            User user = userService.getUserByUsername(authorization);
            ListPersonal listBeers = listService.getListById(id);
            userService.isOwnerOrAdmin(listBeers.getUser().getUsername(),user);
            return listService.deleteList(listBeers);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/delete/{id}")
    public User deleteUser(@PathVariable(name = "id") int id) {
        try {
            return userService.deleteUser(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }



//    @PutMapping("/changeName/{id}{name}")
//    public String updateUser(@PathVariable int id,@PathVariable String newName) {
//        if (!BeerTagServiceImpl.getUserMap().containsKey(id)) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This user doesn't exist");
//        }
//        BeerTagServiceImpl.getUserMap().get(id).setName(newName);
//        return null;
//    }


//    @PostMapping("/add/{userId}/wishlist/{beerId}")
//    public String addBeerWishList(@PathVariable("userId") long userId, @PathVariable("beerId") long beerId) {
//        if (!BeerTagServiceImpl.getUserMap().containsKey(userId)) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This user doesn't exist");
//        }
//        if (!BeerTagServiceImpl.getBeerMap().containsKey(beerId)) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "This beer doesn't exist");
//        }
//
//        UserImpl user = BeerTagServiceImpl.getUserMap().get(userId);
//        BeerImpl beer = BeerTagServiceImpl.getBeerMap().get(beerId);
//
//        BeerTagServiceImpl.getUserMap().get(userId).addBeerToWishList(beer);
//
//        return String.format("Beer %s was added to %s's wishlist",beer.getName(),user.getName());
//    }
//
//    @PostMapping("/addbeerdranklist")
//    public long addBeerDrankList(@RequestBody @Valid BeerImpl beer) {
//
//        return beer.getId();
//    }
//
////    @GetMapping("/wishlist/{id}")
////    public List<BeerImpl> getWishListForUser(@PathVariable long id) {
////        UserImpl user = BeerTagService.getUserMap().getOrDefault(id, null);
////        return user.getWishListBeers();
////    }
//
//    @GetMapping("/dranklist/{id}")
//    public List<BeerImpl> getDrankListForUser(@PathVariable Long id) {
//
//        return null;
//    }
//

}
