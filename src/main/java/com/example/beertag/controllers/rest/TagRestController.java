package com.example.beertag.controllers.rest;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Tag;
import com.example.beertag.services.TagService;
import com.example.beertag.models.dto.TagDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {
    private TagService tagService;

    @Autowired
    public TagRestController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping("/show")
    public Collection<Tag> getAllTags(@RequestParam(required = false) String name) {
        try {
            if (name != null && !name.isEmpty()) {
                return tagService.getTagByName(name);
            }
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        try {
            return tagService.getAllTags();
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Tag getTagById(@PathVariable(name = "id") int id) {
        try {
            return tagService.getTagById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/create")
    public Tag add(@RequestBody @Valid TagDto tagDto) {
        try {
            Tag tagToCreate = new Tag();
            tagToCreate.setName(tagDto.getName());
            tagService.createTag(tagToCreate);
            return tagToCreate;
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PutMapping("/update/{id}")
    public Tag update(@PathVariable(name = "id") int id,
                      @RequestBody @Valid Tag newTag) {
        try {
            Tag oldTag = tagService.getTagById(id);
            oldTag.setName(newTag.getName());
            tagService.updateTag(id,oldTag);
            return oldTag;
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @DeleteMapping("/delete/{id}")
    public Tag deleteTag(@PathVariable(name = "id") int id) {
        try {
            return tagService.deleteTag(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
