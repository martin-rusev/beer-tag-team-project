package com.example.beertag.controllers.rest;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.services.BeerService;
import com.example.beertag.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {
    private StyleService styleService;
    private BeerService beerService;

    @Autowired
    public StyleRestController(StyleService styleService,
                               BeerService beerService) {
        this.styleService = styleService;
        this.beerService = beerService;
    }

    @GetMapping("/show")
    public Collection<Style> getAllStyles() {
        try {
            return styleService.allStyles();
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{name}")
    public Style getStyleByName(@PathVariable(name = "name") String name) {
        try {
            return styleService.getStyleByName(name);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/create")
    public Style create(@RequestBody @Valid Style style) {
        try {
            Style styleToCreate = new Style();
            styleToCreate.setName(style.getName());
            styleService.create(styleToCreate);
            return styleToCreate;
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{name}")
    public Style updateStyle(@PathVariable(name = "name") String name,
                             @RequestBody @Valid Style style) {
        try {
            Style styleToCreate = styleService.getStyleByName(name);
            styleToCreate.setName(style.getName());
            styleService.update(styleToCreate);
            return styleToCreate;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/beers/show")
    public Map<Style, List<Beer>> getAllPopularStyles() {
        try {
            List<Style> styleList = styleService.allStyles();
            Map<Style, List<Beer>> styleBeers = new HashMap<>();
            for (Style style : styleList
            ) {
                List<Beer> tempList = new ArrayList<>();
                if (styleService.checkIfStyleHasBeers(style.getName())) {
                    tempList =
                            beerService.getBeersByStyle(style.getName(), "name");
                    styleBeers.put(style, tempList);
                }
            }
            return styleBeers;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @GetMapping("/beers/count")
    public Map<Style, Integer> getAllStylesBeerCount() {
        try {
            List<Style> styleList = styleService.allStyles();
            Map<Style, Integer> styleBeersCount = new TreeMap<>();
            for (Style style : styleList) {
                List<Beer> tempList = new ArrayList<>();
                if (styleService.checkIfStyleHasBeers(style.getName())) {
                    tempList =
                            beerService.getBeersByStyle(style.getName(), "name");
                    styleBeersCount.put(style, tempList.size());
                }
                styleBeersCount = styleBeersCount.entrySet().stream()
                        .sorted(Map.Entry.<Style, Integer>comparingByValue())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            }
            return styleBeersCount;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Style deleteStyle(@PathVariable(name = "id") int id) {
        try {
            Style style = styleService.getStyleById(id);
            styleService.delete(style);
            return style;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
