package com.example.beertag.controllers.rest;


import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.models.dto.BeerDto;
import com.example.beertag.models.dto.Mapper;
import com.example.beertag.services.*;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@Api
@RestController
@RequestMapping("/api/beers")
public class BeerRestController {
    private BeerService beerService;
    private Mapper mapper;
    private UserService userService;
    private ListService listService;
    private BreweryService breweryService;
    private RatingService ratingService;
    private StyleService styleService;

    @Autowired
    public BeerRestController(BeerService beerService, Mapper mapper,
                              UserService userService, BreweryService breweryService,
                              ListService listService,
                              RatingService ratingservice,
                              StyleService styleService) {
        this.beerService = beerService;
        this.mapper = mapper;
        this.userService = userService;
        this.breweryService = breweryService;
        this.listService = listService;
        this.ratingService = ratingservice;
        this.styleService = styleService;
    }

    @GetMapping("/")
    public List<Beer> getBeerByName(@RequestParam(value = "name") String name) {
        try {
            return beerService.getBeersByName(name,"name");
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Beer getBeerById(@PathVariable(name = "id") int id) {
        try {
            return beerService.getBeerById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/show")
    public Collection<Beer> getBeers(
            @RequestParam(value = "brewery", required = false) String brewery,
            @RequestParam(value = "orderBy", defaultValue = "name") String orderBy,
            @RequestParam(value = "style", required = false) String style,
            @RequestParam(value = "tag", required = false) String tagName) {
        try {

            if (style != null && !style.isEmpty()) {
                Style styleToCheck=styleService.getStyleByName(style);
                List<Beer> beerList=beerService.getBeersByStyle(style, orderBy);
                return beerService.getBeers("Beers","style",style, beerList);
            }
            else if (brewery != null && !brewery.isEmpty()) {
                int breweryId = breweryService.getBreweryName(brewery).getId();
                List<Beer> beerList=beerService.getBreweryBeers(breweryId);
                return beerService.getBeers("Beers","brewery",brewery, beerList);
            }
            else if (tagName != null && !tagName.isEmpty()) {
              List<Beer> beerList= beerService.getBeersByTag(tagName);
                return beerService.getBeers("Tag","contain",tagName, beerList);
            }
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        try {
            return beerService.getListBeers();
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There are no registered beers in BEER TAG app");
        }
    }





    @GetMapping("/sort")
    public Collection<Beer> getBeers(@RequestParam(value = "abv", required = false) String abv,
                                     @RequestParam(value = "rating", required = false) String rating) {

        if (abv != null && !abv.isEmpty()) {
            return beerService.getSortedBeersByAbv();
        }
        return beerService.getListBeers();
    }



    @RequestMapping(value = "/create",method = RequestMethod.POST)

    public Beer createRestBeer(@RequestBody @Valid BeerDto dto,
                    @RequestHeader(name = "Authorization") String authorization) {
        try {
            Beer beer = mapper.convertToBeer(dto);
            User beerCreator = userService.getUserByUsername(authorization);

            beerService.createBeer(beer, beerCreator);
            return beer;
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PutMapping("/update/{id}")
    public Beer update(@PathVariable(name = "id") int id,
                       @RequestBody @Valid BeerDto beerDto,
                       @RequestHeader(name = "Authorization") String authorization) {
        try {
            Beer oldBeer = beerService.getBeerById(id);
            Beer beerToUpdate = mapper.convertToBeer(beerDto);
            User user = userService.getUserByUsername(authorization);

            beerToUpdate = mapper.mergeBeers(oldBeer, beerToUpdate);

            return beerService.updateBeer(beerToUpdate,user );

        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{id}/rate")
    public BeerRating rateBeer(@PathVariable(name = "id") int id,
                               @RequestHeader(name = "rating") String ratingValue,
                               @RequestHeader(name = "authorization") String authorization) {

        try {
            Beer beer = beerService.getBeerById(id);
            Rating rating = Rating.valueOf(ratingValue);
            User user = userService.getUserByUsername(authorization);
            beer.setTempRating(ratingService.getRatingMVC(beer));
            beerService.updateBeerTagingRating(beer);
            return ratingService.addRating(beer, rating, user);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @GetMapping("/{id}/rating")
    public double rateBeer(@PathVariable(name = "id") int id) {
        try {
            Beer beer = beerService.getBeerById(id);
            return ratingService.getRating(beer);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/toList/{listId}")
    public ListPersonal addBeerToList(@PathVariable(name = "id") int beerId,
                                      @PathVariable(name = "listId") int listId,
                                      @RequestHeader(name = "Authorization") String authorization) {
        try {
            Beer beerAdd = beerService.getBeerById(beerId);
            User beerCreator = userService.getUserByUsername(authorization);
            ListPersonal listPersonal = listService.getListById(listId);
            listPersonal.addBeer(beerAdd);
            return listService.updateList(listPersonal);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch (DuplicateException e){
            throw new ResponseStatusException(HttpStatus.CONFLICT,e.getMessage());
        }
    }



    @DeleteMapping("/{id}/delete")
    public Beer deleteBeer(@PathVariable(name = "id") int id,
                           @RequestHeader(name = "Authorization") String authorization) {
        try {
            User beerOwner = userService.getUserByUsername(authorization);
            Beer beerToDelete = beerService.getBeerById(id);
            beerService.deleteBeer(beerToDelete, beerOwner);
            return beerToDelete;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}