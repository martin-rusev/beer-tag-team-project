package com.example.beertag.controllers.rest;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Country;
import com.example.beertag.services.BreweryService;
import com.example.beertag.services.CountryService;
import com.example.beertag.utils.BreweryUtils;
import com.example.beertag.models.dto.BreweryDto;
import com.example.beertag.models.dto.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryRestController {
    private BreweryService breweryService;
    private CountryService countryService;
    private Mapper mapper;

    @Autowired
    public BreweryRestController(BreweryService breweryService, CountryService countryService, Mapper mapper) {
        this.breweryService = breweryService;
        this.countryService = countryService;
        this.mapper = mapper;
    }

    @GetMapping("/show")
    public Collection<Brewery> getAllBreweries(@RequestParam(required = false) String name) {
        try {
            if (name != null && !name.isEmpty()) {
                return breweryService.getCollectionBreweryByName(name);
            }
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        try {
            return breweryService.getAllBreweries();
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Brewery getBreweryById(@PathVariable(name = "id") int id) {
        try {
            return breweryService.getBreweryById(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/countries/{country}")
    public List<Brewery> getBreweriesByCounty(@PathVariable(name = "country") String country) {
        try {
            return breweryService.getBreweriesByCountry(country);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    //TODO dont throw DuplicateExc if brewery already exist!
    @PostMapping("/create")
    public Brewery create(@RequestBody @Valid BreweryDto breweryDto) {
        try {

            Country country = countryService.getCountryById(breweryDto.getCountryId());
            Brewery breweryToCreate =
                    BreweryUtils.createBrewery(country, breweryDto.getName());
            breweryService.createBrewery(breweryToCreate);
            return breweryToCreate;
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public Brewery update(@PathVariable(name = "id") int id,
                          @RequestBody @Valid BreweryDto breweryDto) {
        try {
            Brewery oldBrewery = breweryService.getBreweryById(id);
            Brewery breweryToUpdate = mapper.mergeBreweries(oldBrewery, breweryDto);
            breweryService.updateBrewery(breweryToUpdate);
            return breweryToUpdate;
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/delete/{id}")
    public Brewery deleteBrewery(@PathVariable(name = "id") int id) {
        try {
            return breweryService.deleteBrewery(id);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
