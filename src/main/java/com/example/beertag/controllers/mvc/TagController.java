package com.example.beertag.controllers.mvc;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;
import com.example.beertag.services.BeerService;
import com.example.beertag.services.TagService;
import com.example.beertag.models.dto.TagDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Collection;

@Controller
public class TagController {
    private TagService tagService;
    private BeerService beerService;

    @Autowired
    public TagController(TagService tagService,BeerService beerService) {

        this.tagService = tagService;
        this.beerService=beerService;
    }


    @GetMapping("auth/beers/{id}/addtag")
    public String tagBeerForm(@PathVariable int id, Model model) {
        Beer beer = beerService.getBeerById(id);
        Collection<Tag> tagList = tagService.getAllTags();
        model.addAttribute("tags", tagList);
        model.addAttribute("beer", beer);
        model.addAttribute("tag", new Tag());
        return "beer-tag";
    }

    @PostMapping("auth/beers/{id}/tag")
    public String tagBeer(@PathVariable int id, @ModelAttribute Tag tag,
                          BindingResult errors) {
        if (errors.hasErrors()) {
            return "beer-tag";
        }
        Beer beerToTag = beerService.getBeerById(id);
        Tag tagDB = tagService.getTagById(tag.getId());
        beerToTag.addTag(tagDB);
        Beer beer1 = beerService.updateBeerTagingRating(beerToTag);
        return "redirect:/auth/beers/" + beer1.getId();
    }
}
