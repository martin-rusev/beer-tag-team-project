package com.example.beertag.controllers.mvc;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.services.BeerService;
import com.example.beertag.services.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class StyleController {
    private StyleService styleService;
    private BeerService beerService;

    @Autowired
    public StyleController(StyleService styleService,
                           BeerService beerService) {

        this.styleService = styleService;
        this.beerService = beerService;
    }

    //TODO better url
    @GetMapping("/styles/popular")
    public String getAllPopularStyles(Model model) {
        try {

            Map<Style, List<Beer>> styleBeers = styleService.getBeersPopularStyles();
            Map<Style, Integer> stylePopular = styleService.getStylesBeersCount();
            //TODO sort
            stylePopular = stylePopular.entrySet().stream()
                    .sorted(Map.Entry.<Style, Integer>comparingByValue().reversed()).limit(5)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
            styleBeers = styleBeers.entrySet().stream().limit(5)
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            model.addAttribute("stylesPopular", stylePopular);
            model.addAttribute("stylesBeers", styleBeers);

        } catch (NotFoundException e) {
            model.addAttribute("error", e);
        }
        return "style-grid-catalog";
    }

    @GetMapping("/styles")
    public  String getAllStyles(Model model){
        List<Style> allstyles=styleService.allStyles().subList(0,11);
        model.addAttribute("styles",allstyles);
        return "style-grid-catalog";
    }

    @GetMapping("/styles/addForm")
    public String createBeerForm(Model model) {
        model.addAttribute("style", new Style());
        return "newStyleForm";
    }


    @PostMapping("/styles/add")
    public String createStyle(@Valid @ModelAttribute Style style, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            return "newStyleForm";
        }
        Style  style1 =new Style();

        try {
            style1 = styleService.create(style);
        } catch (DuplicateException ex) {
            model.addAttribute("error", ex);
            return "error";
        }
        return "redirect:/styles/" + style1.getId();
    }

    @GetMapping("/styles/{id}")
    public String showStyle(@PathVariable int id, Model model) {
        Style style = styleService.getStyleById(id);
        model.addAttribute("style", style);
        if (beerService.styleHasBeers(style)) {
            model.addAttribute("beers", beerService.getBeersByStyle(style.getName(), "name"));
        }
        return "stylePage";
    }
}
