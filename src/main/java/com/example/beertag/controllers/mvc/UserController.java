package com.example.beertag.controllers.mvc;

import com.example.beertag.controllers.rest.BeerRestController;
import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.models.dto.UserDto;
import com.example.beertag.models.dto.UserUpdateDTO;
import com.example.beertag.models.modelsUtils.StringToEnum;
import com.example.beertag.repostitory.ListRepository;
import com.example.beertag.services.BeerService;
import com.example.beertag.services.ListService;
import com.example.beertag.services.UserService;
import com.example.beertag.models.dto.Mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Controller
//@RequestMapping("/users")
public class UserController {
    private UserService userService;
    private PasswordEncoder passwordEncoder;
    private BeerService beerService;
    private ListService listService;

    @Autowired
    public UserController(UserService userService, PasswordEncoder passwordEncoder,
                          BeerService beerService, ListService listService) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
        this.beerService = beerService;
        this.listService = listService;
    }




    @GetMapping("/auth/users/{id}")
    public String showUserById(@PathVariable int id, Model model) {
        model.addAttribute("user", userService.getUserById(id));
        return "userfeauture";
    }

    @GetMapping("/auth/users/{id}/modify")
    public String showModifyingPage(Model model,Principal principal) {
        model.addAttribute("userUpdateDTO", new UserUpdateDTO());
        try {
            User user = userService.getUserByUsername(principal.getName());

            userService.isOwnerOrAdmin(user.getUsername(), user);

        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return "modifyUser";
    }

    @PostMapping("/auth/users/modify")
    public String updateUser(@Valid @ModelAttribute UserUpdateDTO userUpdateDTO,
                             Principal principal) {

        String userName = principal.getName();
        User userToUpdate = userService.getUserByUsername(userName);

        if (userUpdateDTO.getEmail().length() > 0) {
            userToUpdate.setEmail(userUpdateDTO.getEmail());
        }
        if (userUpdateDTO.getFirstName().length() > 0) {
            userToUpdate.setFirstName(userUpdateDTO.getFirstName());
        }
        if (userUpdateDTO.getLastName().length() > 0) {
            userToUpdate.setLastName(userUpdateDTO.getLastName());
        }
        if (userUpdateDTO.getPassword().length() > 0) {
            userToUpdate.setPassword(passwordEncoder.encode(userUpdateDTO.getPassword()));
        }

        userService.updateUser(userToUpdate);

        return "modifyUser-confirmation";
    }

    @ModelAttribute(name = "listTypes")
    public void typrList(Model model) {
        Collection<ListType> listTypes = Arrays.asList(ListType.values());
        model.addAttribute("listType",listTypes );
    }



    @GetMapping("/auth/beers/{id}/wish/add")
    public String addBeerToWishList(@PathVariable(name = "id") int beerId,
                                    Principal principal,Model model) {

        Beer beerAdd = beerService.getBeerById(beerId);
        User user = userService.getUserByUsername(principal.getName());
        ListPersonal listPersonal = listService.getListByTypeUser(user, ListType.WISHLIST);
        try {
            listPersonal.addBeer(beerAdd);
            listService.updateList(listPersonal);
        }catch (DuplicateException e){
            model.addAttribute("error",e);
            return "error";
        }

        return "redirect:/auth/users/"+ user.getId()+"/wish";
    }

    @GetMapping("/auth/beers/{id}/drunk/add")
    public String addBeerToDrunkList(@PathVariable(name = "id") int beerId,
                                     Principal principal,Model model) {

        Beer beerAdd = beerService.getBeerById(beerId);
        User user = userService.getUserByUsername(principal.getName());
        ListPersonal listPersonal = listService.getListByTypeUser(user, ListType.DRUNKLIST);
        try {
            listPersonal.addBeer(beerAdd);
            listService.updateList(listPersonal);
        }catch (DuplicateException e){
            model.addAttribute("error",e);
            return "error";
        }
        return "redirect:/auth/users/"+ user.getId()+"/drunk";
    }

    @GetMapping("/auth/users/{id}/drunk")
    public String showDrunkList(@PathVariable (name = "id") int id, Model model,
                                Principal principal) {

        User user = userService.getUserByUsername(principal.getName());
        ListPersonal listPersonal = listService.getListByTypeUser(user, ListType.DRUNKLIST);
        model.addAttribute("list",listPersonal);
        return "beer-grid-list";
    }
    @GetMapping("/auth/users/{id}/wish")
    public String showWishList(@PathVariable (name = "id") int id,Model model,
                               Principal principal) {

        User user = userService.getUserByUsername(principal.getName());
        ListPersonal listPersonal = listService.getListByTypeUser(user, ListType.WISHLIST);
        model.addAttribute("list",listPersonal);
        return "beer-grid-list";
    }




}

