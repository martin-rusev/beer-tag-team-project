package com.example.beertag.controllers.mvc;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.ListType;
import com.example.beertag.models.Role;
import com.example.beertag.models.User;
import com.example.beertag.models.dto.UserDto;
import com.example.beertag.repostitory.RoleRepository;
import com.example.beertag.services.ListService;
import com.example.beertag.services.RoleService;
import com.example.beertag.services.RoleServiceImpl;
import com.example.beertag.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UserService userService;
    private RoleService roleService;
    private ListService listService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder,
                                  UserService userService, RoleService roleService,ListService listService) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
        this.roleService = roleService;
        this.listService=listService;
    }


    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserDto userDto, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can not be empty!");
            return "register";
        }
        if (userDetailsManager.userExists(userDto.getUsername())) {
            model.addAttribute("error", "User with the same username already exist!");
            return "register";
        }
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");

        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getUsername(),
                        passwordEncoder.encode(userDto.getPassword()),
                        authorities);
        userDetailsManager.createUser(newUser);



        User userToCreate = userService.getUserByUsername(newUser.getUsername());


        for (GrantedAuthority authority:authorities
        ) {
         Role role=roleService.createRole(authority.getAuthority());
         userToCreate.setRole(role);
        }

        listService.createList(ListType.WISHLIST,userToCreate);
        listService.createList(ListType.DRUNKLIST,userToCreate);
        userToCreate.setEmail(userDto.getEmail());
        userToCreate.setFirstName(userDto.getFirstName());
        userToCreate.setLastName(userDto.getLastName());
        userToCreate.setUsername(userDto.getUsername());

        try {
            userService.updateUser(userToCreate);

        } catch (DuplicateException e) {
            model.addAttribute("error", e.getMessage());
        }

        return "register-confirmation";
    }



    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register";
    }
}
