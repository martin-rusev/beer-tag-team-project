package com.example.beertag.controllers.mvc;

import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.services.BeerService;
import com.example.beertag.services.ImageService;
import com.example.beertag.services.UserService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class ImageController {
    BeerService beerService;
    ImageService imageService;
    UserService userService;

    public ImageController(BeerService beerService,
                           ImageService imageService,
                           UserService userService) {
        this.beerService = beerService;
        this.imageService = imageService;
        this.userService = userService;
    }

    @GetMapping("/beers/{id}/image")
    public String showPictureForm(@PathVariable int id, Model model) {
        model.addAttribute("beer", beerService.getBeerById(id));
        return "uploadFormBeers";
    }

    @PostMapping("/beers/{id}/image")
    public String saveBeerImage(@PathVariable int id, @RequestParam("imagefile") MultipartFile multipartFile) {
        imageService.saveImgFile(id, multipartFile);
        return "redirect:/auth/beers";
    }

    @GetMapping("/beer/{id}/imagefile")
    public void showPicture(@PathVariable int id, HttpServletResponse httpServletResponse) throws IOException {

        Beer beer = beerService.getBeerById(id);
        if (beer.getPicture() != null) {
            byte[] byteArr = new byte[beer.getPicture().length];
            int i = 0;

            for (Byte wrappedByte:beer.getPicture()){
                byteArr[i++]=wrappedByte;
            }
            httpServletResponse.setContentType("image/jpg");
            InputStream stream=new ByteArrayInputStream(byteArr);
            IOUtils.copy(stream,httpServletResponse.getOutputStream());
        }
    }

    @GetMapping("/users/{id}/image")
    public String showPictureUserForm(@PathVariable int id, Model model) {
        model.addAttribute("user", userService.getUserById(id));
        return "uploadFormUsers";
    }

    @PostMapping("/users/{id}/image")
    public String saveUserImage(@PathVariable int id, @RequestParam("imagefile") MultipartFile multipartFile) {
            imageService.saveUserImgFile(id, multipartFile);
            return "redirect:/auth/users/{id}";
    }

    @GetMapping("/users/{id}/imagefile")
    public void showUserPicture(@PathVariable int id, HttpServletResponse httpServletResponse) throws IOException {

        User user = userService.getUserById(id);
        if (user.getPicture() != null) {
            byte[] byteArr = new byte[user.getPicture().length];
            int i = 0;

            for (Byte wrappedByte:user.getPicture()){
                byteArr[i++]=wrappedByte;
            }
            httpServletResponse.setContentType("image/jpg");
            InputStream stream=new ByteArrayInputStream(byteArr);
            IOUtils.copy(stream,httpServletResponse.getOutputStream());
        }
    }
}
