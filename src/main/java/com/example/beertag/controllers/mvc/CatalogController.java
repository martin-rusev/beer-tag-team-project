package com.example.beertag.controllers.mvc;

import com.example.beertag.exeptions.DeleteForbiddenException;
import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.models.*;
import com.example.beertag.models.dto.Mapper;
import com.example.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Controller

public class CatalogController {
    private BeerService beerService;


    @Autowired
    public CatalogController(BeerService beerService) {
        this.beerService = beerService;
    }

    @GetMapping("/catalog/beers")//button browse
    public String getBeers(Model model) {
        List<Beer> list = beerService.getListBeers();
        model.addAttribute("beers", list);
        return "beer-grid-catalog";
    }

    @GetMapping("/beer/{id}")
    public String showBeerById(@PathVariable int id, Model model) {
        Beer beer = beerService.getBeerById(id);
        model.addAttribute("beer", beer);
        return "beer-user";
    }


    @GetMapping("/catalog/beers/sort")
    public String getBeersSorted(@RequestParam(name = "param", required = false) String param,
                                 Model model) {
        Collection<Beer> list = new ArrayList<>();
        if (param.equals("abv")) {
            list = beerService.getSortedBeersByAbv();
            model.addAttribute("beers", list);
        } else if (param.equals("rating")) {
            list = beerService.getSortedBeersByRating();
            model.addAttribute("beers", list);
        } else if (param.equals("name")) {
            list = beerService.getListBeers();
            model.addAttribute("beers", list);
        }
        return "beer-grid-catalog";
    }

}