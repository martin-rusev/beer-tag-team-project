package com.example.beertag.controllers.mvc;

import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.services.*;
import com.example.beertag.models.dto.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

//
@Controller
public class HomeController {
    private BeerService beerService;
    private Mapper mapper;
    private UserService userService;
    private ListService listService;
    private BreweryService breweryService;
    private RatingService ratingService;

    @Autowired
    public HomeController(BeerService beerService, Mapper mapper,
                          UserService userService, BreweryService breweryService,
                          ListService listService,
                          RatingService ratingservice) {
        this.beerService = beerService;
        this.mapper = mapper;
        this.userService = userService;
        this.breweryService = breweryService;
        this.listService = listService;
        this.ratingService = ratingservice;
    }


    @GetMapping("/")
    public String home(Model model, @AuthenticationPrincipal Principal principal) {
        List<Beer> list = beerService.getListBeers();
        model.addAttribute("beers", list);
        if (principal!=null){
            User user=userService.getUserByUsername(principal.getName());
            model.addAttribute("user",user.getId());
        }else {

            model.addAttribute("user",1);
        }
        return "index";
    }


}