package com.example.beertag.controllers.mvc;

import com.example.beertag.models.*;
import com.example.beertag.models.dto.Mapper;
import com.example.beertag.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collection;
import java.util.List;

@Controller
public class SearchController {
    private BeerService beerService;
    private StyleService styleService;
    private CountryService countryService;
    private TagService tagService;

    @Autowired
    public SearchController(BeerService beerService,
                            StyleService styleService,
                            CountryService countryService,
                            TagService tagService) {

        this.beerService = beerService;
        this.styleService = styleService;
        this.countryService = countryService;
        this.tagService = tagService;
    }


    @GetMapping("/beers/search")
    public String findForm(Model model) {
        model.addAttribute("beer", new Beer());
        model.addAttribute("country", new Country());
        model.addAttribute("style", new Style());
        model.addAttribute("tag", new Tag());
        List<Style> styleList = styleService.allStyles();
        model.addAttribute("styles", styleList);
        List<Country> countryList = countryService.getAllCountries();
        model.addAttribute("countries", countryList);
        Collection <Tag> tagList = tagService.getAllTags();
        model.addAttribute("tags",tagList);
        return "search-form";
    }


    @PostMapping("/beers/by/name")
    public String showBeersByName(Beer beer, Model model) {
        List<Beer> beers = beerService.getBeersByName(beer.getName(), "name");

        model.addAttribute("beers", beers);

        return "beer-grid-catalog";
    }

    @PostMapping("/beers/by/style")
    public String showBeersByStyle(Style style, Model model) {

        List<Beer> beers = beerService.getBeersByStyle(style.getName(), "name");

        model.addAttribute("beers", beers);

        return "beer-grid-catalog";
    }

    @PostMapping("/beers/by/country")
    public String showBeersByCountry(Country country, Model model) {

        List<Beer> beers = countryService.
                getBeersByCountry(country.getName(), "name");

        model.addAttribute("beers", beers);

        return "beer-grid-catalog";
    }

    @PostMapping("/beers/by/tag")
    public String showBeersByName(Tag tag, Model model) {
        List<Beer> beers = beerService.getBeersByTag(tag.getName());
        model.addAttribute("beers", beers);
        return "beer-grid-catalog";
    }



}