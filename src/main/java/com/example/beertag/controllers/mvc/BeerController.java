package com.example.beertag.controllers.mvc;

import com.example.beertag.exeptions.DeleteForbiddenException;
import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.services.*;
import com.example.beertag.models.dto.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Controller

public class BeerController {
    private BeerService beerService;
    private Mapper mapper;
    private UserService userService;
    private BreweryService breweryService;
    private RatingService ratingService;
    private StyleService styleService;
    private TagService tagService;

    @Autowired
    public BeerController(BeerService beerService, Mapper mapper,
                          UserService userService, BreweryService breweryService,
                          RatingService ratingservice,
                          StyleService styleService,
                          TagService tagService) {

        this.beerService = beerService;
        this.mapper = mapper;
        this.userService = userService;
        this.breweryService = breweryService;
        this.ratingService = ratingservice;
        this.styleService = styleService;
        this.tagService = tagService;
    }

    @GetMapping("/auth/beers")
    public String getBeers(Model model) {
        List<Beer> list = beerService.getListBeers();
        model.addAttribute("beers", list);
        return "beerGrid";
    }

    @GetMapping("/auth/beers/{id}")
    public String showBeerById(@PathVariable int id, Model model) {

        try {
            Beer beer = beerService.getBeerById(id);
            model.addAttribute("beer", beer);
        } catch (NotFoundException e) {
            model.addAttribute("error", e);
        }

        model.addAttribute("beerRating", new BeerRating());

        return "beer-featured";
    }

    @ModelAttribute(name = "ratings")
    public void addRatingList(Model model) {
        List<Rating> ratings = Arrays.asList(Rating.values());
        model.addAttribute("ratings", ratings);
    }

    @ModelAttribute(name = "styles")
    public void addStyleList(Model model) {
        List<Style> styleList = styleService.allStyles();
        model.addAttribute("styles", styleList);
    }

    @ModelAttribute(name = "breweries")
    public void addBrewerieList(Model model) {
        Collection<Brewery> breweryList = breweryService.getAllBreweries();
        model.addAttribute("breweries", breweryList);
    }

    @GetMapping("/auth/beers/add")
    public String createBeerForm(Model model) {
        model.addAttribute("beer", new Beer());
        return "newBeerForm";
    }

    @PostMapping("/auth/beers/add")
    public String createBeer(@ModelAttribute @Valid Beer beer, BindingResult errors,
                             Model model, Principal principal) {

        if (errors.hasErrors()) {
            return "newBeerForm";
        }

        Beer beer1 = new Beer();

        mapper.mapModelToBeer(beer);
        User user = userService.getUserByUsername(principal.getName());
        try {
            beer1 = beerService.createBeer(beer, user);
        } catch (DuplicateException ex) {
            model.addAttribute("error", ex);
            return "error";
        }
        return "redirect:/auth/beers/" + beer1.getId();
    }

    @GetMapping("/auth/beers/{id}/edit")
    public String editBeerForm(@PathVariable("id") int id, Model model,
                               Principal principal) {

//        if (result.hasErrors()) {
//            return "error";
//        }
        Beer oldBeer;
        try {
            oldBeer = beerService.getBeerById(id);
            User user = userService.getUserByUsername(principal.getName());
            userService.isOwnerOrAdmin(oldBeer.getCreatedBy().getUsername(), user);

        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        } catch (NotFoundException e) {
            model.addAttribute("error", e);
            return "error";
        }
        model.addAttribute("oldBeer", oldBeer);
        model.addAttribute("beer", new Beer());
        return "beer-edit";
    }


    @PostMapping("/auth/beers/{id}/edit")
    public String editBeer(@PathVariable("id") int id, Beer beer,
                           Model model, Principal principal) {


        Beer oldBeer = beerService.getBeerById(id);
        Beer newBeer = mapper.mapModelToBeer(beer);
        Beer beerToUpdate = mapper.mergeBeers(oldBeer, newBeer);
        User beerCreator = userService.getUserByUsername(principal.getName());

        try {
            beerService.updateBeer(beerToUpdate, beerCreator);
        } catch (DuplicateException ex) {
            model.addAttribute("error", ex);
            return "error";
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);

        }
        return "redirect:/auth/beers/" + oldBeer.getId();
    }

    @RequestMapping("/auth/beers/{id}/delete")
    public String deleteBeer(@PathVariable(name = "id") int id, Principal principal, Model model) {

        try {
            User beerOwner = userService.getUserByUsername(principal.getName());
            Beer beerToDelete = beerService.getBeerById(id);
            userService.isOwnerOrAdmin(beerToDelete.getCreatedBy().getUsername(), beerOwner);
            beerService.safeDelete(beerToDelete);
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        } catch (NotFoundException e) {
            model.addAttribute("error", e);
            return "error";
        } catch (DeleteForbiddenException b) {
            model.addAttribute("error", b);
            return "errorDelete";
        }
        return "redirect:/auth/beers";
    }


    @PostMapping("/auth/{id}/rate")
    public String rateBeer(@PathVariable int id, @ModelAttribute BeerRating rating,
                           BindingResult errors, Principal principal) {
        if (errors.hasErrors()) {
            return "error";
        }

        Beer beerToRate = beerService.getBeerById(id);
        User user = userService.getUserByUsername(principal.getName());
        rating.setBeer(beerToRate);
        rating.setUser(user);
        beerToRate.setTempRating(ratingService.getRatingMVC(beerToRate));
        beerService.updateBeerTagingRating(beerToRate);
        ratingService.addRating(rating);

        return "redirect:/auth/beers/" + beerToRate.getId();
    }

}