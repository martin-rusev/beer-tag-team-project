package com.example.beertag.utils;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Country;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BreweryUtils {
    public static Brewery getBreweryById(Collection<Brewery> breweryMap, int breweryId) {
        return breweryMap
                .stream()
                .filter(b -> b.getId() == breweryId)
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Brewery", breweryId));
    }

    public static boolean checkIfBreweryExists(Collection<Brewery> breweries, Brewery brewery) {
        return breweries
                .stream()
                .map(Brewery::getId)
                .noneMatch(s -> s.equals(brewery.getId()));
    }

    public static Brewery createBrewery(Country country, String name ) {
        Brewery brewery = new Brewery();
        brewery.setCountry(country);
        brewery.setName(name);
        return brewery;
    }
}
