package com.example.beertag.utils;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Tag;
import com.example.beertag.models.User;

import java.util.Collection;

public class UserUtils {
    public static User getUserById(Collection<User> userMap, int userId) {
        return userMap
                .stream()
                .filter(b -> b.getId() == userId)
                .findFirst()
                .orElseThrow(() -> new NotFoundException("User", userId));
    }

    public static boolean checkIfUserExists(Collection<User> users, User user) {
        return users
                .stream()
                .map(User::getId)
                .noneMatch(s -> s.equals(user.getId()));
    }
}
