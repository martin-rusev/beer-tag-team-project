package com.example.beertag.utils;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class BeerUtils {
    public static Beer getBeerById(Collection<Beer> beerMap, int beerId) {
        return beerMap
                .stream()
                .filter(b -> b.getId() == beerId)
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Beer", beerId));
    }

    public static boolean checkIfBeerExists(Collection<Beer> beers, Beer beer) {
        return beers
                .stream()
                .map(Beer::getId)
                .noneMatch(s -> s.equals(beer.getId()));
    }

    public static Collection<Beer> filterBy(Collection<Beer> beersToReturn, Predicate<Beer> predicate) {
        return beersToReturn
                .stream()
                .filter(predicate)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
