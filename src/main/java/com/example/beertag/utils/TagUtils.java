package com.example.beertag.utils;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Style;
import com.example.beertag.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.Collection;

public class TagUtils {

    public static Tag getTagById(Collection<Tag> tagMap, int tagId) {
        return tagMap
                .stream()
                .filter(b -> b.getId() == tagId)
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Tag", tagId));
    }

}
