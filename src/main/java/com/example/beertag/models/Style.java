package com.example.beertag.models;

import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "styles")
public class Style implements Comparable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "style_id")
    private int id;

    @Column(name = "style")
    @NotNull
    private String name;

    public Style() {
    }


    public Style(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Style)) return false;

        Style style = (Style) o;

        return getName().equals(style.getName());
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }

    @Override
    public int compareTo(@NotNull Object o) {

        Style newStyle = (Style) o;

        if (newStyle.getName().equals(this.name)){
            return 1;
        }
        return -1;
    }

    @Override
    public String toString() {
        return String.valueOf(name);
    }
}
