package com.example.beertag.models.dto;

import com.example.beertag.models.Beer;
import com.example.beertag.models.*;
import com.example.beertag.services.BreweryService;
import com.example.beertag.services.CountryService;
import com.example.beertag.services.StyleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Mapper {
    private StyleService styleService;
    private CountryService countryService;
    private BreweryService breweryService;
    private ModelMapper modelMapper;

    @Autowired
    public Mapper(StyleService styleService,
                  CountryService countryService, ModelMapper modelMapper,
                  BreweryService breweryService) {
        this.styleService = styleService;

        this.countryService = countryService;
        this.modelMapper = modelMapper;
        this.breweryService = breweryService;
    }

    public Beer convertToBeer(BeerDto beerDto) {

        Beer beer = modelMapper.map(beerDto, Beer.class);
        if (beerDto.getBrewery() != null) {
            Brewery brewery = breweryService.getBreweryName(beerDto.getBrewery());
            beer.setBrewery(brewery);
            Country country=brewery.getCountry();
            beer.setCountry(country);
        }
        if (beerDto.getStyleName() != null) {
            Style style = styleService.getStyleByName(beerDto.getStyleName());
            beer.setStyle(style);
        }
        return beer;
    }

    public Beer mapModelToBeer(Beer beerModel) {
        Beer beer = modelMapper.map(beerModel, Beer.class);

        if (beerModel.getBrewery() != null) {
            Brewery brewery = breweryService.getBreweryById(beerModel.getBrewery().getId());
            beer.setBrewery(brewery);
            Country country=brewery.getCountry();
            beerModel.setCountry(country);
        }
        if (beerModel.getStyle() != null) {
            Style style = styleService.getStyleById(beerModel.getStyle().getId());
            beer.setStyle(style);
        }

        return beer;
    }


    public Beer toBeer(BeerDto dto) {
        Beer beer = new Beer();
        beer.setAbv(dto.getAbv());
        beer.setName(dto.getName());
        beer.setDescription(dto.getDescription());
        return beer;
    }

    public Beer mergeBeers(Beer oldBeer, Beer newBeer) {
        oldBeer.setName(getNotNullBeer(newBeer.getName(), oldBeer.getName()));
        oldBeer.setStyle(getNotNullBeer(newBeer.getStyle(), oldBeer.getStyle()));
        oldBeer.setAbv(getNotNullBeer(newBeer.getAbv(), oldBeer.getAbv()));
        oldBeer.setDescription(getNotNullBeer(newBeer.getDescription(),oldBeer.getDescription()));
        oldBeer.setBrewery(getNotNullBeer(newBeer.getBrewery(),oldBeer.getBrewery()));
        return oldBeer;
    }

    public Brewery mergeBreweries(Brewery oldBrewery, BreweryDto breweryDto) {
        Brewery brewery = new Brewery();
        oldBrewery.setName(getNotNull(breweryDto.getName(), oldBrewery.getName()));
        //get error if no country
        oldBrewery.setCountry(getNotNull(countryService.getCountryById(breweryDto.getCountryId()), oldBrewery.getCountry()));
        return oldBrewery;
    }

    public User mergeUsers(User oldUser, User newUser) {

        oldUser.setUsername(getNotNull(newUser.getUsername(), oldUser.getUsername()));
        oldUser.setFirstName(getNotNull(newUser.getFirstName(), oldUser.getFirstName()));
        oldUser.setLastName(getNotNull(newUser.getLastName(), oldUser.getLastName()));
        oldUser.setEmail(getNotNull(newUser.getEmail(), oldUser.getEmail()));
        oldUser.setPassword(getNotNull(newUser.getPassword(), newUser.getPassword()));
        return oldUser;
    }

    public <T> T getNotNull(T a, T b) {
        return b != null && a != null && !a.equals(b) ? a : b;}

    public <T> T getNotNullBeer(T n, T l) {

        if (n==null || n.hashCode()==0 ){
            return l;
        }

        return n != null && l != null && !n.equals(l) ? n : l;

    }

}
