package com.example.beertag.models.dto;

import javax.validation.constraints.Size;

public class TagDto {
    @Size(min = 2, max = 200, message = "Name size should be between 2 and 200 symbols")
    private String name;

    public TagDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

