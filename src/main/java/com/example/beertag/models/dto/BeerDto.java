package com.example.beertag.models.dto;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;



public class BeerDto {
    @Size(min = 2, max = 50, message = "Name size should be between 2 and 50 symbols")
    private String name;

    private String styleName;

    private String brewery;

    private String description;

    @Positive
    private double abv;

    public BeerDto() {
    }

    public BeerDto( String name, String description, double abv) {
        this.name = name;
        this.description = description;
        this.abv = abv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

