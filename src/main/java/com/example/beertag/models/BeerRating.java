package com.example.beertag.models;

import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.persistence.*;


@Entity
@Table(name = "ratings")
public class BeerRating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "rating_id")
    private int ratingId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "beer_id")
    private Beer beer;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated
    @JoinColumn(name = "rating_id")
    private Rating rating;

    public BeerRating() {
    }

    public int getRatingId() {
        return ratingId;
    }

    public void setRatingId(int ratingId) {
        this.ratingId = ratingId;
    }

    public Beer getBeer() {
        return beer;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public double getRatingValue() {
        return rating.getValue();
    }

    public void setRatingValue(int value) {
        this.rating.setValue(value);
    }
}
