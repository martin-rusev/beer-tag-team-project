package com.example.beertag.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "lists")
public class ListPersonal {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "list_id")
    int id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    private User user;

    @Column(name = "list_type")
    @Enumerated(EnumType.STRING)
    ListType type;


    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(
            name = "lists_users_beers",
            joinColumns = @JoinColumn(name = "list_id", table = "lists"),
            inverseJoinColumns = @JoinColumn(name = "beer_id", table = "beers")
    )
    private Set<Beer> beers = new HashSet<>();


    public ListPersonal() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ListType getType() {
        return type;
    }

    public void setType(ListType type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Beer> getBeers() {
        return beers;
    }

    public void addBeer(Beer beer) {
        this.beers.add(beer);
    }
    public void removeBeer(Beer beer) {
        this.beers.remove(beer);
    }
}

