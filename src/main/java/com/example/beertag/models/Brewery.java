package com.example.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "breweries")
public class Brewery {
    @Id
//    @Positive(message = "Id should be positive!")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "brewery_id")
    private int id;


    @Size(min = 2, max = 200, message = "Name size should be between 2 and 200 symbols!")
    @Column(name = "brewery")
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

//    @JsonIgnore
//    @OneToMany(mappedBy = "brewery")
//    private List<Beer> beers;

    public Brewery() {
    }

    public Brewery(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
      this.country=country;
    }

//    public List<Beer> getBeers() {
//        return beers;
//    }
//
//    public void setBeers(List<Beer> beers) {
//        this.beers = beers;
//    }
}
