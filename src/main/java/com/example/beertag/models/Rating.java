package com.example.beertag.models;

import javax.persistence.*;


public enum Rating {

    VERY_POOR(1), POOR(2),
    NOT_SATISFIED(3),
    NICE(4),
    SUPERB(5),
    EXCELLENT(6),
    EXCEEDS_EXPECTATIONS(7);

    private double value;

    Rating(double value) {
        this.value=value;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
