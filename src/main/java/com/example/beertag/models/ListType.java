package com.example.beertag.models;

public enum ListType {
    WISHLIST, DRUNKLIST;
}
