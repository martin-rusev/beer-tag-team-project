package com.example.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GeneratorType;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.EnableMBeanExport;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "beers")
public class Beer implements Comparable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    private int id;

    @NotNull
    @Size(min = 2, max = 50, message = "Name size should be between 2 and 50 symbols")
    @Column(name = "name")
    private String name;

    @Column
    private String description;

    @Positive(message = "ABV must be greater than 0")
    @Column(name = "abv")
    private double abv;

    @JsonIgnore
    @Lob
    private Byte[] picture;

    @ManyToOne
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "beers_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;


    @Column(name = "rating")
    private double tempRating =0.0;

    public Beer() {
    }

    public Beer(int id, String name, String description, double abv) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.abv = abv;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public Byte[] getPicture() {
        return picture;
    }

    public void setPicture(Byte[] picture) {
        this.picture = picture;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public void addTag(Tag tag){
        tags.add(tag);
    }

    public com.example.beertag.models.Style getStyle() {
        return style;
    }

    public void setStyle(com.example.beertag.models.Style style) {
        this.style = style;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }


    public double getTempRating() {
        return tempRating;
    }

    public void setTempRating(double tempRating) {
        this.tempRating = tempRating;
    }




    @Override
    public int compareTo(@NotNull Object o) {
        Beer beer = (Beer) o;
        if (beer.getName().equals(this.getName())){
            if (beer.brewery.getName().equals(this.brewery.getName())){
                return 0;
            }
        }
        return -1;
    }
}
