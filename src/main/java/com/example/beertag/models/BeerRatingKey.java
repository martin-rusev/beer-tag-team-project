package com.example.beertag.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;


public class BeerRatingKey implements Serializable {

    @Column
    private int beerId;
    @Column
    private int userId;

    public BeerRatingKey() {
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BeerRatingKey)) return false;

        BeerRatingKey that = (BeerRatingKey) o;

        if (getBeerId() != that.getBeerId()) return false;
        return getUserId() == that.getUserId();
    }

    @Override
    public int hashCode() {
        int result = getBeerId();
        result = 31 * result + getUserId();
        return result;
    }
}
