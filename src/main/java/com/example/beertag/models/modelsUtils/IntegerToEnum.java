package com.example.beertag.models.modelsUtils;

import com.example.beertag.models.ListType;
import com.example.beertag.models.Rating;
import org.springframework.core.convert.converter.Converter;

public class IntegerToEnum implements Converter<String, Rating> {

    @Override
    public Rating convert(String rating) {
        return Rating.valueOf(rating);
    }
}
