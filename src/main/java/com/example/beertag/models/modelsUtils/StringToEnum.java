package com.example.beertag.models.modelsUtils;

import com.example.beertag.models.ListType;
import org.springframework.core.convert.converter.Converter;


public class StringToEnum implements Converter<String,ListType> {

    @Override
    public ListType convert(String source) {
        return ListType.valueOf(source.toUpperCase());
    }
}
