package com.example.beertag.services;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.BeerRating;
import com.example.beertag.models.Rating;
import com.example.beertag.models.User;
import com.example.beertag.repostitory.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {
    private RatingRepository ratingRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    @Override
    public BeerRating addRating(Beer beer, Rating rating, User user) {
        try {
            BeerRating beerRating = createBeerRating(beer, rating, user);
            return ratingRepository.addRating(beerRating);
        } catch (RuntimeException e) {
            throw new NotFoundException("Rating for", beer.getName());
        }
    }

    @Override
    public BeerRating addRating(BeerRating beerRating) {

        return ratingRepository.addRating(beerRating);
    }

    @Override
    public double getRating(Beer beer) {
        if (!ratingRepository.checkIfBeerIsRated(beer)) {
            throw new NotFoundException("Rating for", beer.getName());
        }
        return ratingRepository.getRating(beer);
    }

    @Override
    public double getRatingMVC(Beer beer) {
        return ratingRepository.getRating(beer);
    }

    public BeerRating createBeerRating(Beer beer, Rating rating, User user) {
        BeerRating beerRating = new BeerRating();
        beerRating.setBeer(beer);
        beerRating.setUser(user);
        beerRating.setRating(rating);
        return beerRating;
    }

}
