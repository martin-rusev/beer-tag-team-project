package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.ListType;
import com.example.beertag.models.User;
import com.example.beertag.repostitory.ListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NonUniqueResultException;
import java.util.List;
import java.util.Set;

@Service
public class ListServiceImpl implements ListService {
    ListRepository listRepository;

    @Autowired
    public ListServiceImpl(ListRepository listRepository) {

        this.listRepository = listRepository;
    }


    @Override
    public ListPersonal updateList(ListPersonal listPersonal) {
        try {
            return listRepository.updateList(listPersonal);
        }catch (DuplicateKeyException e){
            throw new DuplicateException();
        }

    }

    @Override
    public ListPersonal getListById(int id) {
        try {
            return listRepository.getListById(id);
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("List");
        }
    }

    @Override
    public ListPersonal getListByTypeUser(User user, ListType listType) {
        return listRepository.getListByTypeUser(user,listType);
    }

    @Override
    public ListPersonal createList(ListPersonal personalList) {
        return listRepository.createList(personalList);
    }

    @Override
    public ListPersonal createList(ListType listType, User user) {
        ListPersonal listPersonal = new ListPersonal();
        listPersonal.setUser(user);
        listPersonal.setType(listType);
        return listRepository.createList(listPersonal);
    }

    @Override
    public ListPersonal deleteList(ListPersonal personalList) {
        Set<Beer> beerList = getListOfBeers(personalList);
        if (!isEmptyList(personalList)) {
            throw new InvalidOperationException
                    ("List %s can't be delete", String.valueOf(personalList.getType()));
        }
        return listRepository.deleteList(personalList);
    }

    @Override
    public boolean isEmptyList(ListPersonal personalList) {
        return getListOfBeers(personalList).size() == 0;
    }

    @Override
    public Set<Beer> getListOfBeers(ListPersonal personalList) {
        return personalList.getBeers();
    }
}
