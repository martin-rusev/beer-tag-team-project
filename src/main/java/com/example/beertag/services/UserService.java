package com.example.beertag.services;

import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.User;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Collection;

@Service
public interface UserService {

    Collection<User> getAllUsers();

    User getUserById(int id);

    User createUser(User user);

    User updateUser(User user);

    User deleteUser(int userId);

    User getUserByUsername(String username);

    Collection<User> getUsersByName(String username);

    ListPersonal createList(ListPersonal personalList, User user);

    boolean isOwnerOrAdmin(String createdBy, User principal);
}
