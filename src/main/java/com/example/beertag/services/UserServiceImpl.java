package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.User;
import com.example.beertag.repostitory.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.Collection;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Collection<User> getAllUsers() {
        try {
            return userRepository.getAllUsers();
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Users");
        }
    }

    @Override
    public User getUserById(int id) {
        try {
            return userRepository.getUserById(id);
        }catch (EntityNotFoundException e){
            throw new NotFoundException("User");
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try {
            return userRepository.getUserByUsername(username);
        }catch (EntityNotFoundException e){
            throw new NotFoundException("User");
        }
    }

    @Override
    public boolean isOwnerOrAdmin(String createdBy, User principal) {
        if (!createdBy.equals(principal.getUsername())
                && !principal.getRoles().stream().anyMatch(role -> role.getRole().equals("ROLE_ADMIN"))) {
            throw new InvalidOperationException(
                    "You can not modify %s beer ", "this");
        }
        return true;
    }

    @Override
    public Collection<User> getUsersByName(String username) {
        return userRepository.getUsersByName(username);
    }

    @Override
    public User createUser(User user) {
        if (userRepository.checkIfUserExist(user)) {
            throw new DuplicateException("User", "username", user.getUsername());
        }
        return userRepository.createUser(user);
    }

    @Override
    public User updateUser(User user) {
        return userRepository.updateUser(user);
    }

    @Override
    public ListPersonal createList(ListPersonal personalList, User user) {
        return userRepository.createList(personalList, user);
    }

    @Override
    public User deleteUser(int userId) {
        User userToDelete = getUserById(userId);
        return userRepository.deleteUser(userToDelete);
    }

}
