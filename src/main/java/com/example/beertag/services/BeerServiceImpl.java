package com.example.beertag.services;

import com.example.beertag.exeptions.DeleteForbiddenException;
import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.InvalidOperationException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.models.dto.Mapper;
import com.example.beertag.repostitory.BeerRepository;
import com.example.beertag.repostitory.BreweryRepository;
import com.example.beertag.repostitory.CountryRepository;
import com.example.beertag.repostitory.StyleRepository;
import com.example.beertag.utils.BeerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class BeerServiceImpl implements BeerService {
    private BeerRepository beerRepository;
    private StyleRepository styleRepository;
    private BreweryRepository breweryRepository;
    private CountryRepository countryRepository;
    private Mapper mapper;


    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository, StyleRepository styleRepository,
                           BreweryRepository breweryRepository, CountryRepository countryRepository,
                           Mapper mapper) {
        this.beerRepository = beerRepository;
        this.styleRepository = styleRepository;
        this.breweryRepository = breweryRepository;
        this.countryRepository = countryRepository;
        this.mapper = mapper;
    }


    @Override
    public Collection<Beer> getAllBeersOrdered(String name, String style) {
        Collection<Beer> beersToReturn = beerRepository.getAllBeers();

        if (beersToReturn.size() > 0 && name != null) {
            beersToReturn = BeerUtils.filterBy(beersToReturn,
                    beer -> beer.getName().toLowerCase().contains(name.toLowerCase()));
        }
        if (beersToReturn.size() > 0 && style != null) {
            beersToReturn = BeerUtils.filterBy(beersToReturn,
                    beer -> beer.getStyle().getName().toLowerCase().contains(style.toLowerCase()));
        }
        return beersToReturn;
    }

    @Override
    public List<Beer> getListBeers() {
        List<Beer> beers = beerRepository.getAllListBeers();
        return beers;
    }

    @Override
    public Beer getBeerById(int id) {
        try {
            return beerRepository.getBeerById(id);
        } catch (RuntimeException e) {
            throw new NotFoundException("Beer", "ID", String.valueOf(id));
        }
    }


    @Override
    public List<Beer> getBreweryBeers(int breweryId) {
        return beerRepository.getBreweryBeers(breweryId);
    }

    @Override
    public List<Beer> getBeersByStyle(String styleName, String orderParam) {
        Style style;
        try {
            style = styleRepository.getStyleByName(styleName);
        } catch (RuntimeException e) {
            throw new NotFoundException("Style", styleName);
        }
        if (styleHasBeers(style)) {
            return beerRepository.getBeersByStyle(styleName, orderParam);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public boolean styleHasBeers(Style style) {
        return beerRepository.getBeersByStyle(style.getName(), "name").size() >= 1;
    }

    @Override
    public List<Beer> getBeersByName(String beerName, String orderParam) {
        try {
            return beerRepository.getBeersByName(beerName, orderParam);
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Name", beerName);
        }
    }

    @Override
    public Collection<Beer> getBeers(String type, String verb, String tagName, List<Beer> beerList) {
        if (beerList.isEmpty()){
            throw new NotFoundException(type, verb, tagName);
        }else {
            return beerList;
        } }


    @Override
    public Beer getBeerByName(String name) {
        try {
            return beerRepository.getBeerByName(name);
        } catch (RuntimeException e) {
            throw new NotFoundException("Beer", "name", name);
        }
    }

    @Transactional
    @Override
    public Beer createBeer(Beer beer, User user) {

        if (beerRepository.checkIfBeerExist(beer)) {
            throw new DuplicateException(beer.getName());
        }
        beer.setCreatedBy(user);
        return beerRepository.createBeer(beer);
    }

    @Override
    public Beer updateBeer(Beer newBeer, User user) {

        if (!newBeer.getCreatedBy().equals(user.getUsername())
                && !user.getRoles().stream().anyMatch(role -> role.getRole().equals("ROLE_ADMIN"))) {
            throw new InvalidOperationException(
                    "You can not modify %s beer ", "this");
        }

        if (beerRepository.checkIfBeerExist(newBeer)) {
            throw new DuplicateException(newBeer.getName());
        }

        return beerRepository.updateBeer(newBeer);
    }

    @Override
    public Beer updateBeerTagingRating(Beer newBeer) {
        return beerRepository.updateBeer(newBeer);
    }




    @Override
    public Beer deleteBeer(Beer beer, User user) {

        if (beer.getCreatedBy() != null && beer.getCreatedBy().getId() != user.getId()
                && !user.getRoles().stream().anyMatch(role -> role.getRole().equals("Admin"))) {
            throw new InvalidOperationException(
                    "User can not modify %d beer ", beer.getId());
        }
        try {
            beerRepository.deleteBeer(beer);
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Beer");
        }catch (DataIntegrityViolationException e){
            throw new DeleteForbiddenException(beer.getName());
        }
        return beer;
    }

    @Override
    public boolean safeDelete(Beer beer) {
        try { beerRepository.deleteBeer(beer);
        }catch (DataIntegrityViolationException e){
            throw new DeleteForbiddenException(beer.getName());
        }

        return true;
    }

    @Override
    public Collection<Beer> getSortedBeersByAbv() {
        try {
            return beerRepository.getSortedBeersByAbv();
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Beers");
        }
    }

    @Override
    public List<Beer> getBeersByTag(String tagName) {

        return  beerRepository.getBeersByTag(tagName);

    }

    @Override
    public Collection<Beer> getSortedBeersByRating() {
        return beerRepository.getSortedBeersByRating();
    }
}


