package com.example.beertag.services;

import com.example.beertag.models.Role;
import org.springframework.stereotype.Service;

@Service
public interface RoleService {

    Role createRole(String role);
}
