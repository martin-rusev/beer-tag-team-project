package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.*;
import com.example.beertag.repostitory.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class StyleServiceImpl implements StyleService {
    private StyleRepository styleRepository;
    private BeerRepository beerRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository,
                            BeerRepository beerRepository) {
        this.styleRepository = styleRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public Style getStyleByName(String name) {
        try {
            return styleRepository.getStyleByName(name);
        } catch (RuntimeException e) {
            throw new NotFoundException("Style", "name", name);
        }
    }

    @Override
    public List<Style> allStyles() {
        try {
            return styleRepository.getAllStyles();
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Styles");
        }
    }

    @Override
    public Map<Style, List<Beer>> getBeersPopularStyles() {
        List<Style> styleList = allStyles();
        Map<Style, List<Beer>> styleBeers = new HashMap<>();
        Map<Style, Integer> stylePopular = new TreeMap<>();
        for (Style style : styleList) {
            List<Beer> tempList = new ArrayList<>();
            if (checkIfStyleHasBeers(style.getName())) {
                tempList = beerRepository.getBeersByStyle(style.getName(), "name");
            }
            styleBeers.put(style, tempList);
        }
        return styleBeers;
    }

    @Override
    public Map<Style, Integer> getStylesBeersCount() {
        List<Style> styleList = allStyles();
        Map<Style, Integer> stylePopular = new TreeMap<>();
        for (Style style : styleList) {
            List<Beer> tempList = new ArrayList<>();
            if (checkIfStyleHasBeers(style.getName())) {
                tempList =
                        beerRepository.getBeersByStyle(style.getName(), "name");

                stylePopular.put(style, tempList.size());
            }
        }
        stylePopular = stylePopular.entrySet().stream()
                .sorted(Map.Entry.<Style, Integer>comparingByValue()).limit(12)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        return stylePopular;
    }

    @Override
    public boolean checkIfStyleHasBeers(String styleName) {
        return styleRepository.checkIfStyleHasBeers(styleName);
    }

    @Override
    public Style create(Style style) {
        if (styleRepository.checkIfStyleExist(style.getName())) {
            throw new DuplicateException("Style", "name", style.getName());
        }
        return styleRepository.create(style);
    }


    @Override
    public Style update(Style style) {
        return styleRepository.update(style);
    }

    @Override
    public Style delete(Style style) {
        return styleRepository.delete(style);
    }

    @Override
    public Style getStyleById(int id) {
        return styleRepository.getStyleById(id);
    }
}

