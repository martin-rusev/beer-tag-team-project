package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Tag;
import com.example.beertag.repostitory.TagRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Collection;

@Service
public class TagServiceImpl implements TagService {
    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Collection<Tag> getAllTags() {
        try {
            return tagRepository.getAllTags();
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Tags");
        }
    }

    @Override
    public Tag getTagById(int id) {
        return tagRepository.getTagById(id);
    }

    @Override
    public Tag createTag(Tag tag) {
        if (tagRepository.checkIfTagExist(tag)) {
            throw new DuplicateException("Tag", "contain", tag.getName());
        }
        return tagRepository.createTag(tag);
    }

    @Override
    public void updateTag(int id, Tag tag) {
//        if (oldTag.getName().equals(tagToUpdate.getName())){
//            throw new DuplicateException("Tag with this contain");
//        }

        tagRepository.updateTag(id,tag);
    }

    @Override
    public Tag deleteTag(int tagId) {
        Tag tagToDelete = getTagById(tagId);
        return tagRepository.deleteTag(tagToDelete);
    }

    @Override
    public Collection<Tag> getTagByName(String name) {
        try {
            if (!tagRepository.checkIfTagExist(tagRepository.getTagByName(name))) {
                throw new NotFoundException("Tag", "contain", name);
            }
        } catch (RuntimeException e) {
            throw new NotFoundException("Tag", "contain", name);
        }
        return tagRepository.getTagsByName(name);
    }
}
