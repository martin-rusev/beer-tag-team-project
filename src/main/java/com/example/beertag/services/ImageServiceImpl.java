package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.repostitory.BeerRepository;
import com.example.beertag.repostitory.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService {
    BeerRepository beerRepository;
    UserRepository userRepository;

    public ImageServiceImpl(BeerRepository beerRepository, UserRepository userRepository) {
        this.beerRepository = beerRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public void saveImgFile(Integer beerId, MultipartFile file) {
        try {
            Beer beer = beerRepository.getBeerById(beerId);

            Byte[] byteArray = new Byte[file.getBytes().length];
            int i = 0;
            for (byte b : file.getBytes()) {
                byteArray[i++] = b;
            }
            beer.setPicture(byteArray);
            beerRepository.updateBeer(beer);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveUserImgFile(Integer userId, MultipartFile file)  {
        try {
            User user = userRepository.getUserById(userId);

            Byte[] byteArray = new Byte[file.getBytes().length];
            int i = 0;
            for (byte b : file.getBytes()) {
                byteArray[i++] = b;
            }
            user.setPicture(byteArray);
            userRepository.updateUser(user);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
