package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.BeerRating;
import com.example.beertag.models.Rating;
import com.example.beertag.models.User;

public interface RatingService {

    BeerRating addRating(Beer beer, Rating rating, User user);

    BeerRating addRating(BeerRating beerRating);

    double getRating(Beer beer);

    double getRatingMVC(Beer beer);

}
