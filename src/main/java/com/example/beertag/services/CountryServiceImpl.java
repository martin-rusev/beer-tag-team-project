package com.example.beertag.services;

import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;
import com.example.beertag.models.Style;
import com.example.beertag.repostitory.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {
    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public Country getCountryById(int id) {
        return countryRepository.getCountryById(id);
    }

    @Override
    public Country getCountryByName(String name) {
        return countryRepository.getCountryByName(name);
    }

    @Override
    public List<Country> getAllCountries() {
        try {
            return countryRepository.getAllCountries();
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Countries");
        }
    }

    @Override
    public List<Beer> getBeersByCountry(String countryName, String orderParam) {

        try {
            Country country =  countryRepository.getCountryByName(countryName);
        } catch (RuntimeException e) {
            throw new NotFoundException("Country", countryName);
        }
        if (checkIfSCountryHasBeers(countryName)) {
            return countryRepository.getAllBeersByCounty(countryName,orderParam);
        } else {


            return new ArrayList<>();
        }
    }

    @Override
    public boolean checkIfSCountryHasBeers(String countryName) {
        return countryRepository.checkIfSCountryHasBeers(countryName);
    }

}
