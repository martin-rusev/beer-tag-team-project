package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Style;
import com.example.beertag.models.User;

import java.util.Collection;
import java.util.List;

public interface BeerService {
    Collection<Beer> getAllBeersOrdered(String name, String style);

    List<Beer> getListBeers();

    Beer getBeerById(int id);

    List<Beer> getBreweryBeers(int breweryId);

    List<Beer> getBeersByStyle(String beerName, String orderParam);

    List<Beer> getBeersByName(String styleName, String orderParam);

    Beer getBeerByName(String name);

    Beer createBeer(Beer beer, User user);

    Beer updateBeer(Beer beer, User user);

    Beer updateBeerTagingRating(Beer beer);

     Collection<Beer> getBeers(String type,String verb,String tagName, List<Beer> beerList);

    Beer deleteBeer(Beer beer, User newUser);

    boolean safeDelete(Beer beer);

    Collection<Beer> getSortedBeersByAbv();

    List<Beer> getBeersByTag(String tagName);

    boolean styleHasBeers(Style style);

    Collection<Beer> getSortedBeersByRating();
}
