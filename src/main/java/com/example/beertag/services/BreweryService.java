package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;

import java.util.Collection;
import java.util.List;

public interface BreweryService {

    Collection<Brewery> getAllBreweries();

    Brewery getBreweryById(int id);

    void createBrewery(Brewery brewery);

    Collection<Brewery> getCollectionBreweryByName(String name);

    Brewery getBreweryName(String name);

    List<Beer> getBreweryBeers(int breweryId);

    List<Brewery>getBreweriesByCountry(String countyName);

    void updateBrewery(Brewery brewery);

    Brewery deleteBrewery(int breweryId);
}
