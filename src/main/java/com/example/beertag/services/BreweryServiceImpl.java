package com.example.beertag.services;

import com.example.beertag.exeptions.DuplicateException;
import com.example.beertag.exeptions.NotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.Brewery;
import com.example.beertag.models.Country;
import com.example.beertag.repostitory.BeerRepository;
import com.example.beertag.repostitory.BreweryRepository;
import com.example.beertag.repostitory.CountryRepository;
import com.example.beertag.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {
    private BreweryRepository breweryRepository;
    private BeerRepository beerRepository;
    private CountryRepository countryRepository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository, BeerRepository beerRepository, CountryRepository countryRepository) {
        this.breweryRepository = breweryRepository;
        this.beerRepository = beerRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public Collection<Brewery> getAllBreweries() {
        return breweryRepository.getAllBreweries();
    }

    @Override
    public Brewery getBreweryById(int id) {
        return breweryRepository.getBreweryById(id);
    }

    @Override
    public void createBrewery(Brewery brewery) {
        if (breweryRepository.checkIfBreweryExist(brewery)) {
            throw new DuplicateException("Brewery", "name", brewery.getName());
        }
        breweryRepository.createBrewery(brewery);
    }

    @Override
    public Collection<Brewery> getCollectionBreweryByName(String name) {
        try {
            if (breweryRepository.checkIfBreweryExist(getBreweryName(name))) {
                throw new NotFoundException("Brewery", "name", name);
            }
        } catch (RuntimeException e) {
            throw new NotFoundException("Brewery", "name", name);
        }
        return breweryRepository.getCollectionBreweryByName(name);
    }

    @Override
    public Brewery getBreweryName(String name) {
            return breweryRepository.getBreweryName(name);

    }

    @Override
    public List<Beer> getBreweryBeers(int breweryId) {
        return beerRepository.getBreweryBeers(breweryId);
    }

    @Override
    public List<Brewery> getBreweriesByCountry(String countryName) {
        try {
            Country country = countryRepository.getCountryByName(countryName);
            return breweryRepository.getBreweriesByCountry(country);
        } catch (RuntimeException e) {
            throw new NotFoundException(String.format("Breweries from country %s", countryName));
        }
    }

    @Override
    public void updateBrewery(Brewery brewery) {
        breweryRepository.updateBrewery(brewery);
    }

    @Override
    public Brewery deleteBrewery(int breweryId) {
        try {
            Brewery breweryToDelete = getBreweryById(breweryId);
            return breweryRepository.deleteBrewery(breweryToDelete);
        } catch (RuntimeException e) {
            throw new NotFoundException("Brewery", "ID", String.valueOf(breweryId));
        }
    }


}
