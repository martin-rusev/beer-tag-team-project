package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.ListPersonal;
import com.example.beertag.models.ListType;
import com.example.beertag.models.User;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public interface ListService {

    ListPersonal updateList(ListPersonal listPersonal);
    ListPersonal getListById(int id);
    ListPersonal getListByTypeUser(User user, ListType listType);
    ListPersonal createList(ListPersonal personalList);
    ListPersonal createList(ListType listType,User user);
    ListPersonal deleteList(ListPersonal personalList);
    Set<Beer> getListOfBeers(ListPersonal personalList);
    boolean isEmptyList(ListPersonal personalList);
}
