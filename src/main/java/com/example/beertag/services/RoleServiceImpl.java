package com.example.beertag.services;

import com.example.beertag.models.Role;
import com.example.beertag.repostitory.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    private RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role createRole(String role) {
        Role role1 = new Role(role);
        return roleRepository.createRole(role1);
    }
}
