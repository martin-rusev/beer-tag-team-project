package com.example.beertag.services;

import com.example.beertag.models.Brewery;
import com.example.beertag.models.Tag;

import java.util.Collection;

public interface TagService {

    Collection<Tag> getAllTags();

    Tag getTagById(int id);

    Tag createTag(Tag tag);

    void updateTag(int id, Tag tag);

    Tag deleteTag(int tagId);

    Collection<Tag> getTagByName(String name);
}
