package com.example.beertag.services;

import com.example.beertag.models.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface StyleService {

    Style getStyleByName(String name);

    List<Style> allStyles();

    Map<Style,List<Beer>> getBeersPopularStyles();

     Map<Style, Integer> getStylesBeersCount();

    boolean checkIfStyleHasBeers(String styleName);

    Style create(Style style);

    Style update(Style style);

    Style delete(Style style);

    Style getStyleById(int id);

}
