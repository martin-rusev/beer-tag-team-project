package com.example.beertag.services;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Country;

import java.util.List;

public interface CountryService {

    Country getCountryById(int id);

    Country getCountryByName(String name);

    List<Country> getAllCountries();

    List<Beer> getBeersByCountry(String countryName, String orderParam);

    boolean checkIfSCountryHasBeers(String countryName);

}
