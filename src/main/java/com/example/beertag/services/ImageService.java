package com.example.beertag.services;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public interface ImageService {
   void saveImgFile(Integer beerId, MultipartFile file);
   void saveUserImgFile(Integer userId, MultipartFile file) ;

}
