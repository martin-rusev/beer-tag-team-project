INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (317, 'Hocus Pocus', 'This traditional Bavarian-style kellerbier originated from the small artisanal breweries of Franconia, where it is still a favorite in the local beer gardens.  Ours is served unfiltered with a crisp, smooth finish that taste like sunshine. ', 1535, 6.2, null, 180, 25, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (318, 'Grimbergen Blonde', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%', 1536, 6.2, null, 181, 26, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (319, 'Widdershins Barleywine', 'Many Mexican beer styles today are descendants of old Austrian styles, from when Austria ruled Mexico in the late 19th century. Our Dos Perros is made with German Munich malt, English Pale malt, and Chocolate malt, and hopped with Perle and Saaz hops. To lighten the body, as many Mexican brewers do, we add a small portion of flaked maize. The result is a wonderfully bready malt aroma, balanced with some maize sweetness and a noble hop finish.', 1537, 6.2, null, 182, 7, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (320, 'Lucifer', 'A new version of an American classic. Our Yazoo Pale Ale bursts with spicy, citrusy hop aroma and flavor, coming from the newly discovered Amarillo hop. The wonderful hop aroma is balanced nicely with a toasty malt body, ending with a cleansing hop finish. Made with English Pale, Munich, Vienna, and Crystal malts, and generously hopped with Amarillo, Perle, and Cascade hops. Fermented with our English ale yeast.', 1538, 6.2, null, 183, 8, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (321, 'Bitter', 'An authentic example of a Bavarian Hefeweizen. ', 1539, 6.2, null, 184, 9, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (322, 'Winter Warmer', 'A rich, chocolaty English Porter with a clean finish. We use the finest floor-malted Maris Otter malts from England, the same malts used for the best single-malt scotch. A portion of malted rye gives a spicy, slightly dry finish.', 1540, 6.2, null, 185, 10, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (323, 'Winter Welcome 2007-2008', 'The tan lace clings to the glass as you raise the pint to your lips. Close your eyes and smile as the rich espresso notes fade to a dry, roasted finish. Exceptionally smooth and satisfying. Made with English Pale malt, roasted barley, black patent malt, and flaked barley. Hopped with East Kent Goldings and Target hops, and fermented with our English ale yeast.', 1541, 6.2, null, 186, 11, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (324, 'Oatmeal Stout', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.', 1542, 6.2, null, 187, 12, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (325, 'Espresso Porter', 'A regular winner of awards for quality and flavour, and winner of the silver medal at the International Brewing Industry Awards 2002, this refreshing yet full-bodied bitter is a favourite with beer drinkers everywhere. The rich flavours of premium malt and goldings hops are unmistakable in this well balanced, traditionally brewed bitter.', 1543, 7.3, null, 188, 13, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (326, 'Chocolate Stout', 'FA is no small beer: despite its deceptively pale golden colour, it boasts a big, smooth flavour and strong punch. Brewed with the finest English malts, and conditioned in cask with dry hops to produce fresh hop aromas and a fuller flavour, delighting the mouth and stimulating the tongue.', 1544, 7.3, null, 189, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (333, 'Wiesen Edel Weisse', 'This beer is not very bitter which allows the flavors of the rye and honey to come through. The result is a full bodied, sweet, and malty beer.													
', 1497, 7.3, null, 165, 23, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (356, 'Killer Penguin', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.											
', 1520, 6.2, null, 188, 26, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (357, 'Oatmeal Stout', 'An authentic example of a Bavarian Hefeweizen. 				
', 1521, 6.2, null, 189, 7, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (358, 'Isolation Ale', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%										
', 1522, 7.3, null, 190, 8, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (359, 'Festive Ale 2007', 'Dark ale with a tan head. Subtle sake flavor from cask aging.						
', 1533, 4.7, 0x, 197, 9, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (360, 'Bourbon County Stout 2007', 'A very yeasty beer with an earthy aroma. Rich flavors deepen the stout character.							
', 1524, 7.3, null, 192, 10, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (361, 'Twelve Days', 'A silky, crisp, and rich amber-colored ale with a fluffy head and strong banana note on the nose.									
', 1525, 7.3, null, 193, 11, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (362, 'Seriously Bad Elf', 'Chaotic yet hypnotic, Mayhem is a deliberate Double IPA, brewed and conditioned with a special Belgian yeast that contributes complexity and spice. Abundant American hops offer grapefruit, pine, must and mint, which play off the fullness and sweetness of pale malts then provide a biting yet enticing finish.

Mayhem Double IPA accents the complex spice combinations found in Thai, Indian, Cajun, Morrocan, Mexican and Southwest American cuisines as well as BBQ marinades, dry rubs and sauces.

Mayhem creates MAYHEM.', 1534, 4, null, 186, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (363, 'Big Shot Seasonal Ale', 'Don''t let the golden color fool you - this isn''t your father''s lite beer!

Brewed with imported German malts and US-grown hops, this beer is a full-flavored introduction to craft-brewed beer. We add the hops late in the boil, allowing you to enjoy the flavor and aroma of the hops without an aggressive bitterness.', 1527, 7.3, null, 195, 13, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (364, 'God Jul - Winter Ale', 'A Lager that actually tastes of something?

You have to be kidding, right?

77 lager is made with 100% malt and whole leaf hops.

It contains no preservatives, additives, cheap substitutes or any other junk.

Maybe we are crazy. So what?

Taste 77 Lager and we are pretty sure you will agree that the fine line between genius and insanity has just become a little more blurred.', 1528, 7.3, null, 196, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (365, 'Harvest Ale 2007', 'A bourbon barrel aged Imperial Stout infused with coffee.					
', 1547, 5.8, null, 177, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (366, 'Nut Cracker Ale', 'It is light with a bit of a citrus flavor.  A pretty standard summer seasonal.							
', 1530, 7.3, null, 198, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (367, 'Goosinator Smoked Doppelbock 2007', 'A BIG PALE ALE with an awsome balance of Belgian malts with Fuggles and East Kent Golding hops.									
', 1531, 7.3, null, 199, 17, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (368, 'Snow Bound Winter Ale', 'Anything but a typical American Pale, Santa Fe Pale Ale is as full bodied as its most robust English counterparts, while asserting its American origin with a healthy nose resplendent with Cascade and Willamette hops. It finishes with a well-balanced combination of the subtle, almost Pilsner-like maltiness accentuated by the German yeast used to brew this Santa Fe classic, and a hop bite sufficient to leave a lingering smile on the face of any fan of American Pale Ales.																																																																											
', 1532, 7.3, null, 200, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (369, 'Warrior IPA', 'A well balanced American Amber Ale. Smooth malt character balanced with a healthy dose of Cascade hops aged on oak chips - our most popular beer.																										
A light bodied golden ale, low in hop character with a spicy aroma from whole Czech Saaz hops.																										
One of these beers is available all of the time. Our heartiest beers emphasize roasted malts and the generous use of hops. Creamy, smooth and always nitrogenated, these beers are nearly a meal or a dessert in themselves.																										
', 1533, 7.3, null, 201, 19, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (370, 'Petrus Oud Bruin', 'The first offering from the Coastal Extreme Brewing Company blends some of the world''s finest ingredients into a delightful beer. In producing our amber ale, we selected the highest quality European and American malts and hops. This ale has a malt character which is delicately balanced with its hop profile so that consumers of all levels enjoy drinking it. Hurricane Amber Ale is a full flavored beer which clearly has more taste than other domestic and imported light beers while at the same time does not overpower the drinker with heavy body or excessive bitterness. So find yourself a cold ''Hurricane'' and ENJOY!																																																																			
', 1534, 7.3, null, 202, 20, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (371, 'Petrus Gouden Tripel Ale', 'Our Summer seasonal, Trade Winds Tripel is a Belgian-style Golden Ale with a Southeast Asian twist. Instead of using candi sugar (typical for such a beer), we use rice in the mash to lighten the body and increase the gravity, and spice with Thai Basil. The result is an aromatic, digestible and complex beer made for a lazy summer evening.																																			
', 1535, 7.3, null, 203, 21, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (372, 'Petrus Blond Ale', 'Brewing the perfect ale is truly a balancing act...hazardous work you might say. With Hop Hazard our challenge was to hand craft a malt rich base that could counterbalance a combustible five-hop blend and still leave your taste buds with enough room to enjoy a unique, crisp hop finish.																											
', 1536, 5.7, null, 204, 22, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (373, 'Petrus Dubbel Bruin Ale', 'This unfiltered ale retains a medium maltiness and body and features a flowery hop perfume and pleasant bitterness.											
', 1537, 5.5, null, 205, 23, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (374, 'Petrus Speciale', 'ESB was launched into the Fuller''s family in 1971, as a winter brew to replace a beer named Old Burton Extra. The potential of the beer was soon realised and ESB was installed as a permanent fixture, creating an immediate impact. 

Not only was it one of the strongest regularly brewed draught beers in the country (at 5.5% ABV), it was also one of the tastiest, and as the awareness of the beer grew, so did its popularity. ESB''s reputation was soon enhanced after being named CAMRA''s (Campaign for Real Ale) Beer of the Year in 1978, and the beer has not stopped winning since! 

With three CAMRA Beer of the Year awards, two World Champion Beer awards, and numerous other gold medals to speak of, ESB is, quite simply, the Champion Ale.', 1538, 5.5, null, 206, 24, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (375, 'Weizengold Hefefein', '"Sweet with some
tropical flavors, some banana, a hint of clove."
', 1539, 5.5, null, 207, 25, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (376, 'Never Summer Ale', 'A Jahva-Choklat hybrid.		
', 1540, 5.5, null, 208, 26, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (377, 'Modern Monks Belgian Blonde', ' It was designed to be enjoyed during the holiday season. 

When you bring a glass of this dark copper ale to your lips to take your first sip you will notice the aroma of cinnamon.  There is no aromatic hop added that might overpower the distinct spice scent.  The medium body of this beer is formed from caramel and pale malts.  These create enough body to support the spices without making the beer excessively rich.  Bittering hops are added to counter the sweetness of the malt and spice.  The finish of the beer is a blend of cinnamon and nutmeg.  The combination of these two spices results in a balanced, pumpkin-pie flavor. 

The overall character is a smooth, medium bodied ale spiced with cinnamon and nutmeg"
', 1541, 5.5, null, 209, 7, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (378, 'Glacier Ale', 'A GOLD MEDAL WINNER at the Great American Beer Festival, beating out 98 other IPA''s and chosen as the best IPA in the country! It''s amber color and incredible hoppy aroma will keep you coming back for more. R-U-HOPPY?																									
', 1542, 5.5, null, 210, 8, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (379, 'Bent Nail IPA', 'Our lightest beer, made from 100% malted barley. This blonde lager is what sailors really swam to shore for; it''s light, with very little bitterness and a slight malt finish.
', 1543, 6.2, null, 211, 9, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (380, 'Espresso Porter', 'Our Coconut Porter is a classic robust porter spiced with all natural toasted coconut.  It is black in color and crowned with a creamy, dark tan head.   It begins with a malty-toasted coconut aroma followed by a rich, silky feel with tastes of dark malt, chocolate, and hints of coffee.  It then finishes with flavors of  toasted coconut and hoppy spice to balance the finish.
', 1544, 6.2, null, 212, 10, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (381, 'Harvest Ale', 'Crisp and smooth. This beer is deep amber in color. Toasty, malty flavor. Full-bodied with a clean finish.
', 1545, 6.2, null, 213, 11, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (382, 'Stillwater Rye', 'This American style amber ale will be our opening release and house beer for SCBC. AmericAle is smooth and diverse all in one shot. With a medium malt body and fresh hop aroma we hope you enjoy drinking it as much as we do making it.
', 1546, 6.2, null, 214, 12, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (383, 'Sharptail Pale Ale', 'India Pale Ale was developed in Burton, England, as a \\Super-Premium\\" hoppy pale ale around 1800. The extra strength in alcohol and hops helped preserve the beer on its long export journeys to India and beyond. The style developed a following worldwide. Its flavor begins with a smooth
', 1547, 6.2, null, 215, 13, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (384, 'Fat Belly Amber', '"Our Czech Premium Lager is beer for light beer lovers. The most gentle heads of the high quality hop, virgin clear natural water and granules of selected species of Moravian barley make it the beverage of real experts. 

', 1548, 6.2, null, 216, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (385, 'Whitetail Wheat', 'Crisp and smooth. This beer is deep amber in color. Toasty, malty flavor. Full-bodied with a clean finish.										
', 1549, 6.2, null, 217, 15, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (386, 'Sandbagger Gold', 'This traditional Bavarian-style kellerbier originated from the small artisanal breweries of Franconia, where it is still a favorite in the local beer gardens.  Ours is served unfiltered with a crisp, smooth finish that taste like sunshine. ', 1550, 6.2, null, 218, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (387, 'Brunette Nut Brown Ale', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%', 1551, 7.3, null, 219, 17, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (388, 'Irish Red', 'Many Mexican beer styles today are descendants of old Austrian styles, from when Austria ruled Mexico in the late 19th century. Our Dos Perros is made with German Munich malt, English Pale malt, and Chocolate malt, and hopped with Perle and Saaz hops. To lighten the body, as many Mexican brewers do, we add a small portion of flaked maize. The result is a wonderfully bready malt aroma, balanced with some maize sweetness and a noble hop finish.', 1552, 7.3, null, 220, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (389, 'Belgian Wit', 'A new version of an American classic. Our Yazoo Pale Ale bursts with spicy, citrusy hop aroma and flavor, coming from the newly discovered Amarillo hop. The wonderful hop aroma is balanced nicely with a toasty malt body, ending with a cleansing hop finish. Made with English Pale, Munich, Vienna, and Crystal malts, and generously hopped with Amarillo, Perle, and Cascade hops. Fermented with our English ale yeast.', 1553, 7.3, null, 221, 19, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (390, 'EOS Hefeweizen', 'An authentic example of a Bavarian Hefeweizen. ', 1554, 7.3, null, 222, 20, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (391, 'India Pale Ale', 'A rich, chocolaty English Porter with a clean finish. We use the finest floor-malted Maris Otter malts from England, the same malts used for the best single-malt scotch. A portion of malted rye gives a spicy, slightly dry finish.', 1555, 7.3, null, 223, 21, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (392, 'Winter Ale', 'The tan lace clings to the glass as you raise the pint to your lips. Close your eyes and smile as the rich espresso notes fade to a dry, roasted finish. Exceptionally smooth and satisfying. Made with English Pale malt, roasted barley, black patent malt, and flaked barley. Hopped with East Kent Goldings and Target hops, and fermented with our English ale yeast.', 1556, 7.3, null, 224, 22, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (393, 'Pride & Joy Mild Ale', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.', 1557, 7.3, null, 225, 23, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (394, 'Robert the Bruce Scottish Ale', 'A regular winner of awards for quality and flavour, and winner of the silver medal at the International Brewing Industry Awards 2002, this refreshing yet full-bodied bitter is a favourite with beer drinkers everywhere. The rich flavours of premium malt and goldings hops are unmistakable in this well balanced, traditionally brewed bitter.', 1558, 7.3, null, 226, 24, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (395, 'Lump of Coal Dark Holiday Stout', 'FA is no small beer: despite its deceptively pale golden colour, it boasts a big, smooth flavour and strong punch. Brewed with the finest English malts, and conditioned in cask with dry hops to produce fresh hop aromas and a fuller flavour, delighting the mouth and stimulating the tongue.', 1559, 7.3, null, 227, 25, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (396, 'Warm Welcome Nut Browned Ale', 'This beer is not very bitter which allows the flavors of the rye and honey to come through. The result is a full bodied, sweet, and malty beer.													
', 1560, 7.3, null, 228, 26, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (397, 'Pendle Witches Brew', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.											
', 1561, 7.3, null, 229, 7, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (398, 'Carnegie Stark-Porter', 'An authentic example of a Bavarian Hefeweizen. 				
', 1562, 7.3, null, 230, 8, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (399, 'Harvest Ale 2002', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%										
', 1563, 7.3, null, 231, 9, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (400, 'Woody Organic IPA', 'Dark ale with a tan head. Subtle sake flavor from cask aging.						
', 1564, 7.3, null, 232, 10, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (401, 'Adriaan', 'A very yeasty beer with an earthy aroma. Rich flavors deepen the stout character.							
', 1565, 5.7, null, 233, 11, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (402, 'Leute Bok Bier', 'A silky, crisp, and rich amber-colored ale with a fluffy head and strong banana note on the nose.									
', 1566, 5.7, null, 234, 12, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (403, 'La Folie Falling Rock', 'Chaotic yet hypnotic, Mayhem is a deliberate Double IPA, brewed and conditioned with a special Belgian yeast that contributes complexity and spice. Abundant American hops offer grapefruit, pine, must and mint, which play off the fullness and sweetness of pale malts then provide a biting yet enticing finish.

Mayhem Double IPA accents the complex spice combinations found in Thai, Indian, Cajun, Morrocan, Mexican and Southwest American cuisines as well as BBQ marinades, dry rubs and sauces.

Mayhem creates MAYHEM.', 1567, 5.7, null, 235, 13, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (404, 'Oak Aged IPA', 'Don''t let the golden color fool you - this isn''t your father''s lite beer!

Brewed with imported German malts and US-grown hops, this beer is a full-flavored introduction to craft-brewed beer. We add the hops late in the boil, allowing you to enjoy the flavor and aroma of the hops without an aggressive bitterness.', 1568, 5.7, null, 236, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (405, 'Tikka Gold', 'A Lager that actually tastes of something?

You have to be kidding, right?

77 lager is made with 100% malt and whole leaf hops.

It contains no preservatives, additives, cheap substitutes or any other junk.

Maybe we are crazy. So what?

Taste 77 Lager and we are pretty sure you will agree that the fine line between genius and insanity has just become a little more blurred.', 1569, 5.7, null, 237, 15, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (406, 'La Divine Double Blond', 'A silky, crisp, and rich amber-colored ale with a fluffy head and strong banana note on the nose.									
', 1570, 5.7, null, 238, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (407, 'La Divine Tripel Amber', 'Chaotic yet hypnotic, Mayhem is a deliberate Double IPA, brewed and conditioned with a special Belgian yeast that contributes complexity and spice. Abundant American hops offer grapefruit, pine, must and mint, which play off the fullness and sweetness of pale malts then provide a biting yet enticing finish.

Mayhem Double IPA accents the complex spice combinations found in Thai, Indian, Cajun, Morrocan, Mexican and Southwest American cuisines as well as BBQ marinades, dry rubs and sauces.

Mayhem creates MAYHEM.', 1571, 5.7, null, 239, 17, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (408, 'Ename Tripel', 'Don''t let the golden color fool you - this isn''t your father''s lite beer!

Brewed with imported German malts and US-grown hops, this beer is a full-flavored introduction to craft-brewed beer. We add the hops late in the boil, allowing you to enjoy the flavor and aroma of the hops without an aggressive bitterness.', 1572, 5.7, null, 240, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (409, 'Triple Dipsea Belgian', 'A Lager that actually tastes of something?

You have to be kidding, right?

77 lager is made with 100% malt and whole leaf hops.

It contains no preservatives, additives, cheap substitutes or any other junk.

Maybe we are crazy. So what?

Taste 77 Lager and we are pretty sure you will agree that the fine line between genius and insanity has just become a little more blurred.', 1573, 5.7, null, 241, 19, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (410, 'Oktoberfest Weizen', 'A bourbon barrel aged Imperial Stout infused with coffee.					
', 1574, 6.2, null, 242, 20, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (411, 'Imperial Hefeweizen', 'It is light with a bit of a citrus flavor.  A pretty standard summer seasonal.							
', 1575, 6.2, null, 243, 21, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (412, 'Triple Exultation Old Ale', 'A BIG PALE ALE with an awsome balance of Belgian malts with Fuggles and East Kent Golding hops.									
', 1576, 6.2, null, 244, 22, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (413, 'Organic Gingerbread Ale', 'Anything but a typical American Pale, Santa Fe Pale Ale is as full bodied as its most robust English counterparts, while asserting its American origin with a healthy nose resplendent with Cascade and Willamette hops. It finishes with a well-balanced combination of the subtle, almost Pilsner-like maltiness accentuated by the German yeast used to brew this Santa Fe classic, and a hop bite sufficient to leave a lingering smile on the face of any fan of American Pale Ales.																																																																											
', 1577, 6.2, null, 245, 23, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (414, 'Falcon Pale Ale', 'A well balanced American Amber Ale. Smooth malt character balanced with a healthy dose of Cascade hops aged on oak chips - our most popular beer.																										
A light bodied golden ale, low in hop character with a spicy aroma from whole Czech Saaz hops.																										
One of these beers is available all of the time. Our heartiest beers emphasize roasted malts and the generous use of hops. Creamy, smooth and always nitrogenated, these beers are nearly a meal or a dessert in themselves.																										
', 1578, 6.2, null, 246, 24, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (415, 'Buzzsaw Brown', 'The first offering from the Coastal Extreme Brewing Company blends some of the world''s finest ingredients into a delightful beer. In producing our amber ale, we selected the highest quality European and American malts and hops. This ale has a malt character which is delicately balanced with its hop profile so that consumers of all levels enjoy drinking it. Hurricane Amber Ale is a full flavored beer which clearly has more taste than other domestic and imported light beers while at the same time does not overpower the drinker with heavy body or excessive bitterness. So find yourself a cold ''Hurricane'' and ENJOY!																																																																			
', 1579, 6.2, null, 247, 25, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (416, 'Eisenbahn South American Pale Ale (S.A.P.A.)', 'Our Summer seasonal, Trade Winds Tripel is a Belgian-style Golden Ale with a Southeast Asian twist. Instead of using candi sugar (typical for such a beer), we use rice in the mash to lighten the body and increase the gravity, and spice with Thai Basil. The result is an aromatic, digestible and complex beer made for a lazy summer evening.																																			
', 1580, 6.2, null, 248, 26, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (417, 'Geist Bock', 'Brewing the perfect ale is truly a balancing act...hazardous work you might say. With Hop Hazard our challenge was to hand craft a malt rich base that could counterbalance a combustible five-hop blend and still leave your taste buds with enough room to enjoy a unique, crisp hop finish.																											
', 1581, 6.2, null, 249, 7, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (418, 'Troll Porter', 'This unfiltered ale retains a medium maltiness and body and features a flowery hop perfume and pleasant bitterness.											
', 1582, 6.2, null, 250, 8, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (419, 'Dark Ale', 'ESB was launched into the Fuller''s family in 1971, as a winter brew to replace a beer named Old Burton Extra. The potential of the beer was soon realised and ESB was installed as a permanent fixture, creating an immediate impact. 

Not only was it one of the strongest regularly brewed draught beers in the country (at 5.5% ABV), it was also one of the tastiest, and as the awareness of the beer grew, so did its popularity. ESB''s reputation was soon enhanced after being named CAMRA''s (Campaign for Real Ale) Beer of the Year in 1978, and the beer has not stopped winning since! 

With three CAMRA Beer of the Year awards, two World Champion Beer awards, and numerous other gold medals to speak of, ESB is, quite simply, the Champion Ale.', 1583, 6.2, null, 251, 9, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (420, 'Cinder Cone Red', '"Sweet with some
tropical flavors, some banana, a hint of clove."
', 1584, 7.3, null, 252, 10, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (421, 'LTD 02 Lager', 'A Jahva-Choklat hybrid.		
', 1585, 7.3, null, 253, 11, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (422, 'Saison Vielle Provision', ' It was designed to be enjoyed during the holiday season. 

When you bring a glass of this dark copper ale to your lips to take your first sip you will notice the aroma of cinnamon.  There is no aromatic hop added that might overpower the distinct spice scent.  The medium body of this beer is formed from caramel and pale malts.  These create enough body to support the spices without making the beer excessively rich.  Bittering hops are added to counter the sweetness of the malt and spice.  The finish of the beer is a blend of cinnamon and nutmeg.  The combination of these two spices results in a balanced, pumpkin-pie flavor. 

The overall character is a smooth, medium bodied ale spiced with cinnamon and nutmeg"
', 1586, 7.3, null, 254, 12, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (423, 'Organic Barley Wine Ale 2005', 'A GOLD MEDAL WINNER at the Great American Beer Festival, beating out 98 other IPA''s and chosen as the best IPA in the country! It''s amber color and incredible hoppy aroma will keep you coming back for more. R-U-HOPPY?																									
', 1587, 7.3, null, 255, 13, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (424, 'Old Boardhead 2006', 'Our lightest beer, made from 100% malted barley. This blonde lager is what sailors really swam to shore for; it''s light, with very little bitterness and a slight malt finish.
', 1588, 7.3, null, 256, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (425, 'Blue Dot Double India Pale Ale', 'Our Coconut Porter is a classic robust porter spiced with all natural toasted coconut.  It is black in color and crowned with a creamy, dark tan head.   It begins with a malty-toasted coconut aroma followed by a rich, silky feel with tastes of dark malt, chocolate, and hints of coffee.  It then finishes with flavors of  toasted coconut and hoppy spice to balance the finish.
', 1589, 7.3, null, 257, 15, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (426, 'Nugget', 'Crisp and smooth. This beer is deep amber in color. Toasty, malty flavor. Full-bodied with a clean finish.
', 1590, 7.3, null, 258, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (427, 'American Pale Ale', 'This American style amber ale will be our opening release and house beer for SCBC. AmericAle is smooth and diverse all in one shot. With a medium malt body and fresh hop aroma we hope you enjoy drinking it as much as we do making it.
', 1591, 7.3, null, 259, 17, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (428, '471 Extra ESB', 'India Pale Ale was developed in Burton, England, as a \\Super-Premium\\" hoppy pale ale around 1800. The extra strength in alcohol and hops helped preserve the beer on its long export journeys to India and beyond. The style developed a following worldwide. Its flavor begins with a smooth
', 1592, 7.3, null, 260, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (429, 'Odell Red Ale', '"Our Czech Premium Lager is beer for light beer lovers. The most gentle heads of the high quality hop, virgin clear natural water and granules of selected species of Moravian barley make it the beverage of real experts. 

', 1593, 7.3, null, 261, 19, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (430, 'Long Leg English Fuggles Hop Ale', 'Crisp and smooth. This beer is deep amber in color. Toasty, malty flavor. Full-bodied with a clean finish.										
', 1594, 7.3, null, 262, 20, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (431, 'Summer Lightning', 'This traditional Bavarian-style kellerbier originated from the small artisanal breweries of Franconia, where it is still a favorite in the local beer gardens.  Ours is served unfiltered with a crisp, smooth finish that taste like sunshine. ', 1595, 7.3, null, 263, 21, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (432, 'Haymaker Extra Pale Ale', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%', 1596, 7.3, null, 264, 22, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (433, 'Pale Ale', 'Many Mexican beer styles today are descendants of old Austrian styles, from when Austria ruled Mexico in the late 19th century. Our Dos Perros is made with German Munich malt, English Pale malt, and Chocolate malt, and hopped with Perle and Saaz hops. To lighten the body, as many Mexican brewers do, we add a small portion of flaked maize. The result is a wonderfully bready malt aroma, balanced with some maize sweetness and a noble hop finish.', 1597, 7.3, null, 265, 23, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (434, 'Silk Lady', 'A new version of an American classic. Our Yazoo Pale Ale bursts with spicy, citrusy hop aroma and flavor, coming from the newly discovered Amarillo hop. The wonderful hop aroma is balanced nicely with a toasty malt body, ending with a cleansing hop finish. Made with English Pale, Munich, Vienna, and Crystal malts, and generously hopped with Amarillo, Perle, and Cascade hops. Fermented with our English ale yeast.', 1598, 5.7, null, 266, 24, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (435, 'Danger Ale', 'An authentic example of a Bavarian Hefeweizen. ', 1599, 5.5, null, 267, 25, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (436, 'Irish Style Ale', 'A rich, chocolaty English Porter with a clean finish. We use the finest floor-malted Maris Otter malts from England, the same malts used for the best single-malt scotch. A portion of malted rye gives a spicy, slightly dry finish.', 1600, 5.5, null, 268, 26, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (437, 'Yeti Special Export', 'The tan lace clings to the glass as you raise the pint to your lips. Close your eyes and smile as the rich espresso notes fade to a dry, roasted finish. Exceptionally smooth and satisfying. Made with English Pale malt, roasted barley, black patent malt, and flaked barley. Hopped with East Kent Goldings and Target hops, and fermented with our English ale yeast.', 1601, 5.5, null, 269, 7, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (438, 'Steelie Brown Ale', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.', 1602, 5.5, null, 270, 8, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (439, 'Bond Street 19th Anniversary', 'A regular winner of awards for quality and flavour, and winner of the silver medal at the International Brewing Industry Awards 2002, this refreshing yet full-bodied bitter is a favourite with beer drinkers everywhere. The rich flavours of premium malt and goldings hops are unmistakable in this well balanced, traditionally brewed bitter.', 1603, 5.5, null, 271, 9, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (440, 'Trumpeter', 'FA is no small beer: despite its deceptively pale golden colour, it boasts a big, smooth flavour and strong punch. Brewed with the finest English malts, and conditioned in cask with dry hops to produce fresh hop aromas and a fuller flavour, delighting the mouth and stimulating the tongue.', 1604, 5.5, null, 272, 10, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (441, 'Nitro Porter', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%', 1605, 5.5, null, 273, 11, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (442, 'Fourteen', 'Many Mexican beer styles today are descendants of old Austrian styles, from when Austria ruled Mexico in the late 19th century. Our Dos Perros is made with German Munich malt, English Pale malt, and Chocolate malt, and hopped with Perle and Saaz hops. To lighten the body, as many Mexican brewers do, we add a small portion of flaked maize. The result is a wonderfully bready malt aroma, balanced with some maize sweetness and a noble hop finish.', 1606, 5.5, null, 274, 12, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (443, 'Old Stock Ale 2007', 'A new version of an American classic. Our Yazoo Pale Ale bursts with spicy, citrusy hop aroma and flavor, coming from the newly discovered Amarillo hop. The wonderful hop aroma is balanced nicely with a toasty malt body, ending with a cleansing hop finish. Made with English Pale, Munich, Vienna, and Crystal malts, and generously hopped with Amarillo, Perle, and Cascade hops. Fermented with our English ale yeast.', 1607, 5.5, null, 275, 13, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (444, 'Cold Hop', 'An authentic example of a Bavarian Hefeweizen. ', 1608, 5.5, null, 276, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (445, 'Hitachino Nest Espresso Stout', 'A rich, chocolaty English Porter with a clean finish. We use the finest floor-malted Maris Otter malts from England, the same malts used for the best single-malt scotch. A portion of malted rye gives a spicy, slightly dry finish.', 1609, 5.5, null, 277, 15, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (446, 'Witte', 'The tan lace clings to the glass as you raise the pint to your lips. Close your eyes and smile as the rich espresso notes fade to a dry, roasted finish. Exceptionally smooth and satisfying. Made with English Pale malt, roasted barley, black patent malt, and flaked barley. Hopped with East Kent Goldings and Target hops, and fermented with our English ale yeast.', 1610, 5.7, null, 278, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (447, 'Slaapmutske Triple Nightcap', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.', 1611, 5.7, null, 279, 17, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (448, 'Biere de Mars', 'A regular winner of awards for quality and flavour, and winner of the silver medal at the International Brewing Industry Awards 2002, this refreshing yet full-bodied bitter is a favourite with beer drinkers everywhere. The rich flavours of premium malt and goldings hops are unmistakable in this well balanced, traditionally brewed bitter.', 1612, 5.7, null, 280, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (449, 'Kerst Pater Special Christmas Beer', 'FA is no small beer: despite its deceptively pale golden colour, it boasts a big, smooth flavour and strong punch. Brewed with the finest English malts, and conditioned in cask with dry hops to produce fresh hop aromas and a fuller flavour, delighting the mouth and stimulating the tongue.', 1613, 5.7, null, 281, 19, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (450, 'Zoetzuur Flemish Ale', 'This beer is not very bitter which allows the flavors of the rye and honey to come through. The result is a full bodied, sweet, and malty beer.													
', 1614, 5.7, null, 282, 20, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (451, 'Extra Strong Vintage Ale', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.											
', 1615, 5.7, null, 283, 21, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (452, 'Certified Organic India Pale Ale', 'An authentic example of a Bavarian Hefeweizen. 				
', 1616, 5.7, null, 284, 22, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (453, 'Certified Organic Porter', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%										
', 1617, 4.5, null, 285, 23, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (454, 'Certified Organic Extra Pale Ale', 'Dark ale with a tan head. Subtle sake flavor from cask aging.						
', 1618, 4.5, null, 286, 24, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (455, 'Certified Organic Amber Ale', 'A very yeasty beer with an earthy aroma. Rich flavors deepen the stout character.							
', 1619, 4.5, null, 182, 25, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (456, 'Farm House Ale', 'Many Mexican beer styles today are descendants of old Austrian styles, from when Austria ruled Mexico in the late 19th century. Our Dos Perros is made with German Munich malt, English Pale malt, and Chocolate malt, and hopped with Perle and Saaz hops. To lighten the body, as many Mexican brewers do, we add a small portion of flaked maize. The result is a wonderfully bready malt aroma, balanced with some maize sweetness and a noble hop finish.', 1620, 4.5, null, 183, 26, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (457, 'Beer Town Brown', 'A new version of an American classic. Our Yazoo Pale Ale bursts with spicy, citrusy hop aroma and flavor, coming from the newly discovered Amarillo hop. The wonderful hop aroma is balanced nicely with a toasty malt body, ending with a cleansing hop finish. Made with English Pale, Munich, Vienna, and Crystal malts, and generously hopped with Amarillo, Perle, and Cascade hops. Fermented with our English ale yeast.', 1621, 4.5, null, 184, 7, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (458, 'Tanners Jack', 'An authentic example of a Bavarian Hefeweizen. ', 1622, 4.5, null, 185, 8, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (459, 'Mephistopheles Stout', 'A rich, chocolaty English Porter with a clean finish. We use the finest floor-malted Maris Otter malts from England, the same malts used for the best single-malt scotch. A portion of malted rye gives a spicy, slightly dry finish.', 1623, 4.5, null, 186, 9, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (460, 'Vertical Epic 07.07.07', 'The tan lace clings to the glass as you raise the pint to your lips. Close your eyes and smile as the rich espresso notes fade to a dry, roasted finish. Exceptionally smooth and satisfying. Made with English Pale malt, roasted barley, black patent malt, and flaked barley. Hopped with East Kent Goldings and Target hops, and fermented with our English ale yeast.', 1625, 4.5, null, 188, 11, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (461, 'Ringwood Brewery Old Thumper Extra Special Ale', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.', 1626, 4.5, null, 189, 12, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (462, 'Export Ale', 'A regular winner of awards for quality and flavour, and winner of the silver medal at the International Brewing Industry Awards 2002, this refreshing yet full-bodied bitter is a favourite with beer drinkers everywhere. The rich flavours of premium malt and goldings hops are unmistakable in this well balanced, traditionally brewed bitter.', 1627, 6.2, null, 190, 13, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (463, 'Bourbon Barrel Porter', 'FA is no small beer: despite its deceptively pale golden colour, it boasts a big, smooth flavour and strong punch. Brewed with the finest English malts, and conditioned in cask with dry hops to produce fresh hop aromas and a fuller flavour, delighting the mouth and stimulating the tongue.', 1628, 6.2, null, 191, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (464, 'Old Guardian Barley Wine 2007', 'This beer is not very bitter which allows the flavors of the rye and honey to come through. The result is a full bodied, sweet, and malty beer.													
', 1629, 6.2, null, 192, 15, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (465, 'White Christmas', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.											
', 1630, 6.2, null, 193, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (466, 'Winter', 'An authentic example of a Bavarian Hefeweizen. 				
', 1631, 5.8, null, 194, 17, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (467, 'Undercover Investigation Shut-Down Ale', 'A bourbon barrel aged Imperial Stout infused with coffee.					
', 1632, 5.8, null, 195, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (468, 'Harvest Ale 2006', 'It is light with a bit of a citrus flavor.  A pretty standard summer seasonal.							
', 1633, 5.8, null, 196, 19, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (469, 'Classic Gueuze', 'A BIG PALE ALE with an awsome balance of Belgian malts with Fuggles and East Kent Golding hops.									
', 1634, 5.8, null, 197, 20, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (470, 'Gueuze-Lambic', 'Anything but a typical American Pale, Santa Fe Pale Ale is as full bodied as its most robust English counterparts, while asserting its American origin with a healthy nose resplendent with Cascade and Willamette hops. It finishes with a well-balanced combination of the subtle, almost Pilsner-like maltiness accentuated by the German yeast used to brew this Santa Fe classic, and a hop bite sufficient to leave a lingering smile on the face of any fan of American Pale Ales.																																																																											
', 1635, 5.8, null, 198, 21, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (471, 'Schwarzbier / Dunkel', 'A well balanced American Amber Ale. Smooth malt character balanced with a healthy dose of Cascade hops aged on oak chips - our most popular beer.																										
A light bodied golden ale, low in hop character with a spicy aroma from whole Czech Saaz hops.																										
One of these beers is available all of the time. Our heartiest beers emphasize roasted malts and the generous use of hops. Creamy, smooth and always nitrogenated, these beers are nearly a meal or a dessert in themselves.																										
', 1636, 5.8, null, 199, 22, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (472, 'Sawtooth Ale', 'The first offering from the Coastal Extreme Brewing Company blends some of the world''s finest ingredients into a delightful beer. In producing our amber ale, we selected the highest quality European and American malts and hops. This ale has a malt character which is delicately balanced with its hop profile so that consumers of all levels enjoy drinking it. Hurricane Amber Ale is a full flavored beer which clearly has more taste than other domestic and imported light beers while at the same time does not overpower the drinker with heavy body or excessive bitterness. So find yourself a cold ''Hurricane'' and ENJOY!																																																																			
', 1637, 5.8, null, 200, 23, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (473, 'Pole Star Pilsner', 'Our Summer seasonal, Trade Winds Tripel is a Belgian-style Golden Ale with a Southeast Asian twist. Instead of using candi sugar (typical for such a beer), we use rice in the mash to lighten the body and increase the gravity, and spice with Thai Basil. The result is an aromatic, digestible and complex beer made for a lazy summer evening.																																			
', 1638, 5.8, null, 201, 24, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (474, '90 Shilling', 'Brewing the perfect ale is truly a balancing act...hazardous work you might say. With Hop Hazard our challenge was to hand craft a malt rich base that could counterbalance a combustible five-hop blend and still leave your taste buds with enough room to enjoy a unique, crisp hop finish.																											
', 1639, 5.8, null, 202, 25, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (475, 'Sunshine Wheat', 'This unfiltered ale retains a medium maltiness and body and features a flowery hop perfume and pleasant bitterness.											
', 1640, 5.8, null, 203, 26, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (476, 'Grand Cru', 'ESB was launched into the Fuller''s family in 1971, as a winter brew to replace a beer named Old Burton Extra. The potential of the beer was soon realised and ESB was installed as a permanent fixture, creating an immediate impact. 

Not only was it one of the strongest regularly brewed draught beers in the country (at 5.5% ABV), it was also one of the tastiest, and as the awareness of the beer grew, so did its popularity. ESB''s reputation was soon enhanced after being named CAMRA''s (Campaign for Real Ale) Beer of the Year in 1978, and the beer has not stopped winning since! 

With three CAMRA Beer of the Year awards, two World Champion Beer awards, and numerous other gold medals to speak of, ESB is, quite simply, the Champion Ale.', 1641, 5.8, null, 204, 7, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (477, 'Der Weisse Bock', '"Sweet with some
tropical flavors, some banana, a hint of clove."
', 1642, 5.8, null, 205, 8, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (478, 'Stouterik / The Brussels Stout', 'A Jahva-Choklat hybrid.		
', 1643, 5.8, null, 206, 9, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (479, 'Black Jack Porter', ' It was designed to be enjoyed during the holiday season. 

When you bring a glass of this dark copper ale to your lips to take your first sip you will notice the aroma of cinnamon.  There is no aromatic hop added that might overpower the distinct spice scent.  The medium body of this beer is formed from caramel and pale malts.  These create enough body to support the spices without making the beer excessively rich.  Bittering hops are added to counter the sweetness of the malt and spice.  The finish of the beer is a blend of cinnamon and nutmeg.  The combination of these two spices results in a balanced, pumpkin-pie flavor. 

The overall character is a smooth, medium bodied ale spiced with cinnamon and nutmeg"
', 1644, 5.8, null, 207, 10, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (480, 'Lunar Ale', 'A GOLD MEDAL WINNER at the Great American Beer Festival, beating out 98 other IPA''s and chosen as the best IPA in the country! It''s amber color and incredible hoppy aroma will keep you coming back for more. R-U-HOPPY?																									
', 1645, 5.8, null, 208, 11, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (481, 'Denver Pale Ale / DPA', 'Our lightest beer, made from 100% malted barley. This blonde lager is what sailors really swam to shore for; it''s light, with very little bitterness and a slight malt finish.
', 1646, 5.8, null, 209, 12, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (482, 'Grotten Flemish Ale', 'Our Coconut Porter is a classic robust porter spiced with all natural toasted coconut.  It is black in color and crowned with a creamy, dark tan head.   It begins with a malty-toasted coconut aroma followed by a rich, silky feel with tastes of dark malt, chocolate, and hints of coffee.  It then finishes with flavors of  toasted coconut and hoppy spice to balance the finish.
', 1647, 5.8, null, 210, 13, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (483, 'Grand Cru', 'Crisp and smooth. This beer is deep amber in color. Toasty, malty flavor. Full-bodied with a clean finish.
', 1648, 5.5, null, 211, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (484, 'Summer Common', 'This American style amber ale will be our opening release and house beer for SCBC. AmericAle is smooth and diverse all in one shot. With a medium malt body and fresh hop aroma we hope you enjoy drinking it as much as we do making it.
', 1649, 5.7, null, 212, 15, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (485, 'Golden Ale', 'India Pale Ale was developed in Burton, England, as a \\Super-Premium\\" hoppy pale ale around 1800. The extra strength in alcohol and hops helped preserve the beer on its long export journeys to India and beyond. The style developed a following worldwide. Its flavor begins with a smooth
', 1650, 5.7, null, 213, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (486, 'Ambree', '"Our Czech Premium Lager is beer for light beer lovers. The most gentle heads of the high quality hop, virgin clear natural water and granules of selected species of Moravian barley make it the beverage of real experts. 

', 1651, 5.7, null, 214, 17, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (487, 'Pater 6', 'Crisp and smooth. This beer is deep amber in color. Toasty, malty flavor. Full-bodied with a clean finish.										
', 1652, 5.7, null, 215, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (488, 'Blanche de Namur', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%', 1653, 5.7, null, 216, 19, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (489, 'Skinny Dip', 'Many Mexican beer styles today are descendants of old Austrian styles, from when Austria ruled Mexico in the late 19th century. Our Dos Perros is made with German Munich malt, English Pale malt, and Chocolate malt, and hopped with Perle and Saaz hops. To lighten the body, as many Mexican brewers do, we add a small portion of flaked maize. The result is a wonderfully bready malt aroma, balanced with some maize sweetness and a noble hop finish.', 1654, 5.7, null, 217, 20, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (490, 'Blanche Double', 'A new version of an American classic. Our Yazoo Pale Ale bursts with spicy, citrusy hop aroma and flavor, coming from the newly discovered Amarillo hop. The wonderful hop aroma is balanced nicely with a toasty malt body, ending with a cleansing hop finish. Made with English Pale, Munich, Vienna, and Crystal malts, and generously hopped with Amarillo, Perle, and Cascade hops. Fermented with our English ale yeast.', 1655, 5.7, null, 218, 21, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (491, 'Summer Bright Ale', 'An authentic example of a Bavarian Hefeweizen. ', 1656, 4.5, null, 219, 22, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (502, 'Sapporo Premium Beer', 'A rich, chocolaty English Porter with a clean finish. We use the finest floor-malted Maris Otter malts from England, the same malts used for the best single-malt scotch. A portion of malted rye gives a spicy, slightly dry finish.', 1668, 5.7, null, 231, 14, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (503, 'Dark German Lager', 'The tan lace clings to the glass as you raise the pint to your lips. Close your eyes and smile as the rich espresso notes fade to a dry, roasted finish. Exceptionally smooth and satisfying. Made with English Pale malt, roasted barley, black patent malt, and flaked barley. Hopped with East Kent Goldings and Target hops, and fermented with our English ale yeast.', 1669, 5.7, null, 232, 15, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (504, 'Spring Heat Spiced Wheat', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.', 1670, 5.7, null, 233, 16, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (505, 'Gose', 'A regular winner of awards for quality and flavour, and winner of the silver medal at the International Brewing Industry Awards 2002, this refreshing yet full-bodied bitter is a favourite with beer drinkers everywhere. The rich flavours of premium malt and goldings hops are unmistakable in this well balanced, traditionally brewed bitter.', 1671, 6.2, null, 234, 17, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (506, 'Pils', 'FA is no small beer: despite its deceptively pale golden colour, it boasts a big, smooth flavour and strong punch. Brewed with the finest English malts, and conditioned in cask with dry hops to produce fresh hop aromas and a fuller flavour, delighting the mouth and stimulating the tongue.', 1672, 6.2, null, 235, 18, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (507, 'BestOfAles', 'This beer is not very bitter which allows the flavors of the rye and honey to come through. The result is a full bodied, sweet, and malty beer.													
', 1673, 6.2, null, 236, 19, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (508, 'Eisenbahn Defumada', 'American Black Ale. Aromas of an American IPA, dark toffee and chocolate flavors without roasted bitterness.											
', 1674, 6.2, null, 237, 20, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (509, 'Eisenbahn Escura', 'An authentic example of a Bavarian Hefeweizen. 				
', 1675, 6.2, null, 238, 21, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (510, 'Eisenbahn Dourada', 'Anniversary offering, Imperial Red. Heavily hopped and oak aged, very balanced. 12 oz. bottles; 4 pack. ABV: 9.2%										
', 1676, 6.2, null, 239, 22, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (511, 'Fallen Angel Sweet Stout', 'Dark ale with a tan head. Subtle sake flavor from cask aging.						
', 1677, 6.2, null, 240, 23, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (512, 'Hot Shot ESB', 'A very yeasty beer with an earthy aroma. Rich flavors deepen the stout character.							
', 1678, 6.2, null, 241, 24, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (513, 'Honey Bunny Blonde Ale', 'A silky, crisp, and rich amber-colored ale with a fluffy head and strong banana note on the nose.									
', 1679, 6.2, null, 242, 25, null);
INSERT INTO db_beers.beers (beer_id, name, description, brewery_id, abv, picture, style_id, created_by, user_id) VALUES (514, 'Kent Lake Kolsch', 'Chaotic yet hypnotic, Mayhem is a deliberate Double IPA, brewed and conditioned with a special Belgian yeast that contributes complexity and spice. Abundant American hops offer grapefruit, pine, must and mint, which play off the fullness and sweetness of pale malts then provide a biting yet enticing finish.

Mayhem Double IPA accents the complex spice combinations found in Thai, Indian, Cajun, Morrocan, Mexican and Southwest American cuisines as well as BBQ marinades, dry rubs and sauces.

Mayhem creates MAYHEM.', 1550, 6.2, null, 243, 26, null);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (1, 317, 7);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (2, 318, 8);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (3, 319, 9);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (4, 320, 10);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (5, 321, 11);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (6, 322, 12);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (7, 323, 13);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (8, 324, 14);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (9, 325, 15);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (10, 326, 16);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (11, 333, 17);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (12, 356, 18);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (13, 357, 19);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (14, 358, 20);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (15, 359, 21);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (16, 360, 22);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (17, 361, 23);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (18, 362, 24);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (19, 363, 25);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (20, 364, 26);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (21, 365, 27);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (22, 366, 28);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (23, 367, 29);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (24, 368, 30);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (25, 369, 31);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (26, 370, 32);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (27, 371, 33);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (28, 372, 34);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (29, 373, 7);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (30, 374, 8);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (31, 375, 9);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (32, 376, 10);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (33, 377, 11);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (34, 378, 12);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (35, 379, 13);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (36, 380, 14);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (37, 381, 15);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (38, 382, 16);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (39, 383, 17);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (40, 384, 18);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (41, 385, 19);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (42, 386, 20);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (43, 387, 21);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (44, 388, 22);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (45, 389, 23);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (46, 390, 24);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (47, 391, 25);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (48, 392, 26);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (49, 393, 27);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (50, 394, 28);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (51, 395, 29);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (52, 396, 30);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (53, 397, 31);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (54, 398, 32);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (55, 399, 33);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (56, 400, 34);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (57, 401, 7);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (58, 402, 8);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (59, 403, 9);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (60, 404, 10);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (61, 405, 11);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (62, 406, 12);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (63, 407, 13);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (64, 408, 14);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (65, 409, 15);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (66, 410, 16);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (67, 411, 17);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (68, 412, 18);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (69, 413, 19);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (70, 414, 20);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (71, 415, 21);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (72, 416, 22);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (73, 417, 23);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (74, 418, 24);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (75, 419, 25);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (76, 420, 26);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (77, 421, 27);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (78, 422, 28);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (79, 423, 29);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (80, 424, 30);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (81, 425, 31);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (82, 426, 32);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (83, 427, 33);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (84, 428, 34);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (85, 429, 7);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (86, 430, 8);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (87, 431, 9);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (88, 432, 10);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (89, 433, 11);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (90, 434, 12);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (91, 435, 13);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (92, 436, 14);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (93, 437, 15);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (94, 438, 16);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (95, 439, 17);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (96, 440, 18);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (97, 441, 19);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (98, 442, 20);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (99, 443, 21);
INSERT INTO db_beers.beers_tags (beer_tag_id, beer_id, tag_id) VALUES (100, 444, 22);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1481, 'Bloomington Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1482, 'BluCreek Brewing Company', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1483, 'Blue Cat Brew Pub', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1484, 'Blue Corn Caf and Brewery - Albuquerque', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1485, 'Blue Point Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1486, 'Blue Ridge Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1487, 'Bluegrass Brewing Company, Inc.', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1488, 'Bobcat Cafe & Brewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1489, 'Boiler Room Brewpub', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1490, 'Bonaventure Brewing Co', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1491, 'Bonfire Brewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1492, 'Bootleggers Steakhouse and Brewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1493, 'Boscos Memphis Brewing', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1494, 'Boston Beer Company', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1495, 'Boston Beer Works', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1496, 'Bottom''s Up Brewing', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1497, 'Boulder Beer Company', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1498, 'Boulevard Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1499, 'Boundary Bay Brewery and Bistro', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1500, 'Brauhaus Brew Hall', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1501, 'Breckenridge BBQ of Omaha', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1502, 'Breckenridge Brewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1503, 'Brew Kettle Taproom & Smokehouse BOP', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1504, 'Brew Makers', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1505, 'Brewer''s Art', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1506, 'Brewery at Martha''s Vineyard', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1507, 'Brewery Creek Brewing', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1508, 'Brewery Ommegang', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1509, 'Brewmasters Restaurant and Brewery South', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1510, 'Brewpub-on-the-Green', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1511, 'Bricktown Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1512, 'BridgePort Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1513, 'Brimstone Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1514, 'Bristol Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1515, 'Broad Ripple Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1516, 'Brooklyn Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1517, 'DC Brau', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1518, 'Brown Street Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1519, 'Brownings', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1520, 'BT McClintic Beer Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1521, 'Bube''s Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1522, 'Bull & Bush Pub & Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1523, 'Bulldog Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1524, 'Bullfrog Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1525, 'Butte Creek Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1526, 'Butterfield Brewing #1', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1527, 'Buzzards Bay Brewing Inc.', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1528, 'C.B. & Potts of Cheyenne', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1529, 'C.H. Evans Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1530, 'Caldera Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1531, 'Calhoun''s Microbrewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1532, 'California Cider Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1533, 'Callahan''s Pub and Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1534, 'Cambridge Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1535, 'Cape Ann Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1536, 'Capital Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1537, 'Capital City Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1538, 'Capitol City Brewing #4', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1539, 'Captain Lawrence Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1540, 'Carlyle Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1541, 'Carmel Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1542, 'Carolina Beer Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1543, 'Carolina Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1544, 'Carver Brewing Co.', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1545, 'Castle Springs Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1546, 'Cat''s Paw Home Brew', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1547, 'Catamount Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1548, 'Cedar Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1549, 'Celis Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1550, 'Central Waters Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1551, 'Chama River Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1552, 'Charlie and Jake''s Brewery and BBQ', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1553, 'Chelsea Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1554, 'Cherryland Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1555, 'Cheshire Cat Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1556, 'Chicago Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1557, 'Cigar City Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1558, 'Circle V Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1559, 'City Brewing Company, LLC', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1560, 'CJ''s Brewery & Grill', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1561, 'Cleveland ChopHouse and Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1562, 'Climax Brewing Copmany', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1563, 'Clipper City Brewing Co.', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1564, 'Coach''s Norman', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1565, 'Coast Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1566, 'Coast Range Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1567, 'Coastal Fog Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1568, 'Coeur d''Alene Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1569, 'Cold Spring Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1570, 'Columbia Bay Brewery Restaurant and Pub', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1571, 'Commonwealth Brewing #1', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1572, 'Cooper''s Cave Ale Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1573, 'Cooperstown Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1574, 'Coors Brewing - Golden Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1575, 'Copper Dragon Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1576, 'Copper Eagle Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1577, 'Copper Kettle Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1578, 'Corner Pub', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1579, 'Coronado Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1580, 'Costal Extreme Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1581, 'Court Avenue Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1582, 'Courthouse Pub', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1583, 'Crabby Larry''s Brewpub Steak & Crab House', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1584, 'Crabtree Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1585, 'Craftsman Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1586, 'Crane River Brewpub and Cafe', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1587, 'Crescent City Brewhouse', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1588, 'Crested Butte Brewery &  Pub', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1589, 'Cricket Hill', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1590, 'Crooked River Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1591, 'Crooked Waters Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1592, 'Devil''s Canyon', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1593, 'Cugino Brewing Company', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1594, 'Cumberland Brewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1595, 'D.L. Geary Brewing Company', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1596, 'Dark Horse Brewing Co.', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1597, 'Day Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1598, 'Deep Creek Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1599, 'DeGroen''s Grill', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1600, 'Del Mar Stuft Pizza and Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1601, 'Delafield Brewhaus', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1602, 'Dempsey''s Restaurant & Brewery', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1603, 'Denmark Brewing', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1604, 'Denver ChopHouse and Brewery', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1605, 'Deschutes Brewery', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1606, 'Devil Mountain Brewing', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1607, 'Diamond Bear Brewing Co.', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1608, 'Diamond Knot Brewery & Alehouse', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1609, 'Diamondback Brewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1610, 'Dick''s Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1611, 'Dillon Dam Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1612, 'Dirt Cheap Cigarettes and Beer', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1613, 'Dixie Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1614, 'Dixon''s Downtown Grill', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1615, 'Dock Street Beer', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1616, 'Dogfish Head Craft Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1617, 'Dostal Alley', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1618, 'Double Mountain Brewery & Taproom', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1619, 'Dragonmead Microbrewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1620, 'Drake''s Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1621, 'Dry Gulch Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1622, 'Dubuque Brewing and Bottling', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1623, 'Duck-Rabbit Craft Brewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1624, 'DuClaw', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1625, 'Dunedin Brewery', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1626, 'Duneland Brewhouse Pub and Restaurant', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1627, 'Eagle Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1628, 'East End Brewing Company', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1629, 'Eastern Shore Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1630, 'Eddie McStiff''s Brewpub', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1631, 'Eel River Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1632, 'Egan Brewing', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1633, 'EJ Phair Brewing Company and Alehouse', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1634, 'El Toro Brewing Company', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1635, 'Elk Creek Cafe and Aleworks', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1636, 'Elk Grove Brewery & Restaurant', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1637, 'Ellersick Brewing Big E Ales', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1638, 'Ellicott Mills Brewing', 32);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1639, 'Elliott Bay Brewery and Pub', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1640, 'Elm City Brewing Co', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1641, 'Elysian Brewery & Public House', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1642, 'Elysian Brewing - TangleTown', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1643, 'Emery Pub', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1644, 'Emmett''s Tavern and Brewery', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1645, 'Empyrean Brewing Company', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1646, 'EndeHouse Brewery and Restaurant', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1647, 'Engine House #9', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1648, 'English Ales Brewery', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1649, 'Erie Brewing Company', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1650, 'Esser''s Cross Plains Brewery', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1651, 'Estes Park Brewery', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1652, 'Etna Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1653, 'F.X. Matt Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1654, 'Far West Ireland Brewing', 9);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1655, 'Fauerbach Brewing Company', 81);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1656, 'Faultline Brewing #1', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1657, 'Faultline Brewing #2', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1658, 'Fifty Fifty Brewing Co.', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1659, 'Firehouse Brewery & Restaurant', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1660, 'Firehouse Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1661, 'Firehouse Grill & Brewery', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1662, 'Firestone Walker Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1663, 'First Coast Brewing', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1664, 'Fish Brewing Company & Fish Tail Brewpub', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1665, 'Fitger''s Brewhouse, Brewery and Grill', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1666, 'Fitzpatrick''s Brewing', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1667, 'Flagstaff Brewing', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1668, 'Flat Branch Pub & Brewing', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1669, 'Flat Earth Brewing Company', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1670, 'Flatlander''s Restaurant & Brewery', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1671, 'Florida Beer Company', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1672, 'Flossmoor Station Brewery', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1673, 'Flour City Brewing', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1674, 'Flyers Restraunt and Brewery', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1675, 'Flying Bison Brewing', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1676, 'Flying Dog Brewery', 45);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1677, 'Flying Fish Brewing Company', 187);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1678, 'Foothills Brewing Company', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1679, 'Fordham Brewing', 186);
INSERT INTO db_beers.breweries (brewery_id, brewery, country_id) VALUES (1680, 'Fort Collins Brewery', 81);
INSERT INTO db_beers.countries (country_id, country) VALUES (1, 'Afghanistan');
INSERT INTO db_beers.countries (country_id, country) VALUES (2, 'Albania');
INSERT INTO db_beers.countries (country_id, country) VALUES (3, 'Algeria');
INSERT INTO db_beers.countries (country_id, country) VALUES (4, 'Andorra');
INSERT INTO db_beers.countries (country_id, country) VALUES (5, 'Angola');
INSERT INTO db_beers.countries (country_id, country) VALUES (6, 'Antigua and Barbuda');
INSERT INTO db_beers.countries (country_id, country) VALUES (7, 'Argentina');
INSERT INTO db_beers.countries (country_id, country) VALUES (8, 'Armenia');
INSERT INTO db_beers.countries (country_id, country) VALUES (9, 'Australia');
INSERT INTO db_beers.countries (country_id, country) VALUES (10, 'Austria');
INSERT INTO db_beers.countries (country_id, country) VALUES (11, 'Azerbaijan');
INSERT INTO db_beers.countries (country_id, country) VALUES (12, 'Bahamas');
INSERT INTO db_beers.countries (country_id, country) VALUES (13, 'Bahrain');
INSERT INTO db_beers.countries (country_id, country) VALUES (14, 'Bangladesh');
INSERT INTO db_beers.countries (country_id, country) VALUES (15, 'Barbados');
INSERT INTO db_beers.countries (country_id, country) VALUES (16, 'Belarus');
INSERT INTO db_beers.countries (country_id, country) VALUES (17, 'Belgium');
INSERT INTO db_beers.countries (country_id, country) VALUES (18, 'Belize');
INSERT INTO db_beers.countries (country_id, country) VALUES (19, 'Benin');
INSERT INTO db_beers.countries (country_id, country) VALUES (20, 'Bhutan');
INSERT INTO db_beers.countries (country_id, country) VALUES (21, 'Bolivia');
INSERT INTO db_beers.countries (country_id, country) VALUES (22, 'Bosnia and Herzegovina');
INSERT INTO db_beers.countries (country_id, country) VALUES (23, 'Botswana');
INSERT INTO db_beers.countries (country_id, country) VALUES (24, 'Brazil');
INSERT INTO db_beers.countries (country_id, country) VALUES (25, 'Brunei');
INSERT INTO db_beers.countries (country_id, country) VALUES (26, 'Bulgaria');
INSERT INTO db_beers.countries (country_id, country) VALUES (27, 'Burkina Faso');
INSERT INTO db_beers.countries (country_id, country) VALUES (28, 'Burundi');
INSERT INTO db_beers.countries (country_id, country) VALUES (29, 'Cabo Verde');
INSERT INTO db_beers.countries (country_id, country) VALUES (30, 'Cambodia');
INSERT INTO db_beers.countries (country_id, country) VALUES (31, 'Cameroon');
INSERT INTO db_beers.countries (country_id, country) VALUES (32, 'Canada');
INSERT INTO db_beers.countries (country_id, country) VALUES (33, 'Central African Republic');
INSERT INTO db_beers.countries (country_id, country) VALUES (34, 'Chad');
INSERT INTO db_beers.countries (country_id, country) VALUES (35, 'Chile');
INSERT INTO db_beers.countries (country_id, country) VALUES (36, 'China');
INSERT INTO db_beers.countries (country_id, country) VALUES (37, 'Colombia');
INSERT INTO db_beers.countries (country_id, country) VALUES (38, 'Comoros');
INSERT INTO db_beers.countries (country_id, country) VALUES (39, 'Congo (Congo-Brazzaville)');
INSERT INTO db_beers.countries (country_id, country) VALUES (40, 'Costa Rica');
INSERT INTO db_beers.countries (country_id, country) VALUES (41, 'Côte d''Ivoire');
INSERT INTO db_beers.countries (country_id, country) VALUES (42, 'Croatia');
INSERT INTO db_beers.countries (country_id, country) VALUES (43, 'Cuba');
INSERT INTO db_beers.countries (country_id, country) VALUES (44, 'Cyprus');
INSERT INTO db_beers.countries (country_id, country) VALUES (45, 'Czech Republic');
INSERT INTO db_beers.countries (country_id, country) VALUES (46, 'Denmark');
INSERT INTO db_beers.countries (country_id, country) VALUES (47, 'Djibouti');
INSERT INTO db_beers.countries (country_id, country) VALUES (48, 'Dominica');
INSERT INTO db_beers.countries (country_id, country) VALUES (49, 'Dominican Republic');
INSERT INTO db_beers.countries (country_id, country) VALUES (50, 'East Timor (Timor-Leste)');
INSERT INTO db_beers.countries (country_id, country) VALUES (51, 'Ecuador');
INSERT INTO db_beers.countries (country_id, country) VALUES (52, 'Egypt');
INSERT INTO db_beers.countries (country_id, country) VALUES (53, 'El Salvador');
INSERT INTO db_beers.countries (country_id, country) VALUES (54, 'Equatorial Guinea');
INSERT INTO db_beers.countries (country_id, country) VALUES (55, 'Eritrea');
INSERT INTO db_beers.countries (country_id, country) VALUES (56, 'Estonia');
INSERT INTO db_beers.countries (country_id, country) VALUES (57, 'Eswatini');
INSERT INTO db_beers.countries (country_id, country) VALUES (58, 'Ethiopia');
INSERT INTO db_beers.countries (country_id, country) VALUES (59, 'Fiji');
INSERT INTO db_beers.countries (country_id, country) VALUES (60, 'Finland');
INSERT INTO db_beers.countries (country_id, country) VALUES (61, 'France');
INSERT INTO db_beers.countries (country_id, country) VALUES (62, 'Gabon');
INSERT INTO db_beers.countries (country_id, country) VALUES (63, 'Georgia');
INSERT INTO db_beers.countries (country_id, country) VALUES (64, 'Germany');
INSERT INTO db_beers.countries (country_id, country) VALUES (65, 'Ghana');
INSERT INTO db_beers.countries (country_id, country) VALUES (66, 'Greece');
INSERT INTO db_beers.countries (country_id, country) VALUES (67, 'Grenada');
INSERT INTO db_beers.countries (country_id, country) VALUES (68, 'Guatemala');
INSERT INTO db_beers.countries (country_id, country) VALUES (69, 'Guinea');
INSERT INTO db_beers.countries (country_id, country) VALUES (70, 'Guinea-Bissau');
INSERT INTO db_beers.countries (country_id, country) VALUES (71, 'Guyana');
INSERT INTO db_beers.countries (country_id, country) VALUES (72, 'Haiti');
INSERT INTO db_beers.countries (country_id, country) VALUES (73, 'Honduras');
INSERT INTO db_beers.countries (country_id, country) VALUES (74, 'Hungary');
INSERT INTO db_beers.countries (country_id, country) VALUES (75, 'Iceland');
INSERT INTO db_beers.countries (country_id, country) VALUES (76, 'India');
INSERT INTO db_beers.countries (country_id, country) VALUES (77, 'Indira');
INSERT INTO db_beers.countries (country_id, country) VALUES (78, 'Indonesia');
INSERT INTO db_beers.countries (country_id, country) VALUES (79, 'Iran');
INSERT INTO db_beers.countries (country_id, country) VALUES (80, 'Iraq');
INSERT INTO db_beers.countries (country_id, country) VALUES (81, 'Ireland');
INSERT INTO db_beers.countries (country_id, country) VALUES (82, 'Israel');
INSERT INTO db_beers.countries (country_id, country) VALUES (83, 'Italy');
INSERT INTO db_beers.countries (country_id, country) VALUES (84, 'Jamaica');
INSERT INTO db_beers.countries (country_id, country) VALUES (85, 'Japan');
INSERT INTO db_beers.countries (country_id, country) VALUES (86, 'Jordan');
INSERT INTO db_beers.countries (country_id, country) VALUES (87, 'Kazakhstan');
INSERT INTO db_beers.countries (country_id, country) VALUES (88, 'Kenya');
INSERT INTO db_beers.countries (country_id, country) VALUES (89, 'Kiribati');
INSERT INTO db_beers.countries (country_id, country) VALUES (90, 'Korea, North');
INSERT INTO db_beers.countries (country_id, country) VALUES (91, 'Korea, South');
INSERT INTO db_beers.countries (country_id, country) VALUES (92, 'Kosovo');
INSERT INTO db_beers.countries (country_id, country) VALUES (93, 'Kuwait');
INSERT INTO db_beers.countries (country_id, country) VALUES (94, 'Kyrgyzstan');
INSERT INTO db_beers.countries (country_id, country) VALUES (95, 'Laos');
INSERT INTO db_beers.countries (country_id, country) VALUES (96, 'Latvia');
INSERT INTO db_beers.countries (country_id, country) VALUES (97, 'Lebanon');
INSERT INTO db_beers.countries (country_id, country) VALUES (98, 'Lesotho');
INSERT INTO db_beers.countries (country_id, country) VALUES (99, 'Liberia');
INSERT INTO db_beers.countries (country_id, country) VALUES (100, 'Libya');
INSERT INTO db_beers.countries (country_id, country) VALUES (101, 'Liechtenstein');
INSERT INTO db_beers.countries (country_id, country) VALUES (102, 'Lithuania');
INSERT INTO db_beers.countries (country_id, country) VALUES (103, 'Luxembourg');
INSERT INTO db_beers.countries (country_id, country) VALUES (104, 'Madagascar');
INSERT INTO db_beers.countries (country_id, country) VALUES (105, 'Malawi');
INSERT INTO db_beers.countries (country_id, country) VALUES (106, 'Malaysia');
INSERT INTO db_beers.countries (country_id, country) VALUES (107, 'Maldives');
INSERT INTO db_beers.countries (country_id, country) VALUES (108, 'Mali');
INSERT INTO db_beers.countries (country_id, country) VALUES (109, 'Malta');
INSERT INTO db_beers.countries (country_id, country) VALUES (110, 'Marshall Islands');
INSERT INTO db_beers.countries (country_id, country) VALUES (111, 'Mauritania');
INSERT INTO db_beers.countries (country_id, country) VALUES (112, 'Mauritius');
INSERT INTO db_beers.countries (country_id, country) VALUES (113, 'Mexico');
INSERT INTO db_beers.countries (country_id, country) VALUES (114, 'Micronesia, Federated States of');
INSERT INTO db_beers.countries (country_id, country) VALUES (115, 'Moldova');
INSERT INTO db_beers.countries (country_id, country) VALUES (116, 'Monaco');
INSERT INTO db_beers.countries (country_id, country) VALUES (117, 'Mongolia');
INSERT INTO db_beers.countries (country_id, country) VALUES (118, 'Montenegro');
INSERT INTO db_beers.countries (country_id, country) VALUES (119, 'Morocco');
INSERT INTO db_beers.countries (country_id, country) VALUES (120, 'Mozambique');
INSERT INTO db_beers.countries (country_id, country) VALUES (121, 'Myanmar (Burma)');
INSERT INTO db_beers.countries (country_id, country) VALUES (122, 'Namibia');
INSERT INTO db_beers.countries (country_id, country) VALUES (123, 'Nauru');
INSERT INTO db_beers.countries (country_id, country) VALUES (124, 'Nepal');
INSERT INTO db_beers.countries (country_id, country) VALUES (125, 'Netherlands');
INSERT INTO db_beers.countries (country_id, country) VALUES (126, 'New Zealand');
INSERT INTO db_beers.countries (country_id, country) VALUES (127, 'Nicaragua');
INSERT INTO db_beers.countries (country_id, country) VALUES (128, 'Niger');
INSERT INTO db_beers.countries (country_id, country) VALUES (129, 'Nigeria');
INSERT INTO db_beers.countries (country_id, country) VALUES (130, 'North Macedonia');
INSERT INTO db_beers.countries (country_id, country) VALUES (131, 'Norway');
INSERT INTO db_beers.countries (country_id, country) VALUES (132, 'Oman');
INSERT INTO db_beers.countries (country_id, country) VALUES (133, 'Pakistan');
INSERT INTO db_beers.countries (country_id, country) VALUES (134, 'Palau');
INSERT INTO db_beers.countries (country_id, country) VALUES (135, 'Panama');
INSERT INTO db_beers.countries (country_id, country) VALUES (136, 'Papua New Guinea');
INSERT INTO db_beers.countries (country_id, country) VALUES (137, 'Paraguay');
INSERT INTO db_beers.countries (country_id, country) VALUES (138, 'Peru');
INSERT INTO db_beers.countries (country_id, country) VALUES (139, 'Philippines');
INSERT INTO db_beers.countries (country_id, country) VALUES (140, 'Poland');
INSERT INTO db_beers.countries (country_id, country) VALUES (141, 'Portugal');
INSERT INTO db_beers.countries (country_id, country) VALUES (142, 'Qatar



');
INSERT INTO db_beers.countries (country_id, country) VALUES (143, 'Romania');
INSERT INTO db_beers.countries (country_id, country) VALUES (144, 'Russia');
INSERT INTO db_beers.countries (country_id, country) VALUES (145, 'Rwanda');
INSERT INTO db_beers.countries (country_id, country) VALUES (146, 'Saint Kitts and Nevis');
INSERT INTO db_beers.countries (country_id, country) VALUES (147, 'Saint Lucia');
INSERT INTO db_beers.countries (country_id, country) VALUES (148, 'Saint Vincent and the Grenadines');
INSERT INTO db_beers.countries (country_id, country) VALUES (149, 'Samoa');
INSERT INTO db_beers.countries (country_id, country) VALUES (150, 'San Marino');
INSERT INTO db_beers.countries (country_id, country) VALUES (151, 'Sao Tome and Principe');
INSERT INTO db_beers.countries (country_id, country) VALUES (152, 'Saudi Arabia');
INSERT INTO db_beers.countries (country_id, country) VALUES (153, 'Senegal');
INSERT INTO db_beers.countries (country_id, country) VALUES (154, 'Serbia');
INSERT INTO db_beers.countries (country_id, country) VALUES (155, 'Seychelles');
INSERT INTO db_beers.countries (country_id, country) VALUES (156, 'Sierra Leone');
INSERT INTO db_beers.countries (country_id, country) VALUES (157, 'Singapore');
INSERT INTO db_beers.countries (country_id, country) VALUES (158, 'Slovakia');
INSERT INTO db_beers.countries (country_id, country) VALUES (159, 'Slovenia');
INSERT INTO db_beers.countries (country_id, country) VALUES (160, 'Solomon Islands');
INSERT INTO db_beers.countries (country_id, country) VALUES (161, 'Somalia');
INSERT INTO db_beers.countries (country_id, country) VALUES (162, 'South Africa');
INSERT INTO db_beers.countries (country_id, country) VALUES (163, 'Spain');
INSERT INTO db_beers.countries (country_id, country) VALUES (164, 'Sri Lanka');
INSERT INTO db_beers.countries (country_id, country) VALUES (165, 'Sudan');
INSERT INTO db_beers.countries (country_id, country) VALUES (166, 'Sudan, South');
INSERT INTO db_beers.countries (country_id, country) VALUES (167, 'Suriname');
INSERT INTO db_beers.countries (country_id, country) VALUES (168, 'Sweden');
INSERT INTO db_beers.countries (country_id, country) VALUES (169, 'Switzerland');
INSERT INTO db_beers.countries (country_id, country) VALUES (170, 'Syria');
INSERT INTO db_beers.countries (country_id, country) VALUES (171, 'Taiwan');
INSERT INTO db_beers.countries (country_id, country) VALUES (172, 'Tajikistan');
INSERT INTO db_beers.countries (country_id, country) VALUES (173, 'Tanzania');
INSERT INTO db_beers.countries (country_id, country) VALUES (174, 'Thailand');
INSERT INTO db_beers.countries (country_id, country) VALUES (175, 'The Gambia');
INSERT INTO db_beers.countries (country_id, country) VALUES (176, 'Togo');
INSERT INTO db_beers.countries (country_id, country) VALUES (177, 'Tonga');
INSERT INTO db_beers.countries (country_id, country) VALUES (178, 'Trinidad and Tobago');
INSERT INTO db_beers.countries (country_id, country) VALUES (179, 'Tunisia');
INSERT INTO db_beers.countries (country_id, country) VALUES (180, 'Turkey');
INSERT INTO db_beers.countries (country_id, country) VALUES (181, 'Turkmenistan');
INSERT INTO db_beers.countries (country_id, country) VALUES (182, 'Tuvalu');
INSERT INTO db_beers.countries (country_id, country) VALUES (183, 'Uganda');
INSERT INTO db_beers.countries (country_id, country) VALUES (184, 'Ukraine');
INSERT INTO db_beers.countries (country_id, country) VALUES (185, 'United Arab Emirates');
INSERT INTO db_beers.countries (country_id, country) VALUES (186, 'United Kingdom');
INSERT INTO db_beers.countries (country_id, country) VALUES (187, 'United States');
INSERT INTO db_beers.countries (country_id, country) VALUES (188, 'Uruguay');
INSERT INTO db_beers.countries (country_id, country) VALUES (189, 'Uzbekistan');
INSERT INTO db_beers.countries (country_id, country) VALUES (190, 'Vanuatu');
INSERT INTO db_beers.countries (country_id, country) VALUES (191, 'Vatican City');
INSERT INTO db_beers.countries (country_id, country) VALUES (192, 'Venezuela');
INSERT INTO db_beers.countries (country_id, country) VALUES (193, 'Vietnam');
INSERT INTO db_beers.countries (country_id, country) VALUES (194, 'Yemen');
INSERT INTO db_beers.countries (country_id, country) VALUES (195, 'Zambia');
INSERT INTO db_beers.lists (list_id, list_type) VALUES (2, 'DrankList');
INSERT INTO db_beers.lists (list_id, list_type) VALUES (1, 'WishList');
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (1, 1, 7, 512);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (2, 1, 8, 513);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (3, 1, 9, 514);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (4, 1, 10, 409);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (5, 1, 11, 410);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (6, 1, 12, 411);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (7, 1, 13, 412);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (8, 1, 14, 413);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (9, 1, 15, 414);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (10, 1, 16, 415);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (11, 2, 17, 416);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (12, 2, 18, 417);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (13, 2, 19, 418);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (14, 2, 20, 419);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (15, 2, 21, 420);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (16, 2, 22, 421);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (17, 2, 23, 422);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (18, 2, 24, 423);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (19, 2, 25, 424);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (20, 2, 26, 425);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (21, 1, 7, 426);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (22, 1, 8, 320);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (23, 1, 9, 321);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (24, 1, 10, 322);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (25, 1, 11, 323);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (26, 1, 12, 324);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (27, 1, 13, 325);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (28, 1, 14, 326);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (29, 1, 15, 333);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (30, 1, 16, 356);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (31, 2, 17, 357);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (32, 2, 18, 358);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (33, 2, 19, 359);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (34, 2, 20, 360);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (35, 2, 21, 361);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (36, 2, 22, 362);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (37, 2, 23, 363);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (38, 2, 24, 364);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (39, 2, 25, 365);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (40, 2, 26, 366);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (41, 1, 7, 367);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (42, 1, 8, 368);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (43, 1, 9, 369);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (44, 1, 10, 370);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (45, 1, 11, 371);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (46, 1, 12, 372);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (47, 1, 13, 373);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (48, 1, 14, 374);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (49, 1, 15, 375);
INSERT INTO db_beers.lists_users_beers (list_user_beer_id, list_id, user_id, beer_id) VALUES (50, 1, 16, 376);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (1, 14, 369, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (2, 18, 370, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (3, 9, 371, '2', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (4, 10, 372, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (5, 11, 373, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (6, 12, 374, '4', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (7, 13, 375, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (8, 14, 376, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (9, 15, 377, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (10, 16, 378, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (11, 17, 379, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (12, 18, 380, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (13, 19, 381, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (14, 20, 382, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (15, 21, 383, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (16, 22, 384, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (17, 23, 385, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (18, 24, 386, '4', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (19, 25, 387, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (20, 26, 388, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (21, 13, 389, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (22, 9, 390, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (23, 12, 391, '3', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (24, 10, 392, '2', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (25, 11, 393, '1', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (26, 12, 394, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (27, 13, 395, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (28, 14, 396, '4', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (29, 15, 397, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (30, 16, 398, '3', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (31, 17, 399, '4', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (32, 18, 400, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (33, 19, 401, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (34, 20, 402, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (35, 21, 403, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (36, 22, 404, '4', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (37, 23, 405, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (38, 24, 406, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (39, 25, 407, '4', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (40, 26, 408, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (41, 11, 409, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (42, 12, 410, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (43, 9, 424, '6', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (44, 10, 425, '3', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (45, 11, 426, '2', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (46, 12, 427, '1', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (47, 13, 428, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (48, 14, 429, '5', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (49, 15, 430, '4', 0, 0);
INSERT INTO db_beers.ratings (rating_id, user_id, beer_id, rating_value, beerId, userId) VALUES (50, 16, 431, '6', 0, 0);
INSERT INTO db_beers.roles (role_id, role, id) VALUES (1, 'Admin', 0);
INSERT INTO db_beers.roles (role_id, role, id) VALUES (2, 'Regular User', 0);
INSERT INTO db_beers.styles (style_id, style) VALUES (284, 'Aged Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (265, 'American Rye Ale or Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (245, 'American-Style Amber Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (181, 'American-Style Amber/Red Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (183, 'American-Style Barley Wine Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (186, 'American-Style Brown Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (259, 'American-Style Cream Ale or Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (251, 'American-Style Dark Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (248, 'American-Style Ice Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (193, 'American-Style Imperial Porter');
INSERT INTO db_beers.styles (style_id, style) VALUES (191, 'American-Style Imperial Stout');
INSERT INTO db_beers.styles (style_id, style) VALUES (189, 'American-Style India Black Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (179, 'American-Style India Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (242, 'American-Style Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (243, 'American-Style Light Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (244, 'American-Style Low-Carb Light Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (249, 'American-Style Malt Liquor');
INSERT INTO db_beers.styles (style_id, style) VALUES (250, 'American-Style Marzen/Oktoberfest');
INSERT INTO db_beers.styles (style_id, style) VALUES (174, 'American-Style Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (247, 'American-Style Pilsener');
INSERT INTO db_beers.styles (style_id, style) VALUES (246, 'American-Style Premium Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (188, 'American-Style Sour Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (190, 'American-Style Stout');
INSERT INTO db_beers.styles (style_id, style) VALUES (178, 'American-Style Strong Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (184, 'American-Style Wheat Wine Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (253, 'Australasian-Style Light Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (223, 'Australasian-Style Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (252, 'Baltic-Style Porter');
INSERT INTO db_beers.styles (style_id, style) VALUES (236, 'Bamberg-Style Bock Rauchbier');
INSERT INTO db_beers.styles (style_id, style) VALUES (235, 'Bamberg-Style Helles Rauchbier');
INSERT INTO db_beers.styles (style_id, style) VALUES (234, 'Bamberg-Style Marzen');
INSERT INTO db_beers.styles (style_id, style) VALUES (203, 'Bamberg-Style Weiss Rauchbier');
INSERT INTO db_beers.styles (style_id, style) VALUES (210, 'Belgian-Style Blonde Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (213, 'Belgian-Style Dark Strong Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (207, 'Belgian-Style Dubbel');
INSERT INTO db_beers.styles (style_id, style) VALUES (206, 'Belgian-Style Flanders/Oud Bruin');
INSERT INTO db_beers.styles (style_id, style) VALUES (217, 'Belgian-Style Fruit Lambic');
INSERT INTO db_beers.styles (style_id, style) VALUES (216, 'Belgian-Style Gueuze Lambic');
INSERT INTO db_beers.styles (style_id, style) VALUES (215, 'Belgian-Style Lambic');
INSERT INTO db_beers.styles (style_id, style) VALUES (211, 'Belgian-Style Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (212, 'Belgian-Style Pale Strong Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (209, 'Belgian-Style Quadrupel');
INSERT INTO db_beers.styles (style_id, style) VALUES (218, 'Belgian-Style Table Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (208, 'Belgian-Style Tripel');
INSERT INTO db_beers.styles (style_id, style) VALUES (214, 'Belgian-Style White');
INSERT INTO db_beers.styles (style_id, style) VALUES (195, 'Berliner-Style Weisse');
INSERT INTO db_beers.styles (style_id, style) VALUES (225, 'Bohemian-Style Pilsener');
INSERT INTO db_beers.styles (style_id, style) VALUES (165, 'British-Style Barley Wine Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (164, 'British-Style Imperial Stout');
INSERT INTO db_beers.styles (style_id, style) VALUES (167, 'Brown Porter');
INSERT INTO db_beers.styles (style_id, style) VALUES (260, 'California Common Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (270, 'Chocolate/Cocoa-Flavored Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (149, 'Classic English-Style Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (171, 'Classic Irish-Style Dry Stout');
INSERT INTO db_beers.styles (style_id, style) VALUES (271, 'Coffee-Flavored Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (264, 'Dark American Wheat Ale or Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (177, 'Dark American-Belgo-Style Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (228, 'Dortmunder/European-Style Export');
INSERT INTO db_beers.styles (style_id, style) VALUES (257, 'Dry Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (160, 'English-Style Brown Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (159, 'English-Style Dark Mild Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (150, 'English-Style India Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (158, 'English-Style Pale Mild Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (154, 'English-Style Summer Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (226, 'European Low-Alcohol Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (232, 'European-Style Dark');
INSERT INTO db_beers.styles (style_id, style) VALUES (277, 'Experimental Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (153, 'Extra Special Bitter');
INSERT INTO db_beers.styles (style_id, style) VALUES (268, 'Field Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (172, 'Foreign (Export)-Style Stout');
INSERT INTO db_beers.styles (style_id, style) VALUES (221, 'French & Belgian-Style Saison');
INSERT INTO db_beers.styles (style_id, style) VALUES (220, 'French-Style Biere de Garde');
INSERT INTO db_beers.styles (style_id, style) VALUES (175, 'Fresh Hop Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (267, 'Fruit Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (263, 'Fruit Wheat Ale or Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (204, 'German-Style Brown Ale/Altbier');
INSERT INTO db_beers.styles (style_id, style) VALUES (239, 'German-Style Doppelbock');
INSERT INTO db_beers.styles (style_id, style) VALUES (240, 'German-Style Eisbock');
INSERT INTO db_beers.styles (style_id, style) VALUES (238, 'German-Style Heller Bock/Maibock');
INSERT INTO db_beers.styles (style_id, style) VALUES (194, 'German-Style Kolsch');
INSERT INTO db_beers.styles (style_id, style) VALUES (199, 'German-Style Leichtes Weizen');
INSERT INTO db_beers.styles (style_id, style) VALUES (230, 'German-Style Marzen');
INSERT INTO db_beers.styles (style_id, style) VALUES (231, 'German-Style Oktoberfest');
INSERT INTO db_beers.styles (style_id, style) VALUES (224, 'German-Style Pilsener');
INSERT INTO db_beers.styles (style_id, style) VALUES (266, 'German-Style Rye Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (233, 'German-Style Schwarzbier');
INSERT INTO db_beers.styles (style_id, style) VALUES (275, 'Gluten-Free Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (185, 'Golden or Blonde Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (272, 'Herb and Spice Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (180, 'Imperial or Double India Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (182, 'Imperial or Double Red Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (222, 'International-Style Pale Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (256, 'International-Style Pilsener');
INSERT INTO db_beers.styles (style_id, style) VALUES (170, 'Irish-Style Red Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (261, 'Japanese Sake-Yeast Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (205, 'Kellerbier - Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (241, 'Kellerbier - Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (254, 'Latin American-Style Light Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (196, 'Leipzig-Style Gose');
INSERT INTO db_beers.styles (style_id, style) VALUES (262, 'Light American Wheat Ale or Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (227, 'Munchner-Style Helles');
INSERT INTO db_beers.styles (style_id, style) VALUES (286, 'Non-Alcoholic Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (169, 'Oatmeal Stout');
INSERT INTO db_beers.styles (style_id, style) VALUES (161, 'Old Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (151, 'Ordinary Bitter');
INSERT INTO db_beers.styles (style_id, style) VALUES (219, 'Other Belgian-Style Ales');
INSERT INTO db_beers.styles (style_id, style) VALUES (285, 'Other Strong Ale or Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (278, 'Out of Category');
INSERT INTO db_beers.styles (style_id, style) VALUES (176, 'Pale American-Belgo-Style Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (173, 'Porter');
INSERT INTO db_beers.styles (style_id, style) VALUES (269, 'Pumpkin Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (166, 'Robust Porter');
INSERT INTO db_beers.styles (style_id, style) VALUES (163, 'Scotch Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (157, 'Scottish-Style Export Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (156, 'Scottish-Style Heavy Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (155, 'Scottish-Style Light Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (258, 'Session Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (276, 'Smoke Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (187, 'Smoke Porter');
INSERT INTO db_beers.styles (style_id, style) VALUES (200, 'South German-Style Bernsteinfarbenes Weizen');
INSERT INTO db_beers.styles (style_id, style) VALUES (201, 'South German-Style Dunkel Weizen');
INSERT INTO db_beers.styles (style_id, style) VALUES (197, 'South German-Style Hefeweizen');
INSERT INTO db_beers.styles (style_id, style) VALUES (198, 'South German-Style Kristal Weizen');
INSERT INTO db_beers.styles (style_id, style) VALUES (202, 'South German-Style Weizenbock');
INSERT INTO db_beers.styles (style_id, style) VALUES (152, 'Special Bitter or Best Bitter');
INSERT INTO db_beers.styles (style_id, style) VALUES (273, 'Specialty Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (274, 'Specialty Honey Lager or Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (192, 'Specialty Stouts');
INSERT INTO db_beers.styles (style_id, style) VALUES (162, 'Strong Ale');
INSERT INTO db_beers.styles (style_id, style) VALUES (168, 'Sweet Stout');
INSERT INTO db_beers.styles (style_id, style) VALUES (237, 'Traditional German-Style Bock');
INSERT INTO db_beers.styles (style_id, style) VALUES (255, 'Tropical-Style Light Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (229, 'Vienna-Style Lager');
INSERT INTO db_beers.styles (style_id, style) VALUES (287, 'Winter Warmer');
INSERT INTO db_beers.styles (style_id, style) VALUES (279, 'Wood- and Barrel-Aged Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (281, 'Wood- and Barrel-Aged Dark Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (280, 'Wood- and Barrel-Aged Pale to Amber Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (283, 'Wood- and Barrel-Aged Sour Beer');
INSERT INTO db_beers.styles (style_id, style) VALUES (282, 'Wood- and Barrel-Aged Strong Beer');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (7, 'super');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (8, 'cool');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (9, 'perfect');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (10, 'awesome');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (11, 'genious');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (12, 'massacre');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (13, 'chill');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (14, 'cold');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (15, 'tepid');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (16, 'chilly');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (17, 'lukewarm');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (18, 'extra');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (19, 'nailing');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (20, 'ideal');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (21, 'full');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (22, 'withoutdoupt');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (23, 'joy');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (24, 'happiness');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (25, 'rejoying');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (26, 'joy');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (27, 'georgios');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (28, 'givemethebeer');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (29, 'extratime');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (30, 'enjoy');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (31, 'recklesness');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (32, 'bestlife');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (33, 'hello');
INSERT INTO db_beers.tags (tag_id, tag) VALUES (34, 'makingmyday');
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (7, 'martin_rusev', 212323, 'martin', 'rusev', 'mrusev@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (8, 'teodora_ivanova', 1123123, 'teodora', 'ivanova', 'tivanova@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (9, 'stanimir_gymov', 1312312, 'stanimir', 'gymov', 'sgymov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (10, 'todor_batkov', 1312312, 'todor', 'batkov', 'tbatkov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (11, 'ivan_ivanov', 242323, 'ivan', 'ivanov', 'ivanov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (12, 'petyr_petrov', 55235235, 'petyr', 'petrov', 'ppetrov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (13, 'nadejda_naidenova', 34343, 'nadejda', 'naidenova', 'nnaidenova@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (14, 'jordan_jordanov', 32352323, 'jordan', 'jordanov', 'jjordanov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (15, 'georgi_georgiev', 44234, 'georgi', 'georgiev', 'ggeorgiev@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (16, 'zvezdan_zvezdinov', 234234234, 'zvezdan', 'zvezdinov', 'zzvezdanov', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (17, 'ivana_ivanova', 434342, 'ivana', 'ivanova', 'iivanova@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (18, 'galq_petrova', 24234232, 'galq', 'petrova', 'gpetrova@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (19, 'elena_elenova', 423423, 'elena', 'elenova', 'eelenova@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (20, 'joana_joanova', 52523, 'joana', 'joanova', 'jjoanova@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (21, 'svilen_svilenov', 55555, 'svilen', 'svilenov', 'ssvilenov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (22, 'todor_ivanov', 6456456, 'todor', 'ivanov', 'tivanov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (23, 'svetlin_svetlinov', 64564544, 'svetlin', 'svetlinov', 'ssvetlinov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (24, 'angel_angelov', 8768768, 'angel', 'angelov', 'aangelov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (25, 'milen_milenov', 876766, 'milen', 'milenov', 'mmilenov@abv.bg', 0, null, null);
INSERT INTO db_beers.users (user_id, username, password, first_name, last_name, email, id, firstName, lastName) VALUES (26, 'evgeni_evgeniev', 445644, 'evgeni', 'evgeniev', 'eevgeniev@abv.bg', 0, null, null);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (1, 7, 1);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (2, 8, 1);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (3, 9, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (4, 10, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (5, 11, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (6, 12, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (7, 13, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (8, 14, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (9, 15, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (10, 16, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (11, 17, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (12, 18, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (13, 19, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (14, 20, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (15, 21, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (16, 22, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (17, 23, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (18, 24, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (19, 25, 2);
INSERT INTO db_beers.users_roles (user_role_id, user_id, role_id) VALUES (20, 26, 2);